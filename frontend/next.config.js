/** @type {import('next').NextConfig} */

require('dotenv').config();

const publicRuntimeConfig = {
    socketUrl: process.env.NEXT_PUBLIC_SOCKET_URL || 'http://localhost:3008/',
    socketPath: process.env.NEXT_PUBLIC_SOCKET_PATH || '/socket.io/',
    apiUrl: process.env.NEXT_PUBLIC_API_URL || "http://localhost:8000/api",
    apiUrlInternal: process.env.NEXT_PUBLIC_API_URL_INTERNAL || "http://localhost:8000/api",
    tanalyticsToken: process.env.NEXT_PUBLIC_TANALYTICS_TOKEN || '28f9e9a8-61c5-4a17-975a-8f6c99b7f7b7',
}

const nextConfig = {
  publicRuntimeConfig,
  reactStrictMode: false,
  env: {
    NEXT_PUBLIC_SOCKET_URL: process.env.NEXT_PUBLIC_SOCKET_URL || 'http://localhost:3008/',
    NEXT_PUBLIC_SOCKET_PATH: process.env.NEXT_PUBLIC_SOCKET_PATH || '/socket.io/',
    NEXT_PUBLIC_API_URL: process.env.NEXT_PUBLIC_API_URL || "http://localhost:8000/api",
    NEXT_PUBLIC_API_URL_INTERNAL: process.env.NEXT_PUBLIC_API_URL_INTERNAL || "http://localhost:8000/api",
}
}

module.exports = nextConfig
