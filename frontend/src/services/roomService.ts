import { AxiosResponse } from "axios";
import { RoomApiRoutes } from "../const/room/API_ROUTES";
import { IRoomCreateResponse, IRoomResponse } from "../const/room/types";
import $api from "../http";
import { Langs } from "../const/languages/LANGS";

interface ICreateRoomData {
    languageKey: Langs,
    isPublic?: boolean,
    name?: string,
    wordLists: string[],
    userId?: string,
}

export default class RoomService {
    static async create({
        languageKey,
        isPublic,
        name,
        wordLists,
        userId,
      }: ICreateRoomData): Promise<AxiosResponse<IRoomCreateResponse>> {
        return $api.post<IRoomCreateResponse>(RoomApiRoutes.CREATE, {
            languageKey,
            isPublic,
            name,
            wordLists,
            userId,
          });
    }

    static async get(roomId: string): Promise<AxiosResponse<IRoomResponse>> {
        return $api.get<IRoomResponse>(RoomApiRoutes.getRoomAPI(roomId));
    }
}