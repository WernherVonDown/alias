import { AxiosResponse } from "axios";
import $api from "../http";
import { IWord } from "../components/AddWordsList/AddWordsList";
import { Langs } from "../const/languages/LANGS";
import { IWordsList } from "../components/MyWords/MyWords";
import { IWordsListFull } from "../pages/my-words/[wordsListId]";

const BASE = 'words-list';

export default class WordsListService {
    static async create({ words, languageKey, name }: { name: string, words: IWord[], languageKey: Langs }): Promise<AxiosResponse<{ wordsListId: string }>> {
        return $api.post<{ wordsListId: string }>(BASE, { words, languageKey, name });
    }

    static async edit({ words, languageKey, name, id }: { name: string, words: IWord[], languageKey: Langs, id: string }): Promise<AxiosResponse<{ wordsListId: string }>> {
        return $api.post<{ wordsListId: string }>(`${BASE}/${id}`, { words, languageKey, name });
    }

    static async get({ }): Promise<AxiosResponse<IWordsList[]>> {
        return $api.get<IWordsList[]>(BASE, {});
    }

    static async getById(id: string): Promise<AxiosResponse<IWordsListFull>> {
        return $api.get<IWordsListFull>(`${BASE}/${id}`, {});
    }

    static async getPublic({ }): Promise<AxiosResponse<IWordsList[]>> {
        return $api.get<IWordsList[]>(`${BASE}/public`, {});
    }

    static async getTranslation({ word, translation, language }: { word: string, translation: string, language: Langs }): Promise<AxiosResponse<{ word: string, translation: string }>> {
        return $api.get<{ word: string, translation: string }>(`${BASE}/translation`, { params: { word, translation, language } });
    }
}
