import React from "react";
import styles from './Welcome.module.scss';
import classNames from 'classnames';
import Lottie from 'react-lottie';
import { Button, Grid, Typography } from "@mui/material";
import { RulesModal } from "../../RulesModal/RulesModal";
import LandingAnimation from "../../../lottie/landing_animation.json";
import { CreateGameBtn } from "../../CreateGameBtn/CreateGameBtn";
import { LangsList } from "../../../const/languages/LANGS";

export const Welcome: React.FC = () => {
    return (
        <Grid container display="flex" flexDirection={{ sx: 'column', sm: 'column', md: 'row' }} height={1} alignItems={"center"}>
            {/* <> */}
            <Grid item display="flex" xs={12} sm={12} md={6} alignItems={"center"}>
                <div className={styles.title}>
                    <div >
                        <Typography variant="h1">Общайся.</Typography>
                        <Typography variant="h1">Играй в ALIAS.</Typography>
                        <Typography variant="h1">Практикуй  <span style={{ color: 'var(--primary)' }}>язык.</span></Typography>
                    </div>

                    <Typography variant="h5">Начни разговаривать на {Object.values(LangsList).map(l => l.flag).join(' ')} вместе с друзьями, просто играя в Alias</Typography>
                    <div className={styles.buttons}>
                        {/* <Button variant="contained" className={styles.playButton} onClick={(e: any) => {
                        document.getElementById('startGameSection')?.scrollIntoView({ behavior: 'smooth', block: 'start' })
                    }}>
                        Играть
                    </Button> */}
                        <CreateGameBtn className={styles.playButton} />
                        <RulesModal>
                            <Button className={styles.playButton} color="inherit" variant='outlined'>Правила</Button>
                        </RulesModal>
                    </div>
                </div>



            </Grid>
            <Grid item display={{ md: "flex", xs: 'none' }} overflow={'auto'} xs={12} lg={5.9} md={6}>
                {/* <div className={styles.rightContent}></div> */}
                <Lottie options={{
                    loop: true,
                    autoplay: true,
                    animationData: LandingAnimation,
                    rendererSettings: {
                        preserveAspectRatio: 'xMidYMid slice'
                    }
                }}
                    // height={'10%'}
                    // width={400}
                    style={{ display: 'flex' }}
                    speed={0.7}
                    isStopped={false}
                    isPaused={false} />
            </Grid>
            {/* </> */}

        </Grid>
    )
}