import React from 'react';

const Donate = () => {
    return <iframe src="https://widget.qiwi.com/widgets/big-widget-728x200?publicKey=48e7qUxn9T7RyYE1MVZswX1FRSbE6iyCj2gCRwwF3Dnh5XrasNTx3BGPiMsyXQFNKQhvukniQG8RTVhYm3iP692jbHLvJfa3UBKvRp4NbBEzqAQanwua57nz1BucWH8dXcgv8JsW4mJrotxm5GLdGcFrvcwPTWm8yDodhrmWrzVRseDqCKffSK9CYUTr8&noCache=true" width="728" height="200" allowTransparency={true} scrolling="no" style={{border: 'none'}} frameBorder="0"></iframe>
}

export default Donate;