import { Button, Grid, Paper, TextField, Typography } from '@mui/material';
import Link from 'next/link';
import React, { useCallback, useContext, useState } from 'react';
import { LoginRoute } from '../../const/API_ROUTES';
import { AuthContext } from '../../context/AuthContext';
import { useInput } from '../../hooks/useInput';
import styles from './RegistrationForm.module.scss';

export const RegistrationForm: React.FC = () => {
    const { actions: { register } } = useContext(AuthContext);
    const [registerDone, setRegiserDone] = useState(false);
    const email = useInput('');
    const password1 = useInput('');
    const password2 = useInput('');

    const onRegister = useCallback(async () => {
        if (email.value.trim().length && password1.value.trim().length && password2.value.trim().length) {
            if (password1.value === password2.value) {
                const res: any = await register(email.value, password1.value);
                if (res.success) {
                    setRegiserDone(true);
                }
            } else {
                alert('пароли не совпадают')
            }
        } else {
            alert('Заполните все поля');
        }
    }, [email.value, password1.value, password2.value]);

    if (registerDone) {
        return<Paper>
            <div className={styles.registerForm}>
            <Typography textAlign={"center"} variant="h5">Подтвердите вашу почту. Eсли письмо не пришло, проверьте спам.</Typography>
            </div>
            </Paper>
    }

    return <>
        <Paper>
            <div className={styles.registerForm}>
                <Typography variant='h5'>Регистрация</Typography>
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...email} type={'text'} label='почта' />
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...password1} type={'password'} label='пароль' />
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...password2} type={'password'} label='повторите пароль' />
                <Button fullWidth sx={{ marginTop: 1 }} variant='contained' onClick={onRegister}>
                    Регистрация
                </Button>
            </div>
        </Paper>
        <Grid container className={styles.bottomWrapper} direction={"column"} display={"flex"} justifyContent="center" marginTop={0.5} spacing={2} textAlign={"center"}>
            <Grid item>
                <Typography>Есть аккаунт? <Link href={LoginRoute}>
                    Войти
                </Link>
                </Typography>
            </Grid>
        </Grid>
    </>;
}