import React, { useContext, useState } from "react";
import { RoomContext } from "../../context/RoomContext";
// import { TextChatWidget } from "../TextChatWidget/TextChatWidget.component";
import { VideoChat } from "../VideoChat/VideoChat.component";
import styles from "./Room.module.scss";

import dynamic from 'next/dynamic'
import { Suspense } from 'react'
import { Grid, Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Game from "../Game/Game";
import GameHistory from "../GameHistory/GameHistory";
import { Chat } from "../TextChat/Chat";
import { SwipeableEdgeDrawer } from "../MobileChat/MobileChat.component";
import { VideoControls } from "../VideoControls/VideoControls.component";
//@ts-ignore https://nextjs.org/docs/advanced-features/dynamic-import
const TextChatWidget = dynamic(() => import('../TextChatWidget/TextChatWidget.component').then((mod) => mod.TextChatWidget), { ssr: false });

interface IProps {

}

export const Room: React.FC<IProps> = () => {
    const { state: { userName } } = useContext(RoomContext);
    const [mobileChatOpened, setMobileChatOpenend] = useState(false);
    //TODO GAP 
    return (
        <div className={styles.roomWrapper}>
            <Grid container display={"flex"} sx={{ flexGrow: 1 }} padding={1} direction={{ md: 'row', lg: 'row', xs: "row" }} spacing={1}>
                <Grid item xs={2.2} sx={{ background: 'unset' }} display={{ md: 'flex', lg: 'flex', xs: "none" }}>
                    <div className={styles.videoChatWrapper}>
                        <VideoChat />
                    </div>

                </Grid>
                <Grid display={"flex"} flexDirection={"column"} item xs={12} md={6.4} lg={7.1} sx={{ background: 'unset', gap: '0.5rem' }}>
                    <Grid item maxHeight={{ xs: 90, md: 100, lg: 112 }} xs={12} >
                        <GameHistory />
                    </Grid>

                    <Grid item xs={12} sx={{ background: 'unset' }} position="relative">
                        <Game />
                        <Box position={'absolute'} sx={{
                            width: '100%',
                            justifyContent: 'center'
                        }} display={{ md: 'none', lg: 'none', xs: "flex" }}>
                            <VideoControls />
                        </Box>
                    </Grid>
                    <Grid item xs={12} maxHeight={130} display={{ md: 'none', lg: 'none', xs: "flex" }} sx={{ background: '#181820' }}>
                        <VideoChat />
                    </Grid>
                </Grid>
                <Grid item xs={2.7} md={3.4} lg={2.7} display={{ md: 'flex', lg: 'flex', xs: "none" }} sx={{ background: 'unset' }}>
                    <Chat />
                </Grid>
            </Grid>
            <Box position={'absolute'} sx={{
                bottom: '0.5rem',
                width: '100%',
                justifyContent: 'center'
            }} display={{ md: 'flex', lg: 'flex', xs: "none" }}>
                <VideoControls />
            </Box>
        </div>
    )
    return (
        <div className={styles.roomWrapper}>
            <VideoChat />
            {/* <TextChatWidget /> */}
            <Suspense fallback={`Loading...`}>
                <TextChatWidget />
            </Suspense>
        </div>
    )
}
