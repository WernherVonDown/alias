import React from "react";
import { CreateGameModal } from "../CreateGameModal/CreateGameModal";
import { Button, ButtonPropsSizeOverrides } from "@mui/material";
import { Langs } from "../../const/languages/LANGS";

interface IProps {
    className?: string;
    fullWidth?: boolean;
    wordsListId?: string;
    languageKey?: Langs,
    size?: "small" | "medium" | "large";
}

export const CreateGameBtn: React.FC<IProps> = ({className, fullWidth, wordsListId, languageKey, size}) => {
    return <CreateGameModal fullWidthВtn={fullWidth} languageKey={languageKey} wordsListId={wordsListId}>
        <Button variant="contained" size={size} fullWidth={fullWidth} className={className} >
        &#9654;&nbsp;Играть
        </Button>
    </CreateGameModal>
}