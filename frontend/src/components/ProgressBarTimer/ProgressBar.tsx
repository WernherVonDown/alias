import React, { useEffect, useState } from 'react'
import { GAME_EVENTS } from '../../const/game/GAME_EVENTS';
import { useMediaSocketSubscribe } from '../../hooks/useMediaSocketSubscribe';
import { Typography } from '@mui/material';
import { fancyTimeFormat } from '../../utils/fancyTimeFormat';

const MAX_TIMER = 60 * 1000;

const ProgressBarTimer = () => {
    const [progress, setProgress] = useState<string>()

    useMediaSocketSubscribe(GAME_EVENTS.gameTimer, (time) => {
        setProgress(fancyTimeFormat(time))
    })
    return <Typography variant="h4">{progress}</Typography>
}

export default ProgressBarTimer;