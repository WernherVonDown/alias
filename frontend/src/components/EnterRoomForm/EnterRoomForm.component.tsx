import React, { useCallback, useContext } from 'react';
import { Button, Grid, Paper, TextField, Typography } from '@mui/material';
import { useInput } from '../../hooks/useInput';

interface IProps {
    done: (userName: string) => void
}

const inputStyle = { WebkitBoxShadow: "0 0 0 1000px #181820 inset" };

const USER_NAME_LOCAL_STORAGE_KEY = 'user_name';

export const PreEnterRoomForm: React.FC<IProps> = ({ done }) => {
    const userName = useInput(window.localStorage.getItem(USER_NAME_LOCAL_STORAGE_KEY) || '');

    const handleDone = useCallback(() => {
        if (userName.value.trim()) {
            done(userName.value.trim())
            window.localStorage.setItem(USER_NAME_LOCAL_STORAGE_KEY, userName.value.trim())
        } else {
            alert('Заполните поле')
        }
    }, [userName.value])

    return <Paper><Grid padding={3} container direction={"column"}>
        <Typography variant='h5'>Введите имя</Typography>
        <TextField inputProps={{ style: inputStyle }} variant="outlined" margin='normal' fullWidth size='small' {...userName} type={'text'} label='Ваше имя' />
        <Button fullWidth sx={{ marginTop: 1 }} variant='contained' onClick={handleDone}>
            Войти
        </Button>
    </Grid></Paper>;
}