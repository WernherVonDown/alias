import { Button, Container, Grid, Paper, Typography } from "@mui/material";
import React, { useCallback, useContext } from "react";
import { StreamContext } from "../../context/StreamContext";
import { ConferenceVideo } from "../ConferenceVideo/ConferenceVideo.component";

interface IProps {
}

export const StreamSettings: React.FC<IProps> = () => {
    const { state: { stream, devicePermissions }, actions: { setDevicesSet } } = useContext(StreamContext);

    const done = useCallback(() => {
        setDevicesSet(true);
    }, [])

    return (
        <Paper>
            <Container maxWidth="sm">
                <Grid container direction="column" alignSelf={"center"} columnSpacing={1} justifyContent="center" alignContent={"center"} spacing={3} padding={1}>
                    <Grid item>
                        <ConferenceVideo devices={devicePermissions} stream={stream} isLocal />
                    </Grid>
                    <Grid paddingBottom={2} item justifyContent={"center"} style={{display: 'flex'}}>
                        <Button variant="contained" fullWidth size="large" onClick={done}>
                            Играть!
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </Paper>
    )
}
