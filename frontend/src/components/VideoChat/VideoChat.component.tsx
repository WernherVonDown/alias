import React, { useCallback, useContext, useMemo } from "react";
import styles from "./VideoChat.module.scss";
import { ResponsiveRectsContainer } from "../../common/ResponsiveRectsContainer";
import { VideoChatContext } from "../../context/VideoChatContext";
import { ConferenceVideo } from "../ConferenceVideo/ConferenceVideo.component";
import { IDevicePermissions, IRemoteStream } from "../../const/videoChat/types";
import { RoomContext } from "../../context/RoomContext";
import { StreamContext } from "../../context/StreamContext";
import { GameContext } from "../../context/GameContext";

export const VideoChat: React.FC = () => {
    const { state: { me, users } } = useContext(RoomContext);
    const { state: { stream } } = useContext(StreamContext);
    const { state: { remoteStreams } } = useContext(VideoChatContext);
    const { state: { devicePermissions } } = useContext(StreamContext);
    const { state: { currentUserId, roundStarted }} = useContext(GameContext);

    const renderStream = useCallback(
        (
            { stream, userId, userName, isLocal, devices }:
                { stream: MediaStream, userId: string, userName: string, isLocal?: boolean, devices: IDevicePermissions }
        ) => {
            return (
                <div key={`video_${userId}`}><ConferenceVideo noControls devices={devices} isLocal={isLocal} userName={userName} key={`video_${userId}`} stream={stream} /></div>
            )
        }, [])

    const userStream = useMemo(() => {
        return renderStream({ stream, userId: me?.socketId || '', userName: me?.userName || '', isLocal: true, devices: devicePermissions })
    }, [stream, me, devicePermissions])

    const videoStreams = useMemo(() => {
        return remoteStreams.filter(s => s.userId !== currentUserId || !roundStarted).map(r => renderStream({...r, devices: users.find(u => r.userId === u.socketId)?.devices || {audio: true, video: true}}));
    }, [remoteStreams, users, currentUserId, roundStarted]);

    return (
        <ResponsiveRectsContainer rectsGap={"0.5rem"} containerClassName={styles.videoChatGalleryWrapper}>
            {userStream}
            {videoStreams}
        </ResponsiveRectsContainer>
    )
}
