import { Box, Button, Card, CardActions, CardContent, Typography } from "@mui/material";
import React from "react";
import { Langs, LangsList } from "../../const/languages/LANGS";
import { useRouter } from "next/router";
import { CreateGameBtn } from "../CreateGameBtn/CreateGameBtn";

interface IWordsList {
    id: string,
    name: string,
    languageKey: Langs,
    isPublic: boolean,
    wordsNum: number,
}

export const WordsListItem: React.FC<IWordsList> = (props) => {
    const router = useRouter();
    return <Card
    sx={{background: 'none', border: '2px solid #47474c' }}
    // sx={{ height: 100, }}
    >
        <CardContent>
            <Typography variant="h5" component="div" noWrap>
                {LangsList[props.languageKey]?.flag} {props.name}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                {props.wordsNum} слов(а)
            </Typography>
        </CardContent>
        <CardActions >
            <Box gap={1} display={'flex'} width={'100%'}>
                <Button size="large" variant="outlined" onClick={() => router.push(`/my-words/${props.id}`)} fullWidth>Редактировать</Button>
                <CreateGameBtn fullWidth size="large" wordsListId={props.id} languageKey={props.languageKey} />
            </Box>
        </CardActions>
    </Card>
}
