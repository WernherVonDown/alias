import { Box, Card, CardContent, Grid, Typography } from "@mui/material";
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Langs } from "../../const/languages/LANGS";
import { WordsListItem } from "./WordsListItem";
import { useRouter } from "next/router";
import WordsListService from "../../services/wordsListService";

export interface IWordsList {
    name: string,
    languageKey: Langs,
    isPublic: boolean,
    wordsNum: number,
    id: string,
}

export const MyWords: React.FC = () => {
    const router = useRouter()
    const [list, setList] = useState<IWordsList[]>([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getWordsList()
    }, []);

    const getWordsList = useCallback(async () => {
        setLoading(true);
        try {
            const l = await WordsListService.get({});
            setList(l.data)
        } catch (error) {
            console.log("getWordsList error", error);
        } finally {
            setLoading(false);
        }
    }, [])

    return (
        <Grid display="flex" flexDirection={'column'} height={1} overflow={'aut'}>
            <Grid item>
                <Typography variant="h4" color={'white'}>Мои слова</Typography>
            </Grid>
            <Grid height={1} width={1} 
            // sx={{ borderTop: '1px solid rgba(255, 255, 255, 0.1)' }} 
            overflow={'auto'}>
                {loading ? <Grid justifyContent={'center'} padding={3} display={'flex'} width={1}><Typography variant="h5" color={'white'}>Loading...</Typography></Grid>
                    : <Grid
                        container
                        gap={1}
                        // sx={{ background: 'red' }}
                        // maxHeight={'100%'}
                        // alignContent={'start'}
                        // gridColumn={3}
                        marginTop={1}
                        gridTemplateColumns={{
                            xs: 'repeat(1, 1fr)',
                            sm: 'repeat(2, 1fr)',
                            md: 'repeat(3, 1fr)',
                            lg: 'repeat(4, 1fr)',
                            xl: 'repeat(5, 1fr)',
                        }}
                        gridAutoRows={`minmax(${130}px, auto)`}
                        // gridTemplateRows={{ xs: 90, md: 100, lg: 112 }'1fr 1fr 1fr'}
                        display={'grid'}
                        overflow={'auto'}

                    // columns={3}
                    >
                        {/* <Grid display={'grid'} > */}
                        {list.map((l, id) => <WordsListItem key={l.id} {...l} />)}

                        {/* </Grid> */}
                        <Card
                            onClick={() => router.push(location.pathname + '/create')}
                            sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', cursor: 'pointer', background: 'none', border: '2px solid #47474c' }}>
                            {/* <CardContent sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}> */}
                            <Typography variant="h5" component="div">Новый список</Typography>
                            {/* </CardContent> */}
                        </Card>
                    </Grid>}
            </Grid>

        </Grid>
    )
}