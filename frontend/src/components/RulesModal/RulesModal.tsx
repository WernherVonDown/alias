import React, { ReactNode, useState } from "react";
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import styles from './RulesModal.module.scss';
import { SchoolIcon } from "../../icons/SchoolIcon";

interface IProps {
  children?: ReactNode;
}

export const RulesModal: React.FC<IProps> = ({ children }) => {
  const [open, setOpen] = useState(false);

  const onOpenModal = () => setOpen(true);
  const onCloseModal = () => setOpen(false);
  console.log("OPENN", open)

  return (
    <div>
      <div className={styles.openButton} onClick={onOpenModal}>{children ? children : <SchoolIcon />}</div>
      {open && <Modal  open={open} onClose={onCloseModal} center>
        <h2>Правила игры в Alias</h2>
        <h4>Разговаривать можно только на выбранном иностранном языке</h4>
        <div className={styles.text}>
          {`1. Участники деляться на команды разных цветов по два человека.\n
        2. Каждый раунд ходит следующуя команда.\n
        3. В команде поочередно меняются роли того, кто угадывает и кто объясняет\n
        4. Необходимо за ограниченное вермя угадать как можно больше слов.\n
        5. Тот, кто в команде объясняет должен говорить ТОЛЬКО НА ИНСОТРАННОМ ЯЗЫКЕ.\n
        6. Тот, кто угадывает, может угадывать на родном языке, но лучше на иностранном\n
        7. Побеждает та команда, которая наберет больше всех очков.\n`}
        </div>
      </Modal>}
    </div>
  );
}