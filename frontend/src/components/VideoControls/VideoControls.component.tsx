import { IconButton, SvgIcon } from "@mui/material";
import React, { useContext } from "react";
import styles from "./VideoControls.module.scss";
import { StreamContext } from "../../context/StreamContext";
import BlurOnIcon from '@mui/icons-material/BlurOn';
import BlurOffIcon from '@mui/icons-material/BlurOff';
import classNames from "classnames";
import { MinIcon } from "../../icons/MinIcon";
import { CamIcon } from "../../icons/CamIcon";
import { BlurIcon } from "../../icons/BlurIcon";
import { ExitIcon } from "../../icons/ExitIcon";

interface IProps {
    buttonClass?: string;
    buttonWrapperClass?: string;
    noExit?: boolean;
}

export const VideoControls: React.FC<IProps> = ({ buttonClass = '', noExit, buttonWrapperClass = '' }) => {
    const { actions: { setDevicePermissions, setBlurVideo }, state: { blurVideo, devicePermissions } } = useContext(StreamContext);
    return (
        <div className={classNames(styles.videoControls, buttonWrapperClass)}>
            <IconButton onClick={() => setBlurVideo((p: boolean) => !p)} className={classNames(styles.videoControlsItem, styles.videoControlsBlurBtn, buttonClass)} size={'medium'} aria-label="Example" >
                {blurVideo ? <BlurIcon /> : <BlurIcon />}
            </IconButton>
            <IconButton onClick={() => { setDevicePermissions((p: any) => ({ ...p, video: !p.video })) }} className={classNames(styles.videoControlsItem, buttonClass)} size={'medium'} aria-label="Example" >
                {devicePermissions.video ? <CamIcon active /> : <CamIcon />}

            </IconButton>
            <IconButton onClick={() => { setDevicePermissions((p: any) => ({ ...p, audio: !p.audio })) }} className={classNames(styles.videoControlsItem, buttonClass)} size={'medium'} aria-label="Example" >
                {/* <SvgIcon fontSize="large">{devicePermissions.audio ? <MinIcon active/> : <MinIcon/>}</SvgIcon> */}
                {devicePermissions.audio ? <MinIcon active /> : <MinIcon />}
            </IconButton>
            {!noExit && <IconButton className={classNames(buttonClass)} size='medium' href="/">
                <ExitIcon />
            </IconButton>}
        </div>
    )
}
