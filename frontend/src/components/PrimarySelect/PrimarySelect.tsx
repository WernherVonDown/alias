import React, { useMemo } from "react";
import styles from './PrimarySelect.module.scss'
import Select, {SelectItemRenderer, SelectProps, SelectRenderer} from 'react-dropdown-select';
import classNames from "classnames";

interface IProps<T> {
    options: T[];
    values: T[];
    onChange: (value: T[]) => void;
    itemRenderer?: ({
      item,
      itemIndex,
      props,
      state,
      methods
    }: SelectItemRenderer<T>) => JSX.Element;
    optionRenderer?: ({ item, props, state, methods }: SelectItemRenderer<T>) => JSX.Element;
    className?: string;
    dropdownHandleRenderer?: ({ props, state, methods }: SelectRenderer<T>) => JSX.Element;
    wrapperClassName?: string;
}


export const PrimarySelect: React.FC<IProps<any>> = (
    {options, values,wrapperClassName, itemRenderer, onChange, className, optionRenderer, dropdownHandleRenderer}
    ) => {
        const preparedOptions = useMemo(() => {
            return options.map(e => ({...e, label: e.label}))
        }, [options])
    return (
        <Select
            options={preparedOptions}
            values={values}
            itemRenderer={itemRenderer}
            onChange={onChange}
            className={classNames(styles.select,'browser-default',className)}
            optionRenderer={optionRenderer}
            dropdownHandleRenderer={dropdownHandleRenderer}
            dropdownGap={0}
            wrapperClassName={wrapperClassName}
            style={{width: '100%'}}
        />
    )
}