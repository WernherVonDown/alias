import { AppBar, Box, Button, Container, Link, Toolbar, Typography, styled, Grid, IconButton, SvgIcon } from "@mui/material";
import React, { ReactNode, useContext, useMemo } from "react";
import { RoomContext } from "../../context/RoomContext";
import { SwipeableEdgeDrawer } from "../MobileChat/MobileChat.component";
import styles from "./RoomLayout.module.scss"
import LogoutSharpIcon from '@mui/icons-material/LogoutSharp';
import { VideoControls } from "../VideoControls/VideoControls.component";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[];
}

export const RoomLayout: React.FC<IProps> = ({ children }) => {
    const { state: { users } } = useContext(RoomContext);
    const numberOfUsers = useMemo(() => {
        return users.length;
    }, [users.length]);

    return (
        <div className={styles.roomLayout}>
            <div className={styles.roomContent}>
                {children}
            </div>
            <Box sx={{ flexGrow: 1 }} display={{ md: 'none', sm: 'flex', lg: 'none' }}>
                <Offset />
                <AppBar position="static">
                    <SwipeableEdgeDrawer />
                </AppBar>
            </Box>
            {/* </Container> */}
        </div >
    )
}
