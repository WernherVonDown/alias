import { Box, Button, Grid, Input, MenuItem, Select, TextField, Typography } from "@mui/material"
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useState } from "react"
import { AddWordItem } from "./AddWordItem";
import { useInput } from "../../hooks/useInput";
import { Langs, LangsList } from "../../const/languages/LANGS";
import { CenterXY } from "../../common/CenterXY/CenterXY";
import { WordXWowIcon } from "../../icons/WordXWowIcon";
import { CreateGameBtn } from "../CreateGameBtn/CreateGameBtn";
import WordsListService from "../../services/wordsListService";
const DEFAULT_NAME = "Мой список слов";

export interface IWord {
    word: string,
    translation: string,
    id: string,
    new?: boolean,
}

interface IProps {
    words?: IWord[],
    listName?: string,
    language?: Langs,
    onSave: (listName: string, words: IWord[], language: Langs) => Promise<string>,
}

export const AddWordsList: React.FC<IProps> = ({
    words: initialWords = [],
    listName: initialListName = DEFAULT_NAME,
    language: initialLanguage = Langs.EN,
    onSave,
}) => {
    const [words, setWords] = useState(initialWords);
    const wordText = useInput('');
    const wordTrans = useInput('');
    const listName = useInput(initialListName);
    const language = useInput(initialLanguage);
    const [saveSuccess, setSaveSuccess] = useState(false);
    const router = useRouter()
    const [resultId, setResultId] = useState('')
    console.log("words", words)

    const onSaveWordsList = useCallback(async () => {
        try {
            if (!listName.value) {
                return alert('Заполните имя')
            }
            if (!words.length) {
                return alert('Список пустой')
            }
            console.log("onSaveWordsList")
            const resId = await onSave(listName.value, words, language.value as Langs);
            setResultId(resId)
            setSaveSuccess(true)
        } catch (error) {
            alert('Error')
        }
    }, [words, listName.value, language.value])

    const clearInputs = useCallback(() => {
        wordText.onChange({ target: { value: '' } });
        wordTrans.onChange({ target: { value: '' } });
    }, [wordText, wordTrans])

    const getTranslation = useCallback(async (word: any) => {
        try {
            const res = await WordsListService.getTranslation({
                word: word.word,
                translation: word.translation,
                language: language.value as Langs,
            })

            return {
                ...word,
                ...res.data,
            }
        } catch (error) {
            return word;
        }
        
    }, [language.value])

    const onAddWords = useCallback(async () => {
        if (!wordText.value && !wordTrans.value) {
            alert('Заполните слово')
            return;
        }
        console.log("ON MATCH", wordText.value, wordText.value.match(/[а-яА-ЯЁё]/ig))
        if(wordText.value && wordText.value.match(/[а-яА-ЯЁё]/ig)) {
            alert('Слово на русском введите в поле "Перевод"')
            return;
        }
        let word = {
            word: wordText.value,
            translation: wordTrans.value,
            id: Date.now().toString(),
            new: true,
        }
        if (!word.word || !word.translation) {
            word = await getTranslation(word);
        }
        
        if (!word.word) {
            return alert('Произошла ошибка, поробуйте слово самостоятельно')
        }
        setWords(w => [word, ...w])
        clearInputs()
    }, [wordText.value, wordTrans.value, clearInputs, getTranslation]);

    const onRemoveWord = useCallback((wordId: string) => {
        setWords(w => w.filter(e => e.id !== wordId))
    }, []);

    const onEditWord = useCallback((wordId: string, wordText: string, wordTrans: string) => {
        console.log("onEditWord", wordId, wordText, wordTrans)
        setWords(w => w.map(e => {
            if (e.id === wordId) {
                e.word = wordText;
                e.translation = wordTrans;
                e.new = true;
            }
            return e;
        }))
    }, []);

    const handleKeyDown = useCallback((event: any) => {
        if (event.key === 'Enter') {
            onAddWords()
        }
    }, [onAddWords]);

    if (saveSuccess) {
        return <CenterXY>
            <Grid
                display="flex"
                flexDirection={'column'}
                height={1}
                sx={{ background: 'rgba(255, 255, 255, 0.1)', borderRadius: '8px' }}
                overflow={'aut'}
                gap={3}
                padding={3}
            >
                <Grid
                    display="flex"
                    flexDirection={'column'} marginX={3}
                    gap={3}
                >
                    <Grid display={'flex'} justifyContent={'center'} alignItems={'center'}>
                        <WordXWowIcon />
                    </Grid>
                    <Grid display={'flex'} flexDirection={'column'} justifyContent={'space-between'} alignItems={'center'}>
                        <Typography variant="h5" color={'white'}>Список сохранен!</Typography>
                        <Typography variant="body1" color={'white'}>Можно начать игру!</Typography>
                    </Grid>
                </Grid>

                <Grid display={'flex'} flexDirection={'column'} gap={1} alignItems={'center'}>
                    <CreateGameBtn languageKey={language.value as Langs} wordsListId={resultId} fullWidth />
                    <Button fullWidth variant="outlined">
                        <Typography color={'white'} onClick={() => router.back()}>К списку слов</Typography>
                    </Button>
                </Grid>

            </Grid>
        </CenterXY>
    }


    return <Grid display="flex" flexDirection={'column'} height={1} overflow={'aut'} gap={1}>
        <Grid display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
            <Box>
                <Button onClick={() => router.back()}>
                    <Typography variant="h5" color={'white'}>&#9666; Назад</Typography>
                </Button>
            </Box>
            <Box gap={1} display={'flex'}>
                <Button variant="outlined">
                    <Typography color={'white'}>Загрузить из файла...</Typography>
                </Button>
                <Button onClick={onSaveWordsList} variant="contained" disabled={!words.length || !listName.value.length}>
                    <Typography >Сохранить список</Typography>
                </Button>
            </Box>
        </Grid>
        <Grid item display={'flex'} gap={0.7}>
            <TextField {...listName} fullWidth />
            <Select
                label={"Язык"}
                {...language}
            >
                {Object.values(LangsList).map(l => <MenuItem key={l.key} value={l.key}>{l.flag}</MenuItem>)}
            </Select>
        </Grid>
        <Grid display={'flex'} gap={1}>
            <TextField
                sx={{ background: 'white', input: { color: 'black' } }}
                placeholder={'Введите слово и нажмите enter'} fullWidth
                {...wordText}
                onKeyDown={handleKeyDown}
            />
            <TextField
                sx={{ background: 'white', input: { color: 'black' } }}
                placeholder={'Перевод'}
                fullWidth
                {...wordTrans}
                onKeyDown={handleKeyDown}
            />
            <Button variant="contained" onClick={onAddWords} disabled={!wordText.value}>+</Button>
        </Grid>
        <Grid
            height={1}
            width={1}
            sx={{}}
            gap={1}
            display={'flex'}
            flexDirection={'column'}
            overflow={'overlay'}
        >
            {words.map(w => <AddWordItem {...w} key={w.id} onRemoveWord={onRemoveWord} onEditWord={onEditWord} />)}
        </Grid>
    </Grid>
} 