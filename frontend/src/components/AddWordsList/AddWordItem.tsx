import { Button, Grid, TextField } from "@mui/material";
import React from "react";

interface IProps {
    word: string,
    translation?: string,
    id: string,
    onRemoveWord: (id: string) => void,
    onEditWord: (id: string, text: string, trans: string) => void,
}

export const AddWordItem: React.FC<IProps> = ({ word, translation = '', id, onRemoveWord, onEditWord }) => {

    return <Grid display={'flex'} gap={1}>
        <TextField
            sx={{ background: 'rgba(255, 255, 255, 0.1)', input: { color: 'white' } }} 
            value={word} 
            fullWidth 
            onChange={(e) => onEditWord(id, e.target.value, translation)}
            />
        <TextField
            sx={{ background: 'rgba(255, 255, 255, 0.1)', input: { color: 'white' } }}
            value={translation}
            fullWidth
            onChange={(e) => onEditWord(id, word, e.target.value)}
        />
        <Button variant="outlined" onClick={() => onRemoveWord(id)}>x</Button>
    </Grid>
}