import React from "react"
import styles from './Game.module.scss'
import Words from "./Words";
import { useContext, useEffect } from 'react';;
import { GameContext } from "../../context/GameContext";
import { Box, Button, Typography } from "@mui/material";
import { Video } from "../../common/Video/Video.component";
import { RoomContext } from "../../context/RoomContext";
import { VideoChatContext } from "../../context/VideoChatContext";
import { Langs, LangsList } from "../../const/languages/LANGS";
import { ConferenceVideo } from "../ConferenceVideo/ConferenceVideo.component";
import ProgressBarTimer from "../ProgressBarTimer/ProgressBar";


const Game = () => {
    const { state: { gameStarted, roundStarted, showWordStatus, currentTeam, meIsActive, currentUserId, gameEnded }, actions: gameActions } = useContext(GameContext);
    const { state: userState, actions: userActions } = useContext(RoomContext);
    const { state: { remoteStreams } } = useContext(VideoChatContext);
    const { state: { gameUsers: users } } = useContext(GameContext);
    const ready = true;


    const renderWordPanel = () => {
        if (gameEnded) {
            return <Box>
                <Typography variant="h4">Игра окончена!! Ура</Typography>
                <Typography textAlign={'center'} variant="h5">Победила дружба!</Typography>
            </Box>
        }
        if (meIsActive) {
            if (!gameStarted) {
                return (
                    <div className={styles.readyWrapper}>
                        <Button variant={'contained'} onClick={gameActions.startGame}>Начать игру</Button>
                    </div>
                )
            }
            if (!roundStarted) {
                return (
                    <Box display={'flex'} flexDirection={'column'} gap={2} justifyContent={'center'} alignItems={'center'}>
                        <Typography variant="h4">Твоя роль: спикер</Typography>
                        <Typography variant="h6" color={'#FFFFFF80'} textAlign={'center'}>Нажми старт, если готов(а) объяснять слова на {LangsList[userState.languageKey]?.flag}</Typography>
                        <Button size="large" variant={'contained'} onClick={gameActions.startRound}>Старт</Button>
                    </Box>
                )
            }

            return <Words />
        } else {
            if (!gameStarted) {
                return (
                    <div className={styles.readyWrapper}>
                        Ждём пока пользователь начнет игру...
                    </div>
                )
            }
            if (!ready) {
                return (
                    <div className={styles.readyWrapper}>
                        Ждём пока пользователь будет готов...
                    </div>
                )
            }

            if (!roundStarted) {
                return <div className={styles.gameVideosWrapper}>
                    <Typography variant="h4" className={styles.readyWrapper}>Приготовься</Typography>
                    <Typography variant="h5" className={styles.readyWrapper} textAlign={'center'}>Сейчас ты будешь отгадывать слова</Typography>
                </div>
            }

            const activeUser = users.find(u => u.id === currentUserId)
            const activeUsersStream = remoteStreams.find(r => activeUser?.id === r.userId);
            return <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                gap: '0.5rem',
                width: '100%'
            }}>
                <ProgressBarTimer />
                <Typography variant="h5" className={styles.readyWrapper} textAlign={'center'}>
                {showWordStatus ? `${showWordStatus.word?.word} ${showWordStatus.word?.translation ? '- ' : ''}${showWordStatus.word?.translation}`
                : '******'}
                </Typography>
                {activeUsersStream && <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center'
                    }}
                    width={{
                        lg: '60%',
                        md: '70%',
                        sm: '80%',
                        xs: '95%',
                    }}
                >
                        <ConferenceVideo isLocal={activeUser.id === userState.me?.userId} devices={activeUser.devices} noControls userName={activeUsersStream.userName} key={`video_${activeUsersStream.userId}`} stream={activeUsersStream.stream} />
                
                </Box>}

            </Box>
        }
    }

    const renderWordStatus = () => {
        console.log('renderWordStatus', showWordStatus)
        if (!showWordStatus) return null;
        const { word: { word, translation }, status } = showWordStatus;

        console.log('RENDER STATUS', showWordStatus)
        return (
            <div className={styles.wordStatusWrapper}>
                <div className={styles.wordStatus}>
                    {`${word} - ${translation}`}
                </div>
            </div>
        )
    }

    return <div className={styles.game}>
        <div className={styles.wordWrapper}>
            {renderWordPanel()}
            {/* {renderWordStatus()} */}
        </div>
    </div>
}

export default Game;