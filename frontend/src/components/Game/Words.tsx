import React, { ReactNode, useEffect, useMemo, useState } from "react"
import styles from './Game.module.scss'
import classNames from "classnames";
import ProgressBarTimer from '../ProgressBarTimer/ProgressBar';
import { useContext } from 'react';
import { IWord } from '../../const/word/types';
import { WordStatuses } from "../../const/word/WORD_STATUSES";
import { Swiper, SwiperSlide } from "swiper/react";
import 'swiper/swiper-bundle.min.css'
import 'swiper/swiper.min.css'
// import './swiper.scss'
import { useRef } from 'react';
import { GameContext } from "../../context/GameContext";
import { Box, Button, Grid, Typography } from "@mui/material";


const bgColors = ['red', 'amber', 'green', 'blue'];
let bgColorsCopy = [...bgColors];
let currentWordId: any;

const Words = (): any => {
    const { state: { words }, actions: { getWord, clearWord, sendWordStatus } } = useContext(GameContext)
    const [elements, setElments] = useState([{ text: 'la chanson - песня', bgColor: 'green', id: '1jnkj12' }, { text: 'le problème - проблема', bgColor: 'blue', id: 'sldfj323' }]);
    const swiperRef = useRef<any>()

    const getBgColor = (): string => {
        if (!bgColorsCopy.length) {
            bgColorsCopy = [...bgColors]
        }

        return 'amber'

        return bgColorsCopy.shift() as string;
    }

    const renderElement = (word: IWord, i: number) => {
        const { translation, id, transcription } = word;
        const wordText = word.infinitive || word.word
        console.log("WORD", word)
        return <SwiperSlide className={styles.swiperSlide} 
        key={id + i}>
            <div data-id={id} className={classNames(getBgColor(), styles.wordItem, 'carousel-item')}>
                {/* <Grid container direction="column" spacing={2}  fontSize={{xs: '12px', md: '25px',  lg: '25x'}}> */}
                <div className={styles.cardDataWrapper}>
                    <Box display={'flex'} flexDirection={'column'} gap={1}>
                        {wordText && <Typography variant="h4">{wordText}</Typography>}
                        {transcription && word.word !== transcription && <Typography variant="h5" color={'#FFFFFF80'}>{`[${transcription}]`}</Typography>}
                        {translation && <Typography variant="h4">{`${translation}`}</Typography>}
                    </Box>

                    <div className={styles.buttonWrapper}>
                        <Button fullWidth size="large" variant={'outlined'} onClick={() => {
                            sendWordStatus(word, WordStatuses.SKIPPED);
                            next()
                        }}>
                            Пропустить
                        </Button>
                        <Button fullWidth size="large" variant='contained' className={styles.buttonDone} onClick={() => {
                            sendWordStatus(word, WordStatuses.GUESSED);
                            next()
                        }}>{`Угадал(а)`}</Button>

                    </div>
                </div>


                {/* </Grid> */}
            </div></SwiperSlide>
    }


    const next = () => {
        console.log("NEXT", swiperRef.current)
        swiperRef.current?.slideNext()



    }

    const slides = useMemo(() => {
        return words.map(renderElement)
    }, [words])

    return (
        <div className={styles.carouselWrapper}>
            <Box height={'100%'} display={'flex'} flexDirection={'column'} alignItems={'center'} justifyContent={'center'} gap={6}>
                <div className={styles.progressBarWrapper}>
                    <ProgressBarTimer />
                </div>

                <Swiper
                    style={{
                        height: 'unset !important'
                    }}
                    allowSlidePrev={false}
                    onSwiper={(swiper) => {
                        console.log("SWIPER", swiper)
                        swiperRef.current = swiper;
                    }}
                    className={styles.mySwiper}
                    height={0}
                    onSlideChange={(e) => console.log('onSlideChange', e)}
                    onSlideNextTransitionStart={(e) => console.log('onSlideNextTransitionStart', e)}
                    onError={(e) => console.log('onError', e)}
                >
                    {slides}
                </Swiper>
            </Box>



        </div >
    );
}

export default Words;