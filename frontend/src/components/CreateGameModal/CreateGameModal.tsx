import React, { ReactNode, useState } from "react";
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
// import styles from './RulesModal.module.scss';
import { SchoolIcon } from "../../icons/SchoolIcon";
import { Grid } from "@mui/material";
import { CreateGameModalContent } from "./CreateGameModalContent";
import { Langs } from "../../const/languages/LANGS";

interface IProps {
    children?: ReactNode;
    wordsListId?: string;
    languageKey?: Langs;
    fullWidthВtn?: boolean;
}

export const CreateGameModal: React.FC<IProps> = ({ children, wordsListId, languageKey, fullWidthВtn }) => {
    const [open, setOpen] = useState(false);

    const onOpenModal = () => setOpen(true);
    const onCloseModal = () => setOpen(false);
    return (
        <> 
            <div className={'styles.openButton'} style={fullWidthВtn ? {width: '100%'} : {}} onClick={onOpenModal}>{children ? children : <SchoolIcon />}</div>
            {open && <Modal open={open} onClose={onCloseModal} styles={{modal: {background: 'var(--secondary-background)'}}} center>
                <CreateGameModalContent languageKey={languageKey} listId={wordsListId} />
            </Modal>}
        </>
    );
}