import React, { useCallback, useContext, useEffect, useMemo, useState } from "react";
import 'react-responsive-modal/styles.css';
import { Box, Button, Checkbox, Chip, Grid, MenuItem, OutlinedInput, Select, TextField, Typography } from "@mui/material";
import { AuthContext } from "../../context/AuthContext";
import { Langs, LangsList } from "../../const/languages/LANGS";
import WordsListService from "../../services/wordsListService";
import { IWordsList } from "../MyWords/MyWords";
import { useInput } from "../../hooks/useInput";
import { Theme, useTheme } from '@mui/material/styles';
import RoomService from "../../services/roomService";
import { useRouter } from "next/router";
import { SmileIcon } from "../../icons/SmileIcon";
import Link from "next/link";
import { RegistrationRoute } from "../../const/API_ROUTES";
import { sendTrackMessage } from "../../utils/userTracker";

interface IProps {
    languageKey?: Langs,
    listId?: string,
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

function getStyles(name: string, slectedLists: readonly string[], theme: Theme) {
    return {
        fontWeight:
            slectedLists.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

export const CreateGameModalContent: React.FC<IProps> = ({ listId, languageKey = Langs.EN }) => {
    const { state: { loggedIn, user } } = useContext(AuthContext);
    const [myLists, setMyLists] = useState<IWordsList[]>([]);
    const [publicLists, setPublicLists] = useState<IWordsList[]>([]);
    const [slectedLists, setSelectedLists] = useState<string[]>([]);
    const language = useInput(languageKey);
    const theme = useTheme();
    const [isPublic, setIsPublic] = useState(false)
    const gameName = useInput('');
    const router = useRouter()
    const [selectListStep, setSelectListStep] = useState(false);

    useEffect(() => {
        getPublicLists()
    }, [])

    useEffect(() => {
        if (loggedIn) {
            getMyLists()
        } else {
            setMyLists([])
        }
    }, [loggedIn]);

    useEffect(() => {
        if (listId && myLists.length && myLists.find(l => l.id)) {
            setSelectedLists(l => [...l, listId]);
        }
    }, [listId, myLists.length]);

    const getPublicLists = useCallback(async () => {
        try {
            const res = await WordsListService.getPublic({});
            setPublicLists(res.data)
        } catch (error) {
            console.log('getPublicLists error', error)
        }
    }, [])

    const getMyLists = useCallback(async () => {
        try {
            const res = await WordsListService.get({});
            setMyLists(res.data)
        } catch (error) {
            console.log('getMyLists error', error)
        }
    }, []);

    const filteredPublicLists = useMemo(() => {
        return publicLists.filter(l => l.languageKey === language.value)
    }, [publicLists, language.value]);

    const filteredMyLists = useMemo(() => {
        return myLists.filter(l => l.languageKey === language.value)
    }, [myLists, language.value]);

    const slectedListsMyFiltered = useMemo(() => {
        return slectedLists.filter(lId => filteredMyLists.find(l => l.id === lId))
    }, [slectedLists, filteredMyLists]);

    const slectedListsPublicFiltered = useMemo(() => {
        return slectedLists.filter(lId => filteredPublicLists.find(l => l.id === lId))
    }, [slectedLists, filteredPublicLists]);

    const filteredCommon = useMemo(() => {
        return filteredMyLists.concat(filteredPublicLists)
    }, [filteredMyLists, filteredPublicLists])

    const selectedCommon = useMemo(() => {
        return slectedListsPublicFiltered.concat(slectedListsMyFiltered)
    }, [slectedListsPublicFiltered, slectedListsMyFiltered])

    const onCreateGame = useCallback(async () => {
        try {
            const allLists = filteredPublicLists.concat(filteredMyLists)
            const wordListsFull = allLists.filter(l => selectedCommon.includes(l.id))
            const wordLists = wordListsFull.map(l => l.id);
            if (!wordLists.length) return alert('Выберите список');
            if (isPublic && !gameName.value.trim()) return alert('Имя обязательн');
            console.log("SEND TRACK")
            const res = await RoomService.create({
                languageKey: language.value as Langs,
                isPublic,
                name: isPublic ? gameName.value : undefined,
                wordLists,
                userId: user?.id,
            });
            const message = `Новая игра ${language.value}: \n${wordListsFull.map(l => l.name).join(', ')}${user?.id ? `\nПльзователь: ${user?.id}` : ''}`
            sendTrackMessage({message})
            const roomId = res.data.roomId;
            if (roomId) {
                router.push(`/rooms/${roomId}`)
            }
        } catch (error) {
            console.log('Error create room')
        }
    }, [filteredCommon, language.value, selectedCommon, isPublic, gameName, user?.id])

    if (!selectListStep) {
        return (
            <Grid display={'flex'} padding={1} sx={{ color: 'white' }} width={{
                sx: '100%',
            }} gap={2} direction={'column'}>
                
                <Grid display={'flex'} gap={1} alignItems={'center'} flexDirection={'column'}>
                    <SmileIcon variant={1} />
                    <Typography variant="h5" color={'white'}>Выбери язык игры</Typography>
                    <Typography textAlign={'center'}>На каком языке ты будешь объяснять слова?</Typography>
                </Grid>
                <Grid display={'flex'} gap={1} alignItems={'center'}>
                    <Select
                        fullWidth
                        {...language}
                        size="small"
                    >
                        {Object.values(LangsList).map(l => <MenuItem key={l.key} value={l.key}>{l.flag} {l.label}</MenuItem>)}
                    </Select>
                </Grid>
                <Grid justifyContent={'end'} display={'flex'}>
                    <Button size={'large'} onClick={() => setSelectListStep(true)} variant="outlined" fullWidth>
                        Далее
                    </Button>
                </Grid>
            </Grid>
        );
    }
    return (
        <Grid display={'flex'} padding={1} sx={{ color: 'white' }} width={{
            sx: '100%',

        }} gap={2} maxWidth={400} direction={'column'}>
            <Button variant="outlined" onClick={() => setSelectListStep(false)} sx={{position: 'absolute', minWidth: 'unset', left: 10, top: '10px', width: 35, height: 35}} >&#9664;</Button>
            <Grid display={'flex'} gap={1} alignItems={'center'} flexDirection={'column'}>
                <SmileIcon />
                <Typography variant="h5" color={'white'}>Выбери пак слов</Typography>
                <Typography textAlign={'center'}>Можно выбрать несколько из списка</Typography>
            </Grid>


            {!filteredMyLists.length && loggedIn && <Grid display={'flex'} gap={1} alignItems={'center'}>

                <Typography color={'white'} textAlign={'center'}>Вы можете использовать свои слова</Typography>
                <Button variant="contained" href="/my-words">
                    Добавить
                </Button>
            </Grid>}
            {!!filteredCommon.length && <Grid maxWidth={'100%'}>
                <Select
                    size="small"
                    multiple
                    fullWidth
                    
                    sx={{ color: 'white' }}
                    value={selectedCommon}
                    onChange={(e) => {
                        console.log("ONCHANGE", e.target.value)
                        setSelectedLists(e.target.value as string[])
                    }}
                    renderValue={(selected) => (
                        <Box sx={{  gap: 0.5, whiteSpace: 'unset',display: 'flex', flexWrap:'wrap' }}>
                            {selected.map((value) => (
                                <Chip key={value} variant="filled" sx={{
                                    background: 'var(--primary)'
                                }} label={`${filteredCommon.find(l => l.id === value)?.name || ''} (${filteredCommon.find(l => l.id === value)?.wordsNum})`} />
                            ))}
                        </Box>
                    )}
                    MenuProps={MenuProps}
                >
                    {filteredCommon.map((list) => (
                        <MenuItem
                            key={list.id}
                            value={list.id}
                            style={getStyles(list.name, slectedLists, theme)}
                        >
                            {list.name} ({list.wordsNum})
                        </MenuItem>
                    ))}
                </Select>
            </Grid>}
            {isPublic && <Grid display={'flex'} gap={1} alignItems={'center'}>
                <TextField {...gameName} placeholder="Название игры*" />
            </Grid>}
            <Grid justifyContent={'end'} display={'flex'}>
                <Button variant="contained" onClick={onCreateGame} fullWidth>
                    Играть!
                </Button>
            </Grid>
            {!loggedIn && <Grid  display={'flex'} justifyContent={'center'}>
                <Typography textAlign={'center'} fontFamily="Inter" className="primary"><Link href={RegistrationRoute}>Войди,</Link> чтобы использовать свой список</Typography>
            </Grid>}
        </Grid>
    );
}