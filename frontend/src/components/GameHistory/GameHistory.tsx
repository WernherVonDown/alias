import React from "react"
import styles from './GameHistory.module.scss';
import { useContext } from 'react';
import { ITeamScore } from '../../const/game/types';
import { GameContext } from "../../context/GameContext";
import { RoomContext } from "../../context/RoomContext";
import { Box, Typography } from "@mui/material";
import ReactTextTransition from "react-text-transition";

const GameHistory = () => {
    const { state: gameState } = useContext(GameContext);
    const { state: { gameUsers: users } } = useContext(GameContext);
    console.log("users", users)
    return <div className={styles.gameHistory}>
        {gameState.teamsScores.sort((a, b) => b.score - a.score).map(({ teamColor, score }: ITeamScore, i: number) => {
            const usersFiltered = users.filter(u => u.teamColor === teamColor);
            const teamActive = gameState.currentTeam === teamColor;
            return <Box key={`teamScore_${teamColor}`}
                sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    background: ' var(--secondary-background)',
                    border: '1px solid rgba(255, 255, 255, 0.05)',
                    borderRadius: '8px',
                    flexDirection: 'column',
                    padding: '0.5rem',
                    boxShadow: teamActive ? `0px 0px 0px 2px ${teamColor}` : 'unset'
                }}
            >
                <Box sx={{
                    display: 'flex',
                    width: '100%',
                    justifyContent: 'space-between',
                }}>
                    <Typography variant="h5" color={teamColor}>
                        Team {i + 1}
                    </Typography>
                    <Typography variant="h5" color={teamColor}>
                    <ReactTextTransition>{score}</ReactTextTransition>
                    </Typography>
                </Box>
                <Box sx={{
                    display: 'flex',
                    width: '100%',
                    flexDirection: 'column',
                    height: '100%',
                    justifyContent: 'space-evenly',
                    maxWidth: '100%',
                    textOverflow: 'ellipsis',
                    overflow: 'hidden'
                }}>
                    {usersFiltered.map(u => (
                        <Typography
                            key={`user_${u.userId}`}
                            sx={{
                            maxWidth: '100%',
                            textOverflow: 'ellipsis',
                            overflow: 'hidden'
                        }} >{u.userName}</Typography>
                    ))}
                </Box>
            </Box>
        })}
    </div>
}

export default GameHistory;