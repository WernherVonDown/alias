import React, { useRef, useEffect } from "react"
import { IMessage } from '../../const/textChat/types';
import Message from './Message';
import styles from './Chat.module.scss';
import { useContext } from 'react';
import classNames from "classnames";
import { NoMsgEmoji } from "../../icons/NoMgsEmoji";
import { TextChatContext } from "../../context/TextChatContext";

const MessagesList = () => {
  const { state: { messages } } = useContext(TextChatContext);

  const messagesEndRef = useRef<null | HTMLDivElement>(null)

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
  }

  useEffect(() => {
    scrollToBottom()
  }, [messages]);

  console.log("MESSAGES", messages)

  return <div className={classNames(styles.chatMessageList, {[styles.noMsg]: !messages.length})}>
    {!messages.length && <div className={styles.noMsgEmoji}>
      <NoMsgEmoji />
      <div>
      Пока нет сообщений
      </div>
    </div>}
    {messages.map(({ userName, text, isMe }: IMessage | any, id: number) => (
      <Message key={id + '_message'} userName={userName} text={text} isMe={isMe} />
    ))}
    <div ref={messagesEndRef} />
  </div>
}

export default MessagesList;