import React from "react"
import MessagesList from "./MessagesList";
import styles from './Chat.module.scss';
import { useContext } from 'react';
import { useInput } from "../../hooks/useInput";
import { TextChatContext } from "../../context/TextChatContext";
import { TextField } from "@mui/material";
import { SendIcon } from "../../icons/SendIcon";
import { PrimaryInput } from "../PrimatyInput/PrimaryInput";

const inputStyle = { WebkitBoxShadow: "0 0 0 1000px #181820 inset" };

export const Chat = () => {
    const message = useInput('');

    const { actions: chatActions } = useContext(TextChatContext);

    const sendMessage = () => {
        chatActions.sendMessage(message.value);
        console.log('EEEE', message)
        message.onChange({target: {value: ''}})
    }
    // translate(MESSAGES_KEYS.enter_your_message)
    const handleKeyDown = (e: any) => {
        console.log("HANDLE")
        if (e.key == 'Enter') {
            console.log('AAAAAAAAAa', message.value)
            sendMessage()
        }
    }

    return <div className={styles.chatWrapper}>
        <MessagesList />
        <div className={styles.chatInputWrapper}>
        <PrimaryInput className={styles.input} type="text" onKeyDown={handleKeyDown} placeholder={'сообщение'} {...message} />
            <div className={styles.send} onClick={sendMessage}>
                <SendIcon />
            </div>
        </div>
    </div>
}