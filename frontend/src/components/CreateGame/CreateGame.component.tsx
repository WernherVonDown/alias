import { Button, Grid, Paper } from "@mui/material";
import { useRouter } from "next/router";
import React, { useCallback, useState } from "react";
import { Langs } from "../../const/languages/LANGS";
import { SwapHorizIcon } from "../../icons/SwapHorizIcon";
import RoomService from "../../services/roomService";
import { PrimarySelect } from "../PrimarySelect/PrimarySelect";
import styles from './CreateGame.module.scss'

const options = [
    {
        value: Langs.EN,
        // locKey: MESSAGES_KEYS.english,
        label: `🇺🇸 Английский`
    },
    {
        value: Langs.FR,
        // locKey: MESSAGES_KEYS.french,
        label: `🇫🇷 Французский`
    },
    {
        value: Langs.DE,
        // locKey: MESSAGES_KEYS.german,
        label: `🇩🇪 Немецкий`
    }
]
const optionsMother = [
    {
        value: Langs.EN,
        // locKey: MESSAGES_KEYS.russian,
        label: `🇷🇺 Русский`
    }
]

export const CreateGame: React.FC = () => {
    const [foreignLang, setForeign] = useState(Langs.EN);
    const [motherLang, setMotherLang] = useState(Langs.EN);
    const router = useRouter();
    const enterRoom = useCallback(async () => {
        // try {
        //     console.log("LOLO", foreignLang, motherLang)
        //   const res = await RoomService.create({
        //     langs: {
        //         foreignLang,
        //         motherLang
        //     }
        //   });
        //   const roomId = res.data.roomId;
        //   if (roomId) {
        //     router.push(`/rooms/${roomId}`)
        //   }
        // } catch (error) {
        //   console.log('Error create room')
        // }
      }, [foreignLang, motherLang])

    return (
        <div className={styles.createGame}>
            <div className={styles.createGameTitle}>
            Выбери языки
            </div>
            <div className={styles.selectLangPairWrapper}>

                <div className={styles.selectWrapper}>
                    <PrimarySelect
                        className={styles.select}
                        options={options}
                        dropdownHandleRenderer={(e) => <span style={{ width: 0 }} />}
                        values={[options.find(e => e.value === foreignLang) || options[0]]}
                        onChange={(e) => { setForeign(e[0].value) }}
                    />

                </div>
                <div><SwapHorizIcon /></div>
                <div className={styles.selectWrapper}>
                    <PrimarySelect
                        className={styles.select}
                        // wrapperClassName={styles.selectWrapper}
                        dropdownHandleRenderer={(e) => <span style={{ width: 0 }} />}
                        options={optionsMother}
                        values={[optionsMother[0]]}
                        onChange={(e) => { }}
                    />
                </div>
            </div>
            <Button style={{padding: '0.6rem'}} onClick={enterRoom} variant={'contained'}>Погнали!</Button>
        </div>
    )

    return (

        <Grid container display="flex" direction="column">
            {/* <Paper> */}
            <Grid item>
                <PrimarySelect
                    className={styles.select}
                    options={options}
                    dropdownHandleRenderer={(e) => <span style={{ width: 0 }} />}
                    values={[options.find(e => e.value === foreignLang) || options[0]]}
                    onChange={(e) => { setForeign(e[0].value) }}
                />
                <SwapHorizIcon />
                <PrimarySelect
                    className={styles.select}
                    // wrapperClassName={styles.selectWrapper}
                    dropdownHandleRenderer={(e) => <span style={{ width: 0 }} />}
                    options={optionsMother}
                    values={[motherLang]}
                    onChange={(e) => { }}
                />
            </Grid>

            {/* </Paper> */}
        </Grid>

    )
}