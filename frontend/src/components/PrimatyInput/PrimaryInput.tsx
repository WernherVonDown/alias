import classNames from "classnames";
import React from "react";
import styles from './PrimaryInput.module.scss'

interface IProps {
    placeholder?: any;
    type?: string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement> | any) => void;
    value?: string | number;
    id?: string;
    className?: string;
    clear?: () => void;
    translatedPlaceholder?: string;
    onKeyDown?: (e: any) => void;
}

export const PrimaryInput: React.FC<IProps> = (props) => {
    const { className, translatedPlaceholder, clear, onKeyDown, ...inputProps } = props;

    if (translatedPlaceholder) {
        return <input
            tabIndex={0}
            onKeyDown={onKeyDown}
            {...{ ...inputProps }}
            className={classNames(styles.input, 'browser-default', className)}
        />
    }
    return (
        <input {...inputProps} onKeyDown={onKeyDown} className={classNames(styles.input, 'browser-default', className)} />
    )
}