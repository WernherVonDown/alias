import React, { ReactElement, useState } from "react";
import Donate from "../Donate/Donate";
import Modal from "react-responsive-modal";

interface IProps {
    children: ReactElement | ReactElement[]
}

export const DonateModal: React.FC<IProps> = ({children}) => {
    const [open, setOpen] = useState(false);

    return <>
    <span onClick={() => setOpen(true)}>{children || 'Donate'}</span>
   {open && <Modal open={open} onClose={() => setOpen(false)} styles={{ modal: { background: 'black' } }} center>
        <Donate />
    </Modal>}</>
}