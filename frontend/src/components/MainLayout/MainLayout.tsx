import React, { ReactNode, useContext } from "react";
import { AppBar, Box, Button, Container, IconButton, styled, SvgIcon, Toolbar, Typography } from "@mui/material";
import { LoginRoute, RegistrationRoute } from "../../const/API_ROUTES";
import Link from "next/link";
import { AuthContext } from "../../context/AuthContext";
import { ThemeContext } from "../../context/ThemeContext";
import { LogoIcon } from "../../icons/LogoIcon";
import { ShortLogoIcon } from "../../icons/ShortLogoIcon";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { Footer } from "../../Footer/Footer";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);
interface IProps {
    children: ReactNode | ReactNode[];
    detachedFooter?: boolean;
}

export const MainLayout: React.FC<IProps> = ({ children, detachedFooter }) => {
    const { state: { loggedIn, user }, actions: { logout } } = useContext(AuthContext);
    const { state: { theme } } = useContext(ThemeContext);
    const themeHook = useTheme();
    const matches = useMediaQuery(themeHook.breakpoints.down('sm'));
    return (
        <Box sx={{ flexDirection: 'column' }} width={'100%'} position={'fixed'} display={'flex'} height="100%">
            <AppBar position="fixed" sx={{ bgcolor: "var(--background)", backgroundImage: 'none', boxShadow: 'unset' }}>
                <Toolbar>
                    <Box sx={{ flexGrow: 1, display:'inline-flex' }}>
                        <Button size='medium' href="/">
                            {matches ? <ShortLogoIcon /> : <LogoIcon />}
                        </Button>
                    </Box>
                    <Box columnGap={1} display="flex">
                        {!loggedIn ? <>
                            <Button size="large" href={RegistrationRoute} color="inherit" variant='outlined' >
                                Регистрация
                            </Button>
                            <Button size="large" href={LoginRoute} variant='contained'>
                                Вход
                            </Button>
                        </> : <>
                            <Button size="large" href="/my-words">
                                Мои слова
                            </Button>
                            <Button size="large" onClick={logout} variant='outlined'>
                                Выйти
                            </Button>
                        </>
                        } </Box>

                </Toolbar>
            </AppBar>
            {<Offset />}
            {/* <Box sx={{ height: '100%', display: 'flex', flexDirection: 'column' }} display='flex' flex={1}> */}
                <Box sx={{ px: '2rem', flexDirection: 'column' }} height={'100%'} display='flex' flex={1} overflow="auto">
                    {/* <Container fixed> */}
                    
                    {children}
                    {/* </Container> */}
                    {detachedFooter && <AppBar position="static" sx={{ bgcolor: "var(--background)", backgroundImage: 'none', boxShadow: 'unset' }}>
                        <Footer />
                    </AppBar>}
                </Box>
                {!detachedFooter && <Box sx={{ px: '2rem' }}>
                    <AppBar position="static" sx={{ bgcolor: "var(--background)", backgroundImage: 'none', boxShadow: 'unset' }}>
                        <Footer />
                    </AppBar>
                </Box>}
            {/* </Box> */}
        </Box>
    )
}