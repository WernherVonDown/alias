import { SvgIcon, Typography } from "@mui/material";
import React from "react";
import { Video } from "../../common/Video/Video.component";
import styles from "./ConferenceVideo.module.scss";
import MicOffIcon from '@mui/icons-material/MicOff';
import VideocamOffIcon from '@mui/icons-material/VideocamOff';
import { IDevicePermissions } from "../../const/videoChat/types";
import classNames from "classnames";
import { VideoControls } from "../VideoControls/VideoControls.component";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

interface IProps {
    stream: MediaStream;
    isLocal?: boolean;
    userName?: string;
    devices: IDevicePermissions;
    noControls?: boolean;
}

export const ConferenceVideo: React.FC<IProps> = ({ stream, isLocal, userName, noControls, devices = { video: true, audio: true } }) => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <div className={styles.conferenceVideoWrapper}>
            {<Video stream={stream} className={classNames({ [styles.mirrored]: isLocal })} muted={isLocal || !devices.audio} />}
            {!devices.video && <div className={styles.disabledCameraPoster}>{isLocal ? <SvgIcon fontSize="large"><VideocamOffIcon /></SvgIcon> : userName}</div>}
            {!devices.audio && !isLocal && <div className={styles.disabledMicIcon}><SvgIcon fontSize="large"><MicOffIcon /></SvgIcon></div>}
            {isLocal && !matches && !noControls && <VideoControls noExit />}
            {!isLocal && <div className={styles.userName}>
                <Typography
                    sx={{
                        maxWidth: '100%',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden'
                    }}
                    color={'white'}>{userName}</Typography>
            </div>}
        </div>
    )
}
