import * as React from 'react';
import { Global } from '@emotion/react';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import Typography from '@mui/material/Typography';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { Chat } from '../TextChat/Chat';
import LogoutSharpIcon from '@mui/icons-material/LogoutSharp';
import { TextChatContext } from '../../context/TextChatContext';
import { IconButton, SvgIcon } from '@mui/material';

const drawerBleeding = 56;

interface Props {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window?: () => Window;
}

const Root = styled('div')(({ theme }) => ({
    height: '100%',
    backgroundColor:
        theme.palette.mode === 'light' ? grey[100] : theme.palette.background.default,
}));

const StyledBox = styled(Box)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'light' ? '#fff' : theme.palette.background.paper,
}));

const Puller = styled(Box)(({ theme }) => ({
    width: 30,
    height: 6,
    backgroundColor: theme.palette.mode === 'light' ? grey[300] : grey[900],
    borderRadius: 3,
    position: 'absolute',
    top: 8,
    left: 'calc(50% - 15px)',
}));

export const SwipeableEdgeDrawer = (props: Props) => {
    const { window } = props;
    const [open, setOpen] = React.useState(false);
    const { state: { messages } } = React.useContext(TextChatContext);
    const [hasUnreadMessage, setHasUnreadMessage] = React.useState(false);
    const [messagesCount, setMessagesCount] = React.useState(0);


    const toggleDrawer = (newOpen: boolean) => () => {
        setOpen(newOpen);
    };

    React.useEffect(() => {
        if (open) {
            setMessagesCount(messages.length)
        }
    }, [messages.length, open])

    React.useEffect(() => {
        setHasUnreadMessage(!open && messages.length !== messagesCount)
    }, [messages.length, open, messagesCount])

    // This is used only for the example
    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <Root >
            <CssBaseline />
            <Global
                styles={{
                    '.MuiDrawer-root > .MuiPaper-root': {
                        height: `calc(50% - ${drawerBleeding}px)`,
                        overflow: 'visible',
                    },
                }}
            />
            {/* <Box sx={{ textAlign: 'center', pt: 1 }}>
        <Button onClick={toggleDrawer(true)}>Open</Button>
      </Box> */}
            <SwipeableDrawer
                container={container}
                anchor="bottom"
                open={open}
                onClose={toggleDrawer(false)}
                onOpen={toggleDrawer(true)}
                swipeAreaWidth={drawerBleeding}
                disableSwipeToOpen={false}
                ModalProps={{
                    keepMounted: true,
                }}
            >
                <StyledBox
                    sx={{
                        position: 'absolute',
                        top: -drawerBleeding,
                        borderTopLeftRadius: 8,
                        borderTopRightRadius: 8,
                        visibility: 'visible',
                        right: 0,
                        left: 0,
                    }}
                    display={{ md: 'none', sm: 'flex', lg: 'none' }}
                >
                    <Puller />
                    {/* <div style={{ display: 'flex', position: 'relative', justifyContent: 'space-between', marginRight:'0.3rem' }} onClick={() => {console.log("CLICK")}}> */}
                    <div style={{ display: 'flex', position: 'relative', width: 'fit-content' }}>
                        {hasUnreadMessage && <div style={{
                            display: 'flex',
                            position: 'absolute',
                            right: '0.5rem',
                            top: '0.9rem',
                            background: 'var(--primary)',
                            width: '10px',
                            height: '10px',
                            borderRadius: '50%',
                        }} />}
                        <Typography sx={{ p: 2, color: 'text.secondary' }}>Чат</Typography>
                        </div>
                        {/* <IconButton size='medium' style={{position: 'absolute', zIndex: 3223232, right: 0}} onClick={() => {console.log("CLICK")}} href="/">
                            <SvgIcon fontSize={"medium"}><LogoutSharpIcon /></SvgIcon>
                        </IconButton> */}
                    {/* </div> */}
                    {/* <Box display="flex" width={1} height={1} >
                        <div onClick={() => {console.log("CLICK")}} style={{background: 'blue !important'}}>
                        <IconButton href="/" onClick={() => {console.log("CLICK")}} size='medium' sx={{color: '#ff0000', background: 'green'}}>
                            <SvgIcon fill='#ff0000' fontSize={"large"} sx={{color: '#ff0000'}}><LogoutSharpIcon fill='#ff0000' /></SvgIcon>
                        </IconButton></div>
                    </Box> */}

                </StyledBox>
                <StyledBox
                    sx={{
                        px: 0,
                        pb: 0,
                        height: '100%',
                        overflow: 'auto',
                    }}
                >
                    {/* <Skeleton variant="rectangular" height="100%" /> */}
                    <Chat />
                </StyledBox>
            </SwipeableDrawer>
        </Root>
    );
}