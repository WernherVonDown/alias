import Tanalytics from "tanalytics-sdk";
import { generateRandomString } from "./generateRandomString";
import { config } from "../config/config";

const checkIsBot = ({ req }: any) => /bot|googlebot|crawler|spider|robot/i.test(req.headers['user-agent'] || '');

const getTrackId = ({ req, res, ipAddress }: any) => {
    let trackId = req.cookies.trackId;
    let trackNew = false
    if (!trackId) {
        const oneDatInSeconds = 24 * 60 * 60;
        const expiresDate = new Date();
        expiresDate.setSeconds(expiresDate.getSeconds() + oneDatInSeconds);
        const hiddenIp = ipAddress ? btoa(ipAddress) : "";
        trackId = hiddenIp || generateRandomString(30);
        res.setHeader('Set-Cookie', `trackId=${trackId}; Path=/; HttpOnly Max-Age=${oneDatInSeconds}; Expires=${expiresDate.toUTCString()}`);
        trackNew = true;
    }
    return {trackId, trackNew};
}

export const trackUserVisit = async ({ req, res }: any) => {
    try {
        const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const tanalytics = new Tanalytics(config.tanalyticsToken)
        const {trackId, trackNew} = getTrackId({ req, res, ipAddress })
        if (checkIsBot({ req })) {
            return;
        }
        tanalytics.sendVisit({ userId: trackId });
        
    } catch (error) {
        console.log('trackUserVisit error', error)
    }
}

export const sendTrackMessage = async ({message}: {message: string}) => {
    try {
        const tanalytics = new Tanalytics(config.tanalyticsToken)
            tanalytics.sendMessage({ message });
    } catch (error) {
        console.log('sendTrackMessage error', error)
    }
}