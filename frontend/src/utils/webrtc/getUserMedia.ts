import { Constraints } from './getConstraints';
import { DeviceTypes } from '../../const/webrtc/DEVICE_TYPES';

export async function getUserMedia(constraints: Constraints[]): Promise<MediaStream> {
    const copy = [...constraints];
    try {
        const constraint = copy.shift();
        const stream = await navigator.mediaDevices.getUserMedia(constraint);
        console.warn('getUserMedia:success', constraint, stream);
        return stream;
    } catch (e) {
        // eslint-disable-next-line no-console
        console.warn('getUserMedia; error;', e);
        throw e;
    }
}

export async function getUserMediaDevices(): Promise<MediaDeviceInfo[]> {
    try {
        const devices = await navigator.mediaDevices.enumerateDevices();
        return devices;
    } catch (e) {
        // eslint-disable-next-line no-console
        console.warn('getUserMediaDevices; error;', e);
        return [];
    }
}

export function getUserMediaDeviceInfo({
    devices,
    kind,
    deviceId,
}: {
    devices: MediaDeviceInfo[];
    kind: DeviceTypes;
    deviceId?: string;
}): MediaDeviceInfo | undefined {
    if (deviceId) {
        const device = devices.find((d) => d.kind === kind && d.deviceId === deviceId);
        if (device) {
            return device;
        }
    }

    const defaultDevice = devices.find((d) => d.kind === kind && d.deviceId === 'default');
    if (defaultDevice) {
        return defaultDevice;
    }

    return devices.find((d) => d.kind === kind);
}
