import 'webrtc-adapter';
import { CONNECTION_TYPE } from '../../const/webrtc/CONNECTION_TYPE';
import { TRACK_KIND } from '../../const/webrtc/TRACK_KIND';
import { STREAM_TYPE } from '../../const/webrtc/STREAM_TYPE';
import { IWebRtcConnection } from '../../const/webrtc/types';
import { changeTracksState } from './changeTracksState';

export class WebRtcConnection {
  type: string;
  connectionId: string;
  userId: string;
  iceServers?: RTCIceServer[];
  onGotOffer: (args: { sdp?: string; connectionId: string }) => void;
  onGotStream: (args: { stream: MediaStream }) => void;
  onGotCandidate: (args: {
    connectionId: string;
    candidate: RTCIceCandidate;
  }) => void;
  peerConnection?: RTCPeerConnection;
  stream?: MediaStream | null;
  memberStream?: MediaStream | null;
  sdpAnswerSet?: boolean;
  onIceConnectionStateFailed: () => void;
  onIceConnectionStateDisconnected: () => void;
  onDisconnected?: () => void;
  streamType: STREAM_TYPE;
  devicePermissions: { audio: boolean, video: boolean };
  devices?: MediaDevices;
  candidateQueue: RTCIceCandidate[];
  initial?: boolean;
  sdpAnswer: string | null;

  constructor(data: IWebRtcConnection) {
    this.connectionId = data.connectionId;
    this.type = data.type;
    this.streamType = data.streamType;
    this.userId = data.userId;
    this.iceServers = data.iceServers;
    this.devices = data.devices;
    this.stream = data.stream;
    this.devicePermissions = data.devicePermissions;
    this.initial = data.isInitial;

    this.onGotStream = data.onGotStream;
    this.onGotOffer = data.onGotOffer;
    this.onGotCandidate = data.onGotCandidate;
    this.onIceConnectionStateDisconnected = data.onIceConnectionStateDisconnected;
    this.onIceConnectionStateFailed = data.onIceConnectionStateFailed;

    this.candidateQueue = [];
    this.sdpAnswerSet = false;
    this.sdpAnswer = null;

    this.log('constructor', data);
  }

  async createPeerConnection() {
    this.log('createPeerConnection');
    this.peerConnection = new RTCPeerConnection({
      iceServers: this.iceServers,
    });
    this.peerConnection.onsignalingstatechange = () => {
      if (!this.sdpAnswerSet && this.sdpAnswer) {
        if (this.isPublish()) {
          this.addAnswer(this.sdpAnswer);
        }
        if (this.isView()) {
          this.processOffer(this.sdpAnswer);
        }
      }
    };
    this.peerConnection.oniceconnectionstatechange = () => {
      const iceConnectionState = this.peerConnection?.iceConnectionState;
      this.log('onIceConnectionStateChange', iceConnectionState);
      if (iceConnectionState === 'disconnected') {
        this.onIceConnectionStateDisconnected();
      }
      if (iceConnectionState === 'failed') {
        this.onIceConnectionStateFailed();
      }
    };
    this.peerConnection.onicecandidate = e => {
      this.log('onIceCandidate', e);
      if (!e.candidate) {
        return;
      }
      this.log('onIceCandidate', e);
      this.onGotCandidate({
        connectionId: this.connectionId,
        candidate: e.candidate,
      });
    };

    if (!this.isPublish()) {
      this.peerConnection.ontrack = e => {
        this.stream = this.stream || new MediaStream();
        this.stream.addTrack(e.track);
        this.onGotStream({ stream: this.stream });
      };
    }
  }

  async createOffer() {
    this.log('createOffer; stream', this.stream);
    if (!this.peerConnection) {
      return;
    }

    if (this.stream) {
      this.stream.getTracks().map(t => {
        this.log('add track', t);
        this.peerConnection?.addTrack(t);
      });
    }
    const isPublish = this.isPublish();
    const offer = await this.peerConnection.createOffer(
      {
        offerToReceiveAudio: !isPublish && this.devicePermissions.audio,
        offerToReceiveVideo: !isPublish && this.devicePermissions.video,
        // offerToReceiveAudio: true,
        // offerToReceiveVideo: true,
      }
    );
    await this.peerConnection.setLocalDescription(offer);
    this.log('createOffer; offer', offer);
    this.onGotOffer({
      sdp: offer.sdp,
      connectionId: this.connectionId,
    });
  }

  async processOffer(sdp: string) {
    if (!this.peerConnection || this.sdpAnswerSet) {
      return;
    }
    if (this.peerConnection.signalingState !== 'stable') {
      this.sdpAnswer = sdp;
    }
    this.log('processOffer', sdp);
    const offer = new RTCSessionDescription({ type: 'offer', sdp });
    this.sdpAnswerSet = true;

    if (this.stream) {
      this.stream.getTracks().map(t => {
        this.log('add track', t);
        this.peerConnection?.addTrack(t);
      });
    }
    await this.peerConnection.setRemoteDescription(offer);



    await Promise.all(this.candidateQueue.map(c => this.addIceCandidate(c)));
    this.candidateQueue = [];

    const answer = await this.peerConnection.createAnswer();
    await this.peerConnection.setLocalDescription(answer);
    this.onGotOffer({
      sdp: answer.sdp,
      connectionId: this.connectionId,
    });
  }

  async addAnswer(sdp: string) {
    if (!this.peerConnection || this.sdpAnswerSet) {
      return;
    }
    if (this.peerConnection.signalingState !== 'have-local-offer') {
      this.sdpAnswer = sdp;
    }
    this.log('addAnswer', sdp);
    const answer = new RTCSessionDescription({ type: 'answer', sdp });
    this.sdpAnswerSet = true;
    await this.peerConnection.setRemoteDescription(answer);

    await Promise.all(this.candidateQueue.map(c => this.addIceCandidate(c)));
    this.candidateQueue = [];
  }

  async addIceCandidate(candidate: RTCIceCandidate) {
    if (!this.peerConnection) {
      return;
    }
    this.log('addIceCandidate', this.sdpAnswerSet, candidate);
    if (this.sdpAnswerSet) {
      await this.peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
    } else {
      this.candidateQueue.push(candidate);
    }
  }

  async release() {
    this.log('release');
    if (this.peerConnection) {
      await this.peerConnection.close();
      delete this.peerConnection;
    }
  }

  async changeStream(stream: MediaStream) {
    this.log('changeStream');
    this.peerConnection?.getSenders().map(t => {
      if (t?.track?.kind === TRACK_KIND.AUDIO) {
        const newTrack = stream.getAudioTracks()[0];
        t.replaceTrack(newTrack);
      } else if (t?.track?.kind === TRACK_KIND.VIDEO) {
        const newTrack = stream.getVideoTracks()[0];
        t.replaceTrack(newTrack);
      }
    });
    this.stream = stream;
  }

  applyDeviceSettings() {
    if (!this.stream || !this.stream.active || this.isScreenSharing()) {
      return;
    }

    changeTracksState({
      enabled: this.devicePermissions?.audio || false,
      tracks: this.stream.getAudioTracks(),
    });
    changeTracksState({
      enabled: this.devicePermissions?.video || false,
      tracks: this.stream.getVideoTracks(),
    });
  }

  updateDevicePermissions({ devicePermissions }: { devicePermissions: { audio: boolean, video: boolean } }) {
    if (this.isScreenSharing()) {
      return;
    }
    this.devicePermissions = devicePermissions;
    this.applyDeviceSettings();
  }

  async getStats() {
    if (!this.peerConnection || !this.peerConnection.getStats) {
      return null;
    }
    const stats = await this.peerConnection.getStats();
    const result: {
      connectionId: string,
      userId: string,
      streamType: STREAM_TYPE,
      type: string,
      reports: { [key: string]: string }[],
    } = {
      connectionId: this.connectionId,
      userId: this.userId,
      streamType: this.streamType,
      type: this.type,
      reports: [],
    };
    stats.forEach(report => {
      const reportResult: { [key: string]: string } = {
        type: report.type,
        reportId: report.connectionId,
        timestamp: report.timestamp,
      };
      Object.keys(report)
        .filter(key => !['id', 'timestamp', 'type'].includes(key))
        .map(key => {
          reportResult[key] = report[key];
        });
      result.reports.push(reportResult);
    });
    this.log('getStats', result);
    return result;
  }

  isInitial() {
    return this.initial;
  }

  isPublish() {
    return this.type === CONNECTION_TYPE.PUBLISH;
  }

  isView() {
    return this.type === CONNECTION_TYPE.VIEW;
  }

  isVideoChat() {
    return this.streamType === STREAM_TYPE.VIDEO_CHAT;
  }

  isScreenSharing() {
    return this.streamType === STREAM_TYPE.SCREEN_SHARING;
  }

  // eslint-disable-next-line no-unused-vars,class-methods-use-this
  // @ts-ignore
  log(...r) {
    // console.warn(`WebrtcConnection; connectionId: ${this.connectionId};`, ...r);
  }
}
