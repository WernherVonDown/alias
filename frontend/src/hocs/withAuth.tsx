import React, { ComponentType, useContext } from "react";
import { AuthContext } from "../context/AuthContext";

export function withAuth<T>(Component: ComponentType<T>) {
    return (props: T) => {
        const { state: { loggedIn } } = useContext(AuthContext);

        // if (isLoading) {
        //     return <div>Loading....</div>
        // }

        if (!loggedIn) {
            return <div>Пошув отседа</div>
        }
//@ts-ignore
        return <Component {...props} />
    }
}
