
import React from 'react';

interface IProps {
}

export const ExitIcon: React.FC<IProps> = (props) => {

    return (
        <svg width="48" height="48" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M22.3337 6.6665H9.66699C8.01014 6.6665 6.66699 8.00965 6.66699 9.6665V12.8887V14.6665H16.4161L13.1035 11.2132L14.9139 9.33317L21.3337 15.9998L14.9139 22.6665L13.1035 20.7865L16.4161 17.3332H6.66699V19.111V22.3332C6.66699 23.99 8.01014 25.3332 9.66699 25.3332H22.3337C23.9905 25.3332 25.3337 23.99 25.3337 22.3332V9.6665C25.3337 8.00965 23.9905 6.6665 22.3337 6.6665Z" fill="#FD4974" />
        </svg>
    )
}