import { WordStatuses } from "./WORD_STATUSES";

export interface IWordData {
    id: string,
    word: string,
    translation?: string,
    transcription?: string,
    infinitive?: string,
}

export interface IWord extends IWordData{
    status: WordStatuses;
    score?: number;
    id: string;
    
}