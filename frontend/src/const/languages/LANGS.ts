export enum Langs {
    FR = 'french',
    EN = 'english',
    DE = 'german',
    ZH = 'chinese',
}

export const LangsList: {[key in Langs]: {
    key: Langs,
    label: string,
    flag: string,
}} = {
    [Langs.EN]: {
        key: Langs.EN,
        label: 'Английский',
        flag: '🇺🇸'
    },
    [Langs.DE]: {
        key: Langs.DE,
        label: 'Немецкий',
        flag: '🇩🇪'
    },
    [Langs.FR]: {
        key: Langs.FR,
        label: 'Французский',
        flag: '🇫🇷'
    },
    [Langs.ZH]: {
        key: Langs.ZH,
        label: 'Китайский',
        flag: '🇨🇳'
    },
}
