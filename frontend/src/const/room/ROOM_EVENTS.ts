export enum RoomEvents {
    join = 'room:join',
    leave = 'room:leave',
    joinGame = 'room:joinGame',
    usersData = 'room:usersData',
}
