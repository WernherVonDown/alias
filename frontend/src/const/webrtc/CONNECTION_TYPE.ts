export enum CONNECTION_TYPE {
  VIEW = 'view',
  PUBLISH = 'publish',
};
