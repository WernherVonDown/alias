import React, { ReactNode, useCallback, useEffect, useLayoutEffect, useMemo, useRef, useState } from "react";
import styles from './ResponsiveRectsContainer.module.scss';
import classNames from 'classnames';
import { useResize } from "../../hooks/useResize";
import { calculateLayout } from "../../utils/calculateLayout";

interface IProps {
    containerClassName?: string;
    children: ReactNode;
    aspectRatio?: number;
    rectsGap?: string;
    itemsNumber?: number;
}

const ResponsiveRectsContainer: React.FC<IProps> = ({ containerClassName, children, itemsNumber, aspectRatio = 16 / 9, rectsGap = '0px' }) => {
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const [rows, setRows] = useState<number>(0);
    const [cols, setCols] = useState<number>(0);
    const containerRef = useRef(null);
    const { rect: containerRect } = useResize(containerRef)

    useLayoutEffect(() => {
        if (containerRef.current && containerRect) {
            const { width, height, rows, cols } = recalculateLayout(containerRect?.width, containerRect?.height, itemsNumber || React.Children.count(children), aspectRatio);
            setWidth(width);
            setHeight(height);
            setRows(rows);
            setCols(cols);
        }
    }, [containerRef, containerRect, aspectRatio, itemsNumber, children]);

    const recalculateLayout = useCallback((screenWidth: number, screenHeight: number, videoCount: number, aspectRatio: number) => {
        const { width, height, cols, rows } = calculateLayout(
            screenWidth,
            screenHeight,
            videoCount,
            aspectRatio
        );

        return { width, height, cols, rows };
    }, [])


    const reactsWrapperStyle = useMemo(() => {
        return { gap: rectsGap, width: `calc(${width * cols}px + ${rectsGap} * ${cols})`, height: `calc(${height * rows}px)` }
    }, [width, height, cols, rows, rectsGap]);

    const rectStyle = useMemo(() => {
        return { width: `calc(${width}px - ${rectsGap})`, height: `calc(${height}px - ${rectsGap})` }
    }, [width, height, cols, rows, rectsGap]);

    const styledRects = useMemo(() => React.Children.map(children, (child: any) => {
        return React.cloneElement(child, {
            style: rectStyle,
            className: styles.rect
        });
    }), [rectStyle, children]);

    return <div className={classNames(containerClassName, styles.container)} style={{ height: `calc(100% - ${rectsGap})` }} ref={containerRef}>
        <div className={styles.rectsWrapper} style={reactsWrapperStyle}>
            {styledRects}
        </div>
    </div>
}

export default ResponsiveRectsContainer;
