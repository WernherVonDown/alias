import React, { useMemo } from "react";
import styles from './Footer.module.scss'
import { DonateModal } from "../components/DonateModal/DonateModal";

export const Footer: React.FC = () => {
    const year = useMemo(() => {
        return new Date().getFullYear();
    }, [])
    return <div className={styles.footer}>
        <div className={styles.item}>
            
            <DonateModal >
            <div style={{cursor: 'pointer'}}>Donate</div>
            </DonateModal>
            
            {/* <div>
                Add feedback
            </div> */}
        </div>
        <div className={styles.item}>
            Aliasgame, 2021-{year}
        </div>
    </div>
}