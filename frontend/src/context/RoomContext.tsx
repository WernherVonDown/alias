import React, { ReactElement, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { RoomEvents } from '../const/room/ROOM_EVENTS';
import { IRoomUser } from '../const/user/types';
import { IDevicePermissions, IRemoteStream } from '../const/videoChat/types';
import { VideoChatEvents } from '../const/videoChat/VIDEO_CHAT_EVENTS';
import { useMediaSocketSubscribe } from '../hooks/useMediaSocketSubscribe';
import { MediaSocketContext } from './MediaSocketContext';
import { StreamContext } from './StreamContext';
import { Box, CircularProgress } from '@mui/material';
import { CenterXY } from '../common/CenterXY/CenterXY';
import { Langs } from '../const/languages/LANGS';

interface IState {
    roomId: string;
    userName: string;
    users: IRoomUser[];
    me?: IRoomUser;
    languageKey: Langs;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[],
    roomId: string;
    userName: string;
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const RoomContext = React.createContext({} as ContextValue);

const RoomContextProvider = (props: IProps) => {
    const { children, roomId, userName } = props;
    const { state: { isConnected }, actions: { emit } } = useContext(MediaSocketContext);
    const { state: { devicePermissions } } = useContext(StreamContext);
    const [users, setUsers] = useState<IRoomUser[]>([]);
    const [me, setMe] = useState<IRoomUser>();
    const [languageKey, setLanguageKey] = useState<Langs>(Langs.EN)

    useEffect(() => {
        if (isConnected && roomId) {
            joinRoom()
        }
        
    }, [isConnected, roomId]);

    useEffect(() => {
        return () => {
            emit(RoomEvents.leave);
        }
    }, [])

    useEffect(() => {
        if (me && devicePermissions) {
            emit(VideoChatEvents.devicePermissions, { devices: devicePermissions })
        }
    }, [me, devicePermissions]);

    const onDevicePermissionChange = useCallback((data: { socketId: string, devices: IDevicePermissions }) => {
        setUsers(us => us.map(u => {
            if (u.socketId === data.socketId) {
                u.devices = data.devices
            }
            return u;
        }))
    }, [])

    useMediaSocketSubscribe(VideoChatEvents.devicePermissions, onDevicePermissionChange)

    const joinRoom = useCallback(async () => {
        const res = await emit(RoomEvents.join, { devices: devicePermissions });
        if (res.users) {
            setUsers(res.users);
        }

        if (res.user) {
            setMe(res.user);
        }
        if (res.languageKey) {
            setLanguageKey(res.languageKey)
        }
    }, [devicePermissions]);

    const onUserJoined = useCallback((user: IRoomUser) => {
        setUsers(users => [...users, user])
    }, []);

    const onUserLeaved = useCallback((user: IRoomUser) => {
        setUsers(users => users.filter(u => u.socketId !== user.socketId));
    }, []);

    useMediaSocketSubscribe(RoomEvents.join, onUserJoined);
    useMediaSocketSubscribe(RoomEvents.leave, onUserLeaved);

    const state = useMemo(() => ({
        roomId,
        userName,
        users,
        me,
        languageKey,
    }), [
        roomId,
        userName,
        users,
        me,
        languageKey,
    ]);

    const actions = {
    }


    return <RoomContext.Provider
        value={{
            state,
            actions
        }}
    >
        {isConnected && me ? children : <CenterXY><CircularProgress /></CenterXY>}
    </RoomContext.Provider>
}

RoomContextProvider.defaultProps = {
    state: {
    }
}


export { RoomContext, RoomContextProvider };
