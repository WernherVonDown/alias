import React, { ReactElement, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { RoomEvents } from '../const/room/ROOM_EVENTS';
import { IRoomUser } from '../const/user/types';
import { IRemoteStream } from '../const/videoChat/types';
import { VideoChatEvents } from '../const/videoChat/VIDEO_CHAT_EVENTS';
import { CONNECTION_TYPE } from '../const/webrtc/CONNECTION_TYPE';
import { SDP_KEY_MAP } from '../const/webrtc/SDP_KEY_MAP';
import { STREAM_TYPE } from '../const/webrtc/STREAM_TYPE';
import { useMediaSocketSubscribe } from '../hooks/useMediaSocketSubscribe';
import { webRtcController } from '../utils/webrtc/WebRtcController';
import { MediaSocketContext } from './MediaSocketContext';
import { RoomContext } from './RoomContext';
import { StreamContext } from './StreamContext';

interface IState {
    remoteStreams: IRemoteStream[];
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const iceServers: RTCIceServer[] = [
    {
        urls: 'stun:stun.l.google.com:19302'
    },
    {
        urls: ['turn:78.46.107.230:3486'],
        username: 'kurentoturn',
        credential: 'kurentoturnpassword'
    },
]

const VideoChatContext = React.createContext({} as ContextValue);
const publishConnections = []

const VideoChatContextProvider = (props: IProps) => {
    const { children } = props;
    const { state: { users, me } } = useContext(RoomContext);
    const { state: { stream, devicePermissions } } = useContext(StreamContext);
    const { actions: { emit } } = useContext(MediaSocketContext);
    const [remoteStreams, setRemoteStreams] = useState<IRemoteStream[]>([]);

    useEffect(() => {
        console.log('LOL KEK',users, me)
        if (users.length && me) {
            users
                .filter(u => {
                    const con = webRtcController.getConnection({ userId: u.socketId, type: CONNECTION_TYPE.PUBLISH })
                    return !con || (con.peerConnection?.connectionState !== "connected")
                })
                .forEach((u) => {
                    if (u.socketId === me.socketId) return;
                    _onCreateWebrtcConnection({
                        userId: u.socketId,
                        connectionId: u.socketId,
                        type: CONNECTION_TYPE.PUBLISH,
                        streamType: STREAM_TYPE.VIDEO_CHAT,
                        userName: u.userName,
                    });
                })
        }
    }, [users.length, me])

    const _onStopConnection = async ({ userId, type, streamType }: { userId: string, type?: string, streamType?: STREAM_TYPE }) => {
        await webRtcController.stopConnection({
            userId,
            type,
            streamType,
        });
    };

    const _getSdpKey = ({ isPublish, isInitial }: { isPublish: boolean, isInitial: boolean }) => SDP_KEY_MAP[`publish:${isPublish}_initial:${isInitial}`];

    const _onCreateWebrtcConnection = useCallback(async ({ streamType, userId, type, connectionId, isInitial = true, userName }:
        { streamType: STREAM_TYPE, userId: string, type: string, connectionId: string, isInitial?: boolean, userName: string }) => {
        const isPublish = type === CONNECTION_TYPE.PUBLISH;
        await _onStopConnection({
            userId,
            type,
            streamType,
        });

        let streamTmp = null;
        if (type === CONNECTION_TYPE.PUBLISH) {
            streamTmp = stream;
        }

        if (isPublish && !streamTmp) {
            publishConnections.push({
                userId,
                type,
                connectionId,
                streamType,
            });
            return;
        }
        await webRtcController.createConnection({
            connectionId,
            type,
            streamType,
            userId,
            isInitial,
            iceServers: iceServers,
            stream: streamTmp,
            devicePermissions: { audio: true, video: true },
            onGotOffer: ({ sdp }: any) => {
                emit(VideoChatEvents.webrtc, {
                    userId: userId,
                    connectionId,
                    streamType,
                    type,
                    [_getSdpKey({ isPublish, isInitial })]: sdp,
                });
            },
            onGotCandidate: ({ candidate }: any) => {
                emit(VideoChatEvents.webrtc, {
                    userId,
                    connectionId,
                    streamType,
                    candidate,
                });
            },
            onGotStream: ({ stream }: any) => {
                setRemoteStreams((p: IRemoteStream[]) => ([...p.filter(s => s.userId !== userId), { userId, stream, userName }]))
            },
            onIceConnectionStateDisconnected: () => {
                _onCreateWebrtcConnection({
                    userId,
                    type,
                    connectionId,
                    streamType,
                    userName,
                });
            },
            onIceConnectionStateFailed: () => {
                _onCreateWebrtcConnection({
                    userId,
                    type,
                    connectionId,
                    streamType,
                    userName,
                });
            },
        });
    }, [stream, users.length]);

    const onUserLeave = useCallback((data: IRoomUser) => {
        setRemoteStreams(p => [...p.filter((u: { userId: string }) => u.userId !== data.socketId)]);
        _onStopConnection({ userId: data.socketId })
    }, [remoteStreams, users, _onStopConnection])

    useMediaSocketSubscribe(RoomEvents.leave, onUserLeave);

    useEffect(() => {
        if (stream) {
            stream.getVideoTracks().map(t => t.enabled = devicePermissions.video)
        }
    }, [devicePermissions.video, stream, users.length, remoteStreams]);

    useEffect(() => {
        if (stream) {
            stream.getAudioTracks().map(t => t.enabled = devicePermissions.audio)
        }
    }, [devicePermissions.audio, stream, users.length, remoteStreams]);

    useEffect(() => {
        if (stream) {
            webRtcController.changePublishStream({ stream })
        }
    }, [stream])

    const onWebrtcEvent = useCallback(async (data: any) => {
        if (data.offer) {
            const typeToCreate = data.type === CONNECTION_TYPE.PUBLISH ? CONNECTION_TYPE.VIEW : CONNECTION_TYPE.PUBLISH;
            const isPublish = typeToCreate === CONNECTION_TYPE.PUBLISH;
            await _onCreateWebrtcConnection({
                connectionId: data.connectionId,
                type: typeToCreate,
                streamType: data.streamType,
                userId: data.userId,
                isInitial: false,
                userName: data.userName,
            });
            await webRtcController.processOffer({
                connectionId: data.connectionId,
                offer: data.offer,
            });
        } else if (data.answer) {
            await webRtcController.addAnswer({
                connectionId: data.connectionId,
                answer: data.answer,
            });
        } else if (data.candidate) {
            await webRtcController.addIceCandidate({
                connectionId: data.connectionId,
                candidate: data.candidate,
            });
        } else if (data.close) {
            const typeToClose = data.type === CONNECTION_TYPE.PUBLISH ? CONNECTION_TYPE.VIEW : CONNECTION_TYPE.PUBLISH;
            const isPublish = typeToClose === CONNECTION_TYPE.PUBLISH;
            const isView = typeToClose === CONNECTION_TYPE.VIEW;
            if (data.recreate && isPublish) {
                return;
            }
            await _onStopConnection({
                userId: data.userId,
                streamType: data.streamType,
                type: typeToClose,
            });
        }
    }, [stream, _onCreateWebrtcConnection]);

    useMediaSocketSubscribe(VideoChatEvents.webrtc, onWebrtcEvent);

    const state = useMemo(() => ({
        remoteStreams,
    }), [
        remoteStreams,
    ]);

    const actions = {
    }


    return <VideoChatContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </VideoChatContext.Provider>
}

VideoChatContextProvider.defaultProps = {
    state: {
    }
}


export { VideoChatContext, VideoChatContextProvider };
