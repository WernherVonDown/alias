import React, {
    useCallback,
    useRef,
    useEffect,
    ReactElement,
    useState,
    FunctionComponent,
} from 'react';
import io, { Socket } from 'socket.io-client';
import { config } from '../config/config';

export interface EventToSend {
    event: string;
    data: unknown;
}

export interface EventListeners {
    [key: string]: ((...args: unknown[]) => void)[];
}

interface ContextValue {
    actions: {
        subscribe: (event: string, ...args: unknown[]) => void;
        unsubscribe: (event: string, ...args: unknown[]) => void;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        emit: (event: string, ...args: unknown[]) => Promise<any>;
        // emitAnyway: (event: string, ...args: unknown[]) => Promise<unknown> | EventToSend[];
        disconnect: () => void;
        connect: () => void;
    };
    state: IState;
}

interface IState {
    isConnected: boolean;
    socket: typeof Socket | null;
}

interface IProps {
    state: IState;
    children: ReactElement | ReactElement[];
    roomId: string;
    userName: string;
}

const MediaSocketContext = React.createContext<ContextValue>({} as ContextValue);

const MediaSocketContextProvider: FunctionComponent<Partial<IProps>> = (props) => {
    const { children, roomId, userName } = props;
    const socketRef = useRef<SocketIOClient.Socket | null>(null);
    const connectingRef = useRef(false);
    const eventListenersRef = useRef<EventListeners>({});
    const eventsToSendRef = useRef<EventToSend[]>();
    const [isConnected, setConnected] = useState(false);

    const emit = useCallback((event: string, data: any) => new Promise((resolve, reject) => {
        if (socketRef && socketRef.current) {
            socketRef.current.emit(event, data, (err: unknown, answer: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(answer);
                }
            });
        }
    }), []);
    const subscribe = useCallback((event: string, handler: any) => {
        if (socketRef.current) {
            socketRef.current.on(event, handler);
        }

        eventListenersRef.current[event] = eventListenersRef.current[event] || [];
        eventListenersRef.current[event].push(handler);
    }, []);

    const unsubscribe = useCallback((event: keyof EventListeners, handler: any) => {
        if (socketRef.current) {
            socketRef.current.removeListener(event as string, handler);
        }

        if (eventListenersRef.current[event]?.length) {
            eventListenersRef.current[event] = eventListenersRef.current[event].filter(
                (el) => el !== handler,
            );
            if (!eventListenersRef.current[event]?.length) {
                delete eventListenersRef.current[event];
            }
        }
    }, []);

    const connect = useCallback(() => {
        if (connectingRef.current || socketRef.current) {
            return;
        }
        connectingRef.current = true;

        const socket = io.connect(config.socketUrl, {
            transports: ['websocket'],
            reconnection: false,
            reconnectionDelay: 1000,
            reconnectionDelayMax: 10000,
            reconnectionAttempts: Infinity,
            timeout: 60000,
            query: {
                roomId,
                userName,
            },
            path: config.socketPath
        });

        socket.on('error', (error: any) => {
            console.log('Error; MediaSocketContext; on error;', { error });
        });
        const events = Object.keys(eventListenersRef.current);

        events.map((event) => {
            eventListenersRef.current[event].map((handler) => {
                socket.on(event, handler);
            });
        });

        socket.on('connect', () => {
            socketRef.current = socket;
            connectingRef.current = false;
            setConnected(true);
            if (eventsToSendRef.current) {
                eventsToSendRef.current.forEach(({ event, data }) => emit(event, data));
                eventsToSendRef.current = [];
            }
        });
        socket.on('connect_error', (err: any) => {
            // eslint-disable-next-line no-console
            console.warn('MediaSocketContext.connect_error', err);
            socketRef.current?.disconnect()
        });

        socket.on('disconnect', () => {
            setConnected(false);
            eventsToSendRef.current = [];
            eventListenersRef.current = {};
            connectingRef.current = false;
            socketRef.current = null;
        });
    }, []);

    const closeSocketOnLeave = useCallback(() => {
        if (!socketRef.current) {
            return;
        }
        socketRef.current.disconnect();
        socketRef.current = null;
    }, []);

    const disconnect = useCallback(
        (clearCache = true) => {
            if (!socketRef.current) {
                return;
            }
            socketRef.current.disconnect();
            setConnected(false);
            if (clearCache) {
                eventsToSendRef.current = [];
                eventListenersRef.current = {};
                connectingRef.current = false;
                socketRef.current = null;
            }
        },
        [],
    );

    const actions = {
        emit,
        disconnect,
        connect,
        subscribe,
        unsubscribe,
        closeSocketOnLeave,
    };

    useEffect(() => {
        if (!isConnected) {
            connect();
        }
    }, [isConnected]);

    useEffect(() => {
        return () => disconnect();
    }, []);

    return (
        <MediaSocketContext.Provider
            value={{ actions, state: { isConnected, socket: socketRef.current } }}
        >
            {children}
        </MediaSocketContext.Provider>
    );
};

MediaSocketContextProvider.defaultProps = {
    state: {
        isConnected: false,
        socket: null,
    },
};

export { MediaSocketContext, MediaSocketContextProvider };
