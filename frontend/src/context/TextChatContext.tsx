import React, { ReactElement, useState, useCallback, useEffect } from "react";
import { useContext } from 'react';
import { MediaSocketContext } from "./MediaSocketContext";
import { useMediaSocketSubscribe } from "../hooks/useMediaSocketSubscribe";
import { RoomContext } from "./RoomContext";
import { IMessage } from "../const/textChat/types";
import { TextChatEvents } from "../const/textChat/TextChatEvents";

interface IState {
    messages: IMessage[]
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const TextChatContext = React.createContext({} as ContextValue);

const TextChatContextProvider = (props: IProps) => {
    const { children, state: defaultState } = props;
    const [state, setState] = useState<IState>(defaultState);
    const {actions: {emit}} = useContext(MediaSocketContext);
    const {state: {me}} = useContext(RoomContext);

    const receiveMessage = useCallback((message: IMessage) => {
        console.log('receiveMessage', message)
        setState((p: IState) => {
            message.isMe = me?.socketId === message.id;
            const messages = [...p.messages, message]
            return {
                ...p,
                messages
            }
        })
    }, [me])

    const sendMessage = useCallback((text: string) => {
        console.log("SEND", me, text)
        if (text.length && me) {
            const message = {
                userName: me?.userName,
                id: me?.socketId,
                text,
            }

            emit(TextChatEvents.textChatMessage, message);
            receiveMessage(message);
        }
    }, [me])

    useMediaSocketSubscribe(TextChatEvents.textChatMessage, receiveMessage);

    const actions = {
        sendMessage,
    }

    return <TextChatContext.Provider
                value={{
                    state,
                    actions
                }}
            >
                {children}
            </TextChatContext.Provider>
}

TextChatContextProvider.defaultProps = {
    state: {
        messages: []
    },
}

export { TextChatContext, TextChatContextProvider};