import React, { ReactElement, useEffect, useMemo } from 'react';
import { IUser } from '../const/user/types';
import { useState, useCallback } from 'react';
import AuthService from '../services/authService';
import { API_URL } from '../http/index';
import axios from 'axios';
import { UserApiRoutes } from '../const/user/API_ROUTES';
import { useRouter } from 'next/router';


interface IState {
    loggedIn: boolean;
    user: IUser | undefined;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const AuthContext = React.createContext({} as ContextValue);

const HIDDEN_PATHS_AFTER_LOGIN = ['/login', '/registration'];
const PROTECTED_ROUTES = ['my-words']

const AuthContextProvider = (props: IProps) => {
    const { children } = props;
    const [loggedIn, setLoggedIn] = useState<boolean>(false);
    const [user, setUser] = useState<IUser>();
    const router = useRouter();
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        if (localStorage.getItem('token')) {
            console.log("CHECK LOLG", localStorage.getItem('token'))
            checkAuth()
          }
    }, [])

    useEffect(() => {
        if (loading) return;
        if (!loggedIn) {
            if(PROTECTED_ROUTES.find(r => ~router.pathname.indexOf(r))) {
                router.push('/login')
            }
        } else if (HIDDEN_PATHS_AFTER_LOGIN.includes(router.pathname)) {
            router.push('/')
        }
    }, [router.pathname, loggedIn, loading])

    const login = useCallback(async (email: string, password: string) => {
        try {
            setLoading(true)
            const response = await AuthService.login(email, password);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
        } catch (error) {
            console.log('login error;', error);
        } finally {
            setLoading(false)
        }
    }, [])

    const logout = useCallback(async () => {
        try {
            const response = await AuthService.logout();
            localStorage.removeItem('token');
            setLoggedIn(false);
        } catch (error) {
            console.log('logout error;', error)
        }
    }, [])

    const register = useCallback(async (email: string, password: string): Promise<{success: boolean}> => {
        try {
            const response = await AuthService.registration(email, password);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
            return { success: true };
        } catch (error) {
            console.log('register error;', error)
            return { success: false };
        }
    }, []);

    const resetPasswordConfirm = useCallback(async (email: string, token: string, password: string) => {
        try {
            const response = await AuthService.resetPasswordConfrim(email, token, password);
            return { success: true };
        } catch (error) {
            console.log('resetPasswordConfirm error;', error);
            return { success: false };
        }
    }, []);


    const resetPassword = useCallback(async (email: string) => {
        try {
            const response = await AuthService.resetPassword(email);
            return { success: true };
        } catch (error) {
            console.log('resetPassword error;', error);
            return { success: false };
        }
    }, []);

    const checkAuth = useCallback(async () => {
        try {
            setLoading(true)
            const response = await axios(`${API_URL}/${UserApiRoutes.REFRESH}`, { withCredentials: true });
            console.log("check auth", response.data)
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
        } catch (error) {
            console.log('checkAuth error;', error)
        } finally {
            setLoading(false)
        }
    }, [])

    const state = useMemo(() => ({
        loggedIn,
        user,
    }), [
        loggedIn,
        user,
    ]);

    const actions = {
        login,
        logout,
        register,
        checkAuth,
        resetPassword,
        resetPasswordConfirm,
    }


    return <AuthContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </AuthContext.Provider>
}

AuthContextProvider.defaultProps = {
    state: {
        loggedIn: false,
    }
}


export { AuthContext, AuthContextProvider };
