import React, { ReactElement, useState, useCallback, useEffect, useMemo, useRef } from "react";
import { useContext } from 'react';
import { GAME_EVENTS } from '../const/game/GAME_EVENTS';
import { IWord, IWordData } from '../const/word/types';
import { WordStatuses } from '../const/word/WORD_STATUSES';
import { ITeamScore } from '../const/game/types';
import { useMediaSocketSubscribe } from "../hooks/useMediaSocketSubscribe";
import { MediaSocketContext } from "./MediaSocketContext";
import { AuthContext } from "./AuthContext";
import { RoomContext } from "./RoomContext";
import { RoomEvents } from "../const/room/ROOM_EVENTS";

interface IState {
    currentWord: IWord;
    currentTeam: string;
    currentUserId: string;
    isReady: boolean;
    gameStarted: boolean;
    words: IWord[];
    roundStarted: boolean;
    showWordStatus: { word: IWord, status: WordStatuses } | null;
    teamsScores: ITeamScore[]
    meIsActive: boolean;
    gameUsers: any[];
    gameEnded: boolean;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const GameContext = React.createContext({} as ContextValue);

const GameContextProvider = (props: IProps) => {
    const { children, state: defaultState } = props;
    // const [state, setState] = useState<IState>(defaultState);
    const { actions: { emit } } = useContext(MediaSocketContext);
    const { state: { me: user }, actions: userActions } = useContext(RoomContext);
    const [meIsActive1, setMeIsActive] = useState(false);
    const [gameUsers, setGameUsers] = useState<any[]>([])
    const [currentWord, setCurrentWord] = useState<IWord>();
    const [currentTeam, setCurrentTeam] = useState<string>();
    const [currentUserId, setCurrentUserId] = useState<string>();
    const [isReady, setIsReady] = useState<boolean>();
    const [gameStarted, setGameStarted] = useState<boolean>();
    const [words, setWords] = useState<IWord[]>([]);
    const [roundStarted, setRoundStarted] = useState<boolean>(false);
    const [showWordStatus, setShowWordStatus] = useState<{ word: IWord, status: WordStatuses } | null>(null);
    const [teamsScores, setTeamsScores] = useState<ITeamScore[]>([]);
    const wordStatusRef = useRef(0);
    const [winnerTeam, setWinnerTeam] = useState();
    const [gameEnded, setGameEnded] = useState(false);

    console.log("MEEE", user)

    const joinGame = useCallback(async () => {
        const data = await emit(RoomEvents.joinGame);
        console.log("JOIN GAME", data)
        onChangeState(data)
    }, [])

    useEffect(() => {
        joinGame();
    }, [])

    const startGame = async () => {
        console.log("START GAME")
        await emit(GAME_EVENTS.startGame, {}).catch(e => console.log("ERRO", e));
    }

    const getWord = useCallback(() => {
        emit(GAME_EVENTS.getWord)
    }, [])

    // const onUserJoined = useCallback(({user, teamsScores}) => {
    //     setGameUsers()
    // }, [])


    const startRound = async () => {
        const {words} = await emit(GAME_EVENTS.roundStarted);
        setWords(words || [])
        setRoundStarted(true)
    }

    const sendWordStatus = (word: IWord, status: WordStatuses) => {
        setWords(ws => {
            ws.shift()
            return ws;
        })
        emit(GAME_EVENTS.wordStatus, { word, status });
    }
    
    const meIsActive = useMemo(() => user?.userId === currentUserId, [user, currentUserId])

    useEffect(() => console.log("meIsActive", meIsActive, user?.userId, currentUserId), [meIsActive, user, currentUserId])

    const onChangeState = useCallback((data: { 
        currentUserId: string, 
        currentTeam: string, 
        users: any[], 
        teamsScores: ITeamScore[],
        gameStarted: boolean,
        roundStarted: boolean,
        gameEnded: boolean,
    }) => {
        console.log("ON CHANGE", data)
        if (data.currentUserId) {
            setCurrentUserId(data.currentUserId);
        }
        if (data.currentTeam) {
            setCurrentTeam(data.currentTeam);
        }
        if (data.users) {
            setGameUsers(data.users)
        }
        if (data.teamsScores) {
            setTeamsScores(data.teamsScores);
        }
        if (typeof data.gameStarted === 'boolean') {
            setGameStarted(data.gameStarted)
        }
        if (typeof data.gameEnded === 'boolean') {
            setGameEnded(data.gameEnded)
        }
        if (typeof data.roundStarted === 'boolean') {
            setRoundStarted(data.roundStarted)
            if (!data.roundStarted) {
                clearTimeout(wordStatusRef.current);
                setShowWordStatus(null);
                if(meIsActive && words.length) {
                    const restWord = words.shift();
                    sendWordStatus(restWord as IWord, WordStatuses.SKIPPED);
                    setWords([])
                }
            }
            
        }

    }, [user, meIsActive, words])

    // useMediaSocketSubscribe(RoomEvents.usersData, ({ users, teamsScores }) => {
    //     console.log("USERS INFO", { users, teamsScores })
    //     setGameUsers(users);
    //     setTeamsScores(teamsScores);
    // });

    const clearWord = () => {
        // setState((p: IState) => {
        //     return {
        //         ...p,
        //         words: p.words.slice(1)
        //     }
        // })
    }

    const onGetWordStatus = useCallback((data: any) => {
        clearTimeout(wordStatusRef.current);
        setShowWordStatus(data);
        wordStatusRef.current = window.setTimeout(() => setShowWordStatus(null), 3000);
    }, [])

    useMediaSocketSubscribe(GAME_EVENTS.changeState, onChangeState);
    useMediaSocketSubscribe(GAME_EVENTS.wordStatus, onGetWordStatus);

    

    const state = useMemo<any>(() => ({
        currentTeam,
        currentUserId,
        gameStarted,
        roundStarted,
        teamsScores,
        gameUsers,
        meIsActive,
        words,
        showWordStatus,
        gameEnded,
    }), [
        currentTeam,
        currentUserId,
        gameStarted,
        roundStarted,
        teamsScores,
        gameUsers,
        meIsActive,
        words,
        showWordStatus,
        gameEnded,
    ])

    useEffect(() => {
        if (roundStarted && !words.length) {
            emit(GAME_EVENTS.getWords, {}).then(data => setWords(data.words))
        }
    }, [!words.length, roundStarted])

    const actions = {
        startGame,
        startRound,
        getWord,
        clearWord,
        sendWordStatus,
    }

    return <GameContext.Provider
        value={{
            state: { ...state },
            actions
        }}
    >
        {children}
    </GameContext.Provider>
}

GameContextProvider.defaultProps = {
    state: {
        messages: [],
        showWordStatus: null,
        currentUserId: '',
        teamsScores: []
    },
}

export { GameContext, GameContextProvider };