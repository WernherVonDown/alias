import React, { ReactElement, useMemo } from 'react';
import { responsiveFontSizes } from '@mui/material';
import { ThemeProvider, createTheme, Theme } from '@mui/material/styles';
import "@fontsource/russo-one";

interface IState {
    theme: Theme;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const ThemeContext = React.createContext({} as ContextValue);

const ThemeContextProvider = (props: IProps) => {
    const { children } = props;

    const theme = useMemo(() => {
        const theme = createTheme({palette: {
            mode: 'dark',
            primary: {
                contrastText: "#fff",
                dark: "#716FFF",
                light: "#716FFF",
                main: "#716FFF",
            },
            background: {
                default: "#0a0a10",
                paper: "#181820"
            },
            text: {
                primary: '#fff',
                secondary: '#8b8b8f'
            },
            
          },
          components: {
            MuiButton: {
                styleOverrides: {
                    root: ({ ownerState }) => ({
                        color: 'white',
                        ...(ownerState.variant === 'contained' &&
                          ownerState.color === 'primary' && {
                            backgroundColor: 'white',
                            color: 'black',
                            fontWeight: 400,
                            fontSize: '16px',
                            lineHeight: '24px',
                            ":hover": {
                                background: 'grey'
                            },
                          }),
                          ...(ownerState.variant === 'outlined' &&
                          ownerState.color === 'primary' && {
                            borderColor: 'white',
                            fontWeight: 400,
                            fontSize: '16px',
                            lineHeight: '24px'
                          }),
                          textTransform: 'none',
                          fontFamily: 'Russo One',
                      }),
                },
                
            },
            MuiTextField: {
                styleOverrides: {
                    root: {
                        input: {
                            fontFamily: 'Russo One',
                        }
                    }
                }
                
            },
            MuiTypography: {
                styleOverrides: {
                    root: {
                        fontFamily: 'Russo One',
                    }
                }
            }
        }});
        //   console.log("hehem", theme)
        return responsiveFontSizes(theme);
    }, [])

    const state = useMemo(() => ({
        theme,
    }), [
        theme,
    ]);

    const actions = {
    }

   


    return <ThemeContext.Provider
        value={{
            state,
            actions
        }}
    >
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    </ThemeContext.Provider>
}

ThemeContextProvider.defaultProps = {
    state: {
    }
}


export { ThemeContext, ThemeContextProvider };
