import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css';
import { AuthContext } from '../context/AuthContext';
import { useCallback, useContext, useEffect } from 'react';
import { LoginForm } from '../components/LoginForm/LoginForm.component';
import { RegistrationForm } from '../components/RegistrationForm/RegistrationForm.component';
import { LogoutButton } from '../components/LogoutButton/LogoutButton';
import { MainLayout } from '../components/MainLayout/MainLayout';
import { Avatar, Box, Button, CssBaseline, Grid, Paper, Typography } from '@mui/material';
import RoomService from '../services/roomService';
import { useRouter } from 'next/router';
import { Parallax, ParallaxBanner } from 'react-scroll-parallax';
import { LogoIcon } from '../icons/LogoIcon';
import { Welcome } from '../components/Landing/Welcome/Welcome';
import { trackUserVisit } from '../utils/userTracker';

const Home: NextPage = () => {
  const { state: { loggedIn }, actions: { checkAuth } } = useContext(AuthContext);
  const router = useRouter()

  // useEffect(() => {
  //   if (localStorage.getItem('token')) {
  //     console.log("CHECK LOLG", localStorage.getItem('token'))
  //     checkAuth()
  //   }
  // }, [])

  // const enterRoom = useCallback(async () => {
  //   try {
  //     const res = await RoomService.create();
  //     const roomId = res.data.roomId;
  //     if (roomId) {
  //       router.push(`/rooms/${roomId}`)
  //     }
  //   } catch (error) {
  //     console.log('Error create room')
  //   }
  // }, [])

  return (
    <div className={styles.container}>
      <Head>
        <title>Alias</title>
        <meta name="description" content="Сайт для изучения иностранного языка и практики разговорной речи с помощью игры Alias" />
        <meta
          name="keywords"
          content="alias, game, learning, изучение языка, разговорная практика, разговор на иностранно языке, игра alias, практика английского, практика французского"
        />
        <link rel="icon" href="/favicon.svg" />
      </Head>
      <MainLayout detachedFooter>
        <Welcome />
        {/* <Box style={{ minHeight: '100vh' }} display="flex">
          <Welcome />
        </Box>
        <Box display="flex" style={{padding: '1rem'}} justifyContent="center">
          <Donate />
        </Box> */}
      </MainLayout>
      {/* <footer className={styles.footer}>
        fooge
      </footer> */}
    </div>
  )
}


export async function getServerSideProps({ req, res }: any) {
  trackUserVisit({ req, res })

  return {
    props: {},
  };
}


export default Home
