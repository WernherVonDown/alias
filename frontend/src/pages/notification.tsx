import { AppBar, Container, styled, Toolbar } from "@mui/material";
import React, { useEffect } from "react";
import $api from "../http";

export default function Login() {
    const requestNotificationPermission = async () => {
        if (!("Notification" in window)) return;
        const permission = await Notification.requestPermission();
        if (permission !== 'granted') {
          alert('Дай разрешение')
        }
      }
      const registerServiceWorkerAndSubscribe = async () => {
        try {
            const registration = await navigator.serviceWorker.register('/service-worker.js');
            const subscription = await registration.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey: urlBase64ToUint8Array('BDzYFMYWZqyCluYzb5HI1UKSbTNFHghV2ULCIvLyemj4xU-uWcI-1ulth7MysA4QdSy1zIkVJ-TSUycSlg0Ha3I')
            })
            await $api.post('/subscribe', subscription)
            // await fetch(BACKEND_URL, {
            //   method: 'POST',
            //   body: JSON.stringify(subscription),
            //   headers: {'Content-Type': 'application/json'}
            // });
            console.log("SUCESS")
        } catch (error) {
          console.log("Error sub", error) 
        }
      }
    
      const urlBase64ToUint8Array = (base64String: string) => {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');
        const rawData = window.atob(base64);
        return new Uint8Array([...(rawData as any)].map(char => char.charCodeAt(0)));
      };
    
    
      useEffect(() => {
        requestNotificationPermission()
      }, [])
      return (
        <div className="App">
          <h1>Check push</h1>
          <button onClick={registerServiceWorkerAndSubscribe}>
            Подписаться
          </button>
        </div>
      );
}
