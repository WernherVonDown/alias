import '../styles/globals.scss'
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import '../styles/swiper.scss'
import type { AppProps } from 'next/app'
import { AuthContextProvider } from '../context/AuthContext'
import { ThemeContextProvider } from '../context/ThemeContext';
import { ParallaxProvider } from 'react-scroll-parallax';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeContextProvider>
      <AuthContextProvider>
        <ParallaxProvider>
          <Component {...pageProps} />
        </ParallaxProvider>
      </AuthContextProvider>
    </ThemeContextProvider>
  )
}

export default MyApp
