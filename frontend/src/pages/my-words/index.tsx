import React from "react";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import { NextPage } from "next";
import { Box, Container } from "@mui/material";
import { MyWords } from "../../components/MyWords/MyWords";


const Rooms: NextPage = () => {
    return (
        <MainLayout>
            {/* <Container fixed sx={{ height: '100%' }} // maxWidth={"xl"}
            > */}
                <MyWords />
            {/* </Container> */}
        </MainLayout>
    )
}

export default Rooms;