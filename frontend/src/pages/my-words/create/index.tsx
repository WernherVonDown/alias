import React, { useCallback } from "react";
import { MainLayout } from "../../../components/MainLayout/MainLayout";
import { NextPage } from "next";
import { Box, Container } from "@mui/material";
import { MyWords } from "../../../components/MyWords/MyWords";
import { AddWordsList, IWord } from "../../../components/AddWordsList/AddWordsList";
import { Langs } from "../../../const/languages/LANGS";
import WordsListService from "../../../services/wordsListService";


const CreateList: NextPage = () => {
    const onSave = useCallback(async (name: string, words: IWord[], languageKey: Langs) => {
        console.log("ON SAVE", {words, name, languageKey})
        const res = await WordsListService.create({words, name, languageKey})
        return res.data.wordsListId
    }, [])
    return (
        <MainLayout>
            <AddWordsList onSave={onSave} />
        </MainLayout>
    )
}

export default CreateList;