import React, { useCallback, useEffect, useState } from "react";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import { GetServerSidePropsContext, NextPage } from "next";
import { Box, Container, Typography } from "@mui/material";
import { IWordsList, MyWords } from "../../components/MyWords/MyWords";
import { AddWordsList, IWord } from "../../components/AddWordsList/AddWordsList";
import { Langs } from "../../const/languages/LANGS";
import WordsListService from "../../services/wordsListService";
import { useRouter } from "next/router";

interface IProps {
    words?: IWord[],
    listName?: string,
    language?: Langs,
}

export interface IWordsListFull {
    id: string,
    words: { word: string, translation: string, id: string }[],
    name: string,
    languageKey: Langs,
    isPublic?: boolean,
}

const WordsList: NextPage = () => {
    const router = useRouter();
    const { wordsListId } = router.query;
    const [wordsList, setWordsList] = useState<IWordsListFull>()
    const onSave = useCallback(async (name: string, words: IWord[], languageKey: Langs) => {
        console.log("ON SAVE", { words, name, languageKey })
        const res = await WordsListService.edit({ words, name, languageKey, id: wordsListId as string })
        return res.data.wordsListId
    }, [wordsListId])

    useEffect(() => {
        if (wordsListId) {
            getData(wordsListId as string)
        }
    }, [wordsListId])
    const getData = useCallback(async (wordsListId: string) => {
        try {
            const res = await WordsListService.getById(wordsListId);
            setWordsList(res.data)
        } catch (error) {
            console.log('error', error)
        }
    }, [])
    return (
        <MainLayout>
            {wordsList ? <AddWordsList
                words={wordsList.words}
                listName={wordsList.name}
                language={wordsList.languageKey}
                onSave={onSave}
            /> : <Typography color="white">Loading...</Typography>}
        </MainLayout>
    )
}

export default WordsList;