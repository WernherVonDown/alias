import React from "react";
import { CenterXY } from "../common/CenterXY/CenterXY";
import { MainLayout } from "../components/MainLayout/MainLayout";
import { RegistrationForm } from '../components/RegistrationForm/RegistrationForm.component';

export default function Registration () {
    return (
        <MainLayout>
            <CenterXY>
                <RegistrationForm />
            </CenterXY>
        </MainLayout>
    )
}
