import axios from "axios";
import { config } from "../config/config";
import { UserApiRoutes } from "../const/user/API_ROUTES";

export const API_URL = config.apiUrl;

const $api = axios.create({
    withCredentials: true,
    baseURL: API_URL
});

$api.interceptors.request.use((config) => {
    if (!config.headers) {
        config.headers = {};
    }
    console.log("GET TOKEN LOL", localStorage.getItem('token'))
    config.headers.Authtorization = `Bearer ${localStorage.getItem('token')}`;
    return config;
})

$api.interceptors.response.use(
    (config) => {
        return config;
    },
    async (error) => {
        const originalRequest = error.config;
        if (error.response.status === 401 && error.config && !error.config._isRetry) {
            originalRequest._isRetry = true;
            try {
                const response = await axios(`${API_URL}/${UserApiRoutes.REFRESH}`, { withCredentials: true });
                localStorage.setItem('token', response.data.acessToken)
                return $api.request(originalRequest); 
            } catch (error) {
                console.log('Not authorized')
            }
        } else {
            throw error;
        }
    }
)

export default $api;