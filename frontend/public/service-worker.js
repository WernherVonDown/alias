self.addEventListener('push', (event) => {
    const data = event.data.json();
    console.log("Push received", data)
    const options = {
        body: data.body,
        icon: '/favicon.svg',
    }
    // runtime.register().then(registration => {
    //     registration.showNotification(data.title, options)
    // })
    event.waitUntil(
        self.registration.showNotification(data.title, options)
    )
})