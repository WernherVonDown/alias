import { emitToRedis, subscribeToRedis } from "../../../boot/startRedis";
import { RedisEvents } from "../../../game/const/RedisEvents";
import { GAME_EVENTS } from "../../../game/const/game/GAME_EVENTS";
import { ROUND_TIME_IN_SEC, gameService } from "../../../services/game.service";
import { roomService } from "../../../services/room.service";
import type { IRouteFn } from "../../../types/socket";
import { SEC } from "../../../utils/convertors/convertedMilliseconds";
import { getSessionRoomId } from "../../../utils/helpers/getSessionRoomId";
import { getUserRoomId } from "../../../utils/helpers/getUserRoomId";
import { emitter } from "../../../utils/socket/emitter";

export interface ISocketQuery {
    roomId: string;
    userName: string;
    userId: string;
}

const GameTimers = new Map();
const GameTimersIds = new Map();

export const start: IRouteFn = async (socket, data) => {
    try {
        const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
        const socketId = socket.id;
        const sessionRoomId = getSessionRoomId(roomId);
        const userRoomId = getUserRoomId(socketId);
        const { room } = await roomService.getRoom(roomId)
        await gameService.startGame(room.game)
        emitter.sendToRoom(sessionRoomId, GAME_EVENTS.changeState, { gameStarted: true });
        return {}
    } catch (error) {
        console.log("gameController error", error)
    }

}

export const wordStatus: IRouteFn = async (socket, data) => {
    const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const socketId = socket.id;
    const sessionRoomId = getSessionRoomId(roomId);
    const userRoomId = getUserRoomId(socketId);
    const { room } = await roomService.getRoom(roomId);
    const {
        teamsScores,
    } = await gameService.wordStatus(room.game, data);
    emitter.sendToRoom(sessionRoomId, GAME_EVENTS.changeState, { teamsScores });
    emitter.sendToRoomButUser(socket, sessionRoomId, GAME_EVENTS.wordStatus, data);
    return {}
}

export const getWords: IRouteFn = async (socket, data) => {
    const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const socketId = socket.id;
    const sessionRoomId = getSessionRoomId(roomId);
    const userRoomId = getUserRoomId(socketId);
    const { room } = await roomService.getRoom(roomId)
    const {
        words,
    } = await gameService.getWords(room.game);
    
    if (!words.length) {
        const {winnerTeam} = await gameService.endGame(room.game);
        emitter.sendToRoom(sessionRoomId, GAME_EVENTS.changeState, { gameEnded: true });
    }

    return {
        words,
    }
}

export const startRound: IRouteFn = async (socket, data) => {
    const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const socketId = socket.id;
    const sessionRoomId = getSessionRoomId(roomId);
    const userRoomId = getUserRoomId(socketId);
    const { room } = await roomService.getRoom(roomId)
    const {
        words,
    } = await gameService.startRound(room.game);
    emitter.sendToRoom(sessionRoomId, GAME_EVENTS.changeState, { roundStarted: true });
    // emitter.sendToRoomButUser(socket, sessionRoomId, ROOM_EVENTS.usersData, {users: gameUsers, teamsScores});
    GameTimers.set(room.game, 0);
    if (!words.length) {
        const {winnerTeam} = await gameService.endGame(room.game);
        emitter.sendToRoom(sessionRoomId, GAME_EVENTS.changeState, { gameEnded: true });
    } else {
        const timerId = setInterval(async () => {
            const time = GameTimers.get(room.game)
            const nextTime = time + 1;
            GameTimers.set(room.game, nextTime);
            emitter.sendToRoom(sessionRoomId, GAME_EVENTS.gameTimer, ROUND_TIME_IN_SEC - time);
            if (nextTime >= ROUND_TIME_IN_SEC) {
                clearInterval(timerId);
                const {
                    currentTeam,
                    currentUserId,
                } = await gameService.endRound(room.game)
                emitter.sendToRoom(sessionRoomId, GAME_EVENTS.changeState, {
                    currentTeam,
                    currentUserId,
                    roundStarted: false,
                });
            }
        }, SEC)
        GameTimersIds.set(room.game.toString(), timerId)
    }

    return {
        words,
    }
}

const onRoundEnded = ({ gameId }: { gameId: string }) => {
    if (GameTimersIds.has(gameId))
    clearInterval(GameTimersIds.get(gameId));
}

subscribeToRedis(RedisEvents.roundEnded, onRoundEnded);