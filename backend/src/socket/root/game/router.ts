import { Router } from "../../../utils/socket/router";
import * as controller from "./controller";

const router = new Router();

router.addRoute({ path: 'start', log: true }, controller.start)
router.addRoute({ path: 'roundStarted' }, controller.startRound);
router.addRoute({ path: 'wordStatus' }, controller.wordStatus);
router.addRoute({ path: 'getWords' }, controller.getWords)

export const gameRouter = router;