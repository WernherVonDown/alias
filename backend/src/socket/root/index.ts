import { Router } from "../../utils/socket/router";
import { gameRouter } from "./game/router";
import { roomRouter } from "./room/router";
import { textChatRouter } from "./textChat/textChatRouter";
import { videoChatRouter } from "./videoChat/videoChatRouter";


const router = new Router();
router.addRouter("game", gameRouter);
router.addRouter("room", roomRouter);

router.addRouter("videoChat", videoChatRouter);
router.addRouter("textChat", textChatRouter);

export const rootRouter = router;