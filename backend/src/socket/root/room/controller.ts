import { RoomEvents } from "../../../const/room/ROOM_EVENTS";
import { GAME_EVENTS } from "../../../game/const/game/GAME_EVENTS";
import { ROOM_EVENTS } from "../../../game/const/room/ROOM_EVENTS";
import { roomsManager } from "../../../game/roomsManager";
import { gameService } from "../../../services/game.service";
import { roomService } from "../../../services/room.service";
import type { IRouteFn } from "../../../types/socket";
import { getSessionRoomId } from "../../../utils/helpers/getSessionRoomId";
import { getUserRoomId } from "../../../utils/helpers/getUserRoomId";
import { emitter } from "../../../utils/socket/emitter";

export interface ISocketQuery {
    roomId: string;
    userName: string;
    userId: string;
}

export const join: IRouteFn = async (socket, data) => {
    const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const { devices } = data;
    const socketId = socket.id;
    const sessionRoomId = roomId//getSessionRoomId(roomId);
    const userRoomId = userId//getUserRoomId(userId);
    socket.join(sessionRoomId);
    socket.join(userRoomId);
    const { users, languageKey } = await roomService.join({ userName, roomId, socketId, devices });
    emitter.sendToRoomButUser(socket, sessionRoomId, RoomEvents.join, { userName, socketId, devices })
    // roomsManager.joinRoom(socket, {roomId, userName})
    return { users, user: { userName, socketId, userId }, languageKey, };
}

export const joinGame: IRouteFn = async (socket, data) => {
    const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    console.log("JOIN", emitter.isConnected(socket.id))
    const sessionRoomId = roomId//getSessionRoomId(roomId);
    const {room} = await roomService.getRoom(roomId)
    // console.log("ROOMG", room)
    const  {
        user,
        currentTeam,
        currentUserId,
        gameStarted,
        roundStarted,
        teamsScores,
        gameUsers,
        gameEnded,
    } = await gameService.addUser(room.game, userId, userName)
    const resData = {
        users: gameUsers, 
        teamsScores,
        currentTeam,
        currentUserId,
        gameStarted,
        roundStarted,
        gameEnded,
    }
    emitter.sendToRoomButUser(socket, sessionRoomId, GAME_EVENTS.changeState, resData);

    return resData;
}

export const leave: IRouteFn = async (socket, data) => {
    const { roomId, userName, userId = socket.id }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const socketId = socket.id;
    const sessionRoomId = getSessionRoomId(roomId);
    const userRoomId = getUserRoomId(socketId);
    const {room} = await roomService.getRoom(roomId)
    console.log("LEAVE")
    socket.leave(sessionRoomId);
    socket.leave(userRoomId);
    // roomsManager.onDisconnect(socket)
    await roomService.leave({ roomId, socketId })
    const {
        currentTeam,
        currentUserId,
        gameStarted,
        roundStarted,
        teamsScores,
        gameUsers,
    } = await gameService.leave(room.game, userId)
    emitter.sendToRoomButUser(socket, sessionRoomId, GAME_EVENTS.changeState, {
        users: gameUsers, 
        teamsScores,
        currentTeam,
        currentUserId,
        gameStarted,
        roundStarted,
    });
    emitter.sendToRoomButUser(socket, sessionRoomId, RoomEvents.leave, { userName, socketId })
    return {}
}
