import { VideoChatEvents } from "../../../const/videoChat/VideoChatEvents";
import { roomService } from "../../../services/room.service";
import type { IRouteFn } from "../../../types/socket";
import { getSessionRoomId } from "../../../utils/helpers/getSessionRoomId";
import { getUserRoomId } from "../../../utils/helpers/getUserRoomId";
import { emitter } from "../../../utils/socket/emitter";
import type { ISocketQuery } from "../room/controller";

export const webrtc: IRouteFn = async (socket, data) => {
    const { userName, roomId }: ISocketQuery = socket.handshake
        .query as unknown as ISocketQuery;
    const userRoomId = getUserRoomId(data.userId);
    data.userId = socket.id;
    data.userName = userName;

    emitter.sendToRoomButUser(
        socket,
        userRoomId,
        VideoChatEvents.webrtc,
        data
    );
    return {};
}

export const changeDevicePermissions: IRouteFn = async (socket, data) => {
    const { roomId }: ISocketQuery = socket.handshake
        .query as unknown as ISocketQuery;
    const sessionRoomId = getSessionRoomId(roomId);
    const { devices } = data;
    const socketId = socket.id;
    roomService.setDevicePermissions({ socketId, roomId, devices });

    emitter.sendToRoomButUser(
        socket,
        sessionRoomId,
        VideoChatEvents.devicePermissions,
        { devices, socketId }
    );

    return {};
}