
import { Router } from "../../../utils/socket/router";
import * as controller from "./videoChatController";


const router = new Router();

router.addRoute({ path: "webrtc" }, controller.webrtc);
router.addRoute({ path: "devicePermissions" }, controller.changeDevicePermissions);

export const videoChatRouter = router;