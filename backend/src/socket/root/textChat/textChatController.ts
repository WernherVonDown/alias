import { TextChatEvents } from "../../../const/textChat/TextChatEvents";
import type { IRouteFn } from "../../../types/socket";
import { getSessionRoomId } from "../../../utils/helpers/getSessionRoomId";
import { emitter } from "../../../utils/socket/emitter";
import type { ISocketQuery } from "../room/controller";

export const onTextMessage: IRouteFn = async (socket, data) => {
    const { roomId, userName }: ISocketQuery = socket.handshake
        .query as unknown as ISocketQuery;
    const sessionRoomId = getSessionRoomId(roomId);
    const { message } = data;

    emitter.sendToRoomButUser(
        socket,
        sessionRoomId,
        TextChatEvents.textChatMessage,
        data
    );

    return {};
}