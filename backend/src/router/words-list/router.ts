import { Router } from "express";
import { wordsListController } from "../../controllers/wordsList.controller";
import { authMiddleware } from "../../middlewares/auth.middleware";

const router = Router();
// /authMiddleware
router.post('/', authMiddleware, wordsListController.create);
router.post('/:wordsListId', authMiddleware, wordsListController.edit);
router.get('/translation', authMiddleware, wordsListController.getTranslation);
router.get('/public', wordsListController.getAllPublic);
router.get('/:wordsListId', authMiddleware, wordsListController.getById);
router.get('/', authMiddleware, wordsListController.getAll);




export default router;