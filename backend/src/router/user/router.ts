import { Router } from "express";
import { body } from "express-validator";
import { userController } from '../../controllers/user.controller';
import { authMiddleware } from "../../middlewares/auth.middleware";
import { onTrackLogin, onTrackRegister, userTrackerMiddleware } from "../../middlewares/userTrackerMiddleware";

const router = Router();

router.post('/registration',
    body('email').isEmail(),
    body('password').isLength({ min: 3, max: 32 }),
    userTrackerMiddleware({ eventType: "register", onTack: onTrackRegister }),
    userController.registration,
);
router.post('/login', userTrackerMiddleware({ eventType: "login", onTack: onTrackLogin }), userController.login);
router.post('/logout', userController.logout);
router.get('/activate/:link', userController.activate);
router.get('/refresh', userController.refresh);
router.get('/users', authMiddleware, userController.getUsers);
router.post('/reset-password',
    body('email').isEmail(),
    userController.resetPassword
);
router.post('/reset-password-confirm',
    body('email').isEmail(),
    body('password').isLength({ min: 3, max: 32 }),
    userController.resetPasswordConfirm
);

export default router;