import { Router } from "express";
import userRoutes from './user/router';
import roomRoutes from './room/router';
import wordsListRoutes from './words-list/router';
import webPush from 'web-push'

const router = Router();

const vapidKeys = {
    publicKey: 'BDzYFMYWZqyCluYzb5HI1UKSbTNFHghV2ULCIvLyemj4xU-uWcI-1ulth7MysA4QdSy1zIkVJ-TSUycSlg0Ha3I',
    privateKey: 'ZPeBeVmUESE4SFFFLW4Ijhny69mvlMWyFol7BcjIZXw'

}
webPush.setVapidDetails(
    'mailto:danil.a@fora-soft.com',
    vapidKeys.publicKey,
    vapidKeys.privateKey
)

router.use('/user', userRoutes);
router.use('/rooms', roomRoutes);
router.use('/words-list', wordsListRoutes);

router.post('/subscribe', (req, res) => {
    const subscription = req.body;
    console.log('subscription', subscription)
    res.status(201).json({});

    const payload = JSON.stringify({title: "Push norification " + new Date().getUTCSeconds(), body: "Test push!"})
    setTimeout(() => {
        webPush.sendNotification(subscription, payload).catch(error => console.log("Error", error))
    }, 10000)
    
})
export default router;