import mongoose from 'mongoose';
import type { Document } from 'mongoose';

interface Doc extends Document {
    toDto: (...args: unknown[]) => any;
}

export const toDtoIfHas = (
    model?: any,
    options?: any,
): any => {
    if (Array.isArray(model)) {
        return model.map(m => m?.toDto ? m.toDto(options) : m)
    }
     return model?.toDto ? model.toDto(options) : model;
};
