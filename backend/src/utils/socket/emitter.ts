import type { Server, Socket } from "socket.io";
import { io } from "../../config/socket";

class SocketEmitter {
    private io: Server;

    constructor(io:Server) {
        this.io = io;
    }

    init(io: Server) {
        this.io = io;
    }

    sendToUser(socketId: string, event: string, data?: any) {
        this.io.to(socketId).emit(event, data);
    }

    sendToRoomButUser(socket: Socket, roomId: string, event: string, data?: any) {
        socket.broadcast.to(roomId).emit(event, data);
    }

    sendToRoom(roomId: string, event: string, data?: any) {
        this.io.to(roomId).emit(event, data);
    }

    isConnected (socketId: string): boolean {
        return !!this.io.sockets.adapter.sids.has(socketId);
    }
}

export let emitter = new SocketEmitter(io);

