import { LanguageKeys } from "../../const/LanguageKeys";
import ReversoContext from "../../externalServices/reverso.service";
import { translateCommon } from "../../externalServices/yandexDict.service";
import { translateResult } from "../../migrations/preparePublicWordsLists";
import wordModel from "../../models/word.model";
import { wordsService } from "../../services/words.service";
//@ts-ignore
import TextToIPA from 'text-to-ipa';
//https://github.com/yishn/chinese-tokenizer
const ForeignYa: { [key in LanguageKeys]: string } = {
    [LanguageKeys.english]: 'en',
    [LanguageKeys.french]: 'fr',
    [LanguageKeys.german]: 'de',
    [LanguageKeys.chinese]: 'zh',
}

export async function getTranslation(word: string, translation: string, language: string) {
    const result = {
        word: word,
        translation: translation,
    }
    let transWord = false;
    let targetLang = 'russian';
    let sourceLang = language;

    let wordToTranslate = word;
    if (translation && !word) {
        sourceLang = targetLang;
        targetLang = language
        wordToTranslate = translation;
        transWord = true;
    }

    let yaTrans = targetLang === 'russian' ? `${ForeignYa[sourceLang as LanguageKeys]}-ru` : `ru-${ForeignYa[targetLang as LanguageKeys]}`
    let res = '';
    console.log("ON REQUEST", wordToTranslate, sourceLang, targetLang, yaTrans, {
        word: targetLang === 'russian' ? wordToTranslate : '',
        translation: targetLang === 'russian' ? '' : wordToTranslate,
        languageKey: targetLang === 'russian' ? sourceLang : targetLang
    })
    const rootWord = await wordsService.findRootWord({
        word: targetLang === 'russian' ? wordToTranslate : '',
        translation: targetLang === 'russian' ? '' : wordToTranslate,
        languageKey: targetLang === 'russian' ? sourceLang : targetLang
    })
    console.log("ROOT WORD", rootWord)
    if (rootWord) {
        //@ts-ignore
        res = targetLang === 'russian' ? rootWord.translation : rootWord.infinitive || rootWord.word
    } else {
        try {
            const trans = await translateCommon.translateTo(wordToTranslate, yaTrans);
            // if (!res.length) {
            //     console.log("skip", language, word, transcript)
            //     return
            // }
            let infinitive = ''
            if (targetLang === 'russian') {
                const maxFr = Math.max(...trans[0].tr.map((t: any) => t.fr))
                // console.log("MAX FR", maxFr)
                res = trans[0].tr.filter((t: any) => t.fr === maxFr).map((t: any) => t.text).slice(0, 3).join(', ')
            } else {
                res = trans[0]?.tr[0]?.text;
                try {
                    res = translateResult[targetLang as LanguageKeys](trans[0]?.tr[0])
                } catch (error) {
                    
                }
                
            }
            let transcript = '';
            if (LanguageKeys.english === targetLang) {
                try {
                    transcript = TextToIPA.lookup(res).text;
                } catch (error) {
                    console.log("TRANS ERROR", error)
                }

            }
            const wordRes = new wordModel({
                word: targetLang === 'russian' ? wordToTranslate : res,
                translation: targetLang === 'russian' ? res : wordToTranslate,
                transcription: transcript,
                languageKey: targetLang === 'russian' ? sourceLang : targetLang,
                infinitive: '',
                isRoot: true,
            });
            console.log("NEW WORD", wordRes)
            await wordRes.save()
        } catch (error) {
        console.log("yandex translate err", error)
    }
}

// try {
//     res = await ReversoContext.getTranslation(wordToTranslate, sourceLang, targetLang);
// } catch (error) {
//     console.log("reverso translate err", error)
// }

return {
    ...result,
    [transWord ? 'word' : 'translation']: res
};
}