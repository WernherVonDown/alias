import {Schema, model, Document} from 'mongoose';
import type { IWordDocument, IWordDto } from './word.model';
import type { LanguageKeys } from '../const/LanguageKeys';
import { toDtoIfHas } from '../utils/mongo/toDtoIfHas';

const WordsList = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    words: [{
        type: Schema.Types.ObjectId,
        ref: 'Word'
    }],
    name: {
        type: String,
        required: true,
    },
    languageKey: {
        type: String,
        required: true,
    },
    isPublic: {
        type: Boolean,
        default: false,
    },
    color: {
        type: String,
    }
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

export interface IWordsList {
    user?: string,
    words: IWordDocument[],
    name: string,
    infinitive?: string,
    languageKey: LanguageKeys,
    isPublic: boolean,
}

export type IWordsListDocument = IWordsList & Document;

export interface IWordsListFull {
    words: IWordDto[],
    name: string,
    languageKey: LanguageKeys,
    isPublic: boolean,
}

WordsList.methods.toDto = function (short?: boolean) {
    if (short) {
        return {
            id: this._id,
            name: this.name,
            languageKey: this.languageKey,
            isPublic: this.isPublic,
            wordsNum: this.words.length,
        }
    }
    
    return {
        id: this._id,
        name: this.name,
        languageKey: this.languageKey,
        isPublic: this.isPublic,
        wordsNum: this.words.length,
        words: toDtoIfHas(this.words)
    }
}


export default model('WordsList', WordsList);

