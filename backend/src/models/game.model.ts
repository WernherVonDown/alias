import mongoose from 'mongoose';
import { Schema, model, Document } from 'mongoose';
import type { IWord } from './word.model';

export interface IGameUser {
    userName: string,
    id: string,
    teamColor: string,
    score?: number,
    maxScore?: number,
    active?: boolean,
}

export const getGameUser = ({
    userName,
    id,
    teamColor,
    score = 0,
    maxScore = 0,
    active = false,
}: IGameUser) => {
    return {
        userName,
        id,
        teamColor,
        score,
        maxScore,
        active,
    }
}

const Game = new Schema({
    room: {
        type: mongoose.Types.ObjectId,
        ref: 'Room'
    },
    users: {
        type:  Schema.Types.Mixed,
        default: {},
        // {
        //     userName: String,
        //     id: String,
        //     teamColor: String,
        //     score: Number,
        //     maxScore: Number,
        //     active: Boolean,
        // }
    },
    currentUser: {
        type: String,
    },
    currentTeamId: {
        type: String,
    },
    gameStarted: {
        type: Boolean,
        default: false
    },
    roundStarted: {
        type: Boolean,
        default: false
    },
    words: [{
        type: mongoose.Types.ObjectId,
        ref: 'Word'
    }],
    teams: [{
        users: [{
            type: String,
        }],
        teamId: {
            type: String,
        }
    }],
    roundTeams: [{
        type: String,
    }],
    roundTeamsOrder: [{
        type: String,
    }],
    timerId: {
        type: Number,
        default: 0,
    },
    scores: {
        type:  Schema.Types.Mixed,
        default: {},
        // of: Number,
    },
    hostUser: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
    },
    gameEnded: {
        type: Boolean,
    },
    gameWinnerTeam: {
        type: String,
    },
    
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

export interface IGameDocument extends Document {
    room: string,
    users: {[key: string]: IGameUser},
    currentUser: string,
    currentTeamId: string,
    gameStarted: boolean,
    roundStarted: boolean,
    words: IWord[],
    teams: {teamId: string, users: string[]}[],
    roundTeams: string[],
    roundTeamsOrder: string[],
    scores: {[key: string]: number},
    hostUser: string,
    gameEnded: boolean,
    gameWinnerTeam: string,
}

Game.methods.toDto = function toDto() {
    return {

    }
}

export default model('Game', Game);