import mongoose from 'mongoose';
import { Schema, model } from 'mongoose';

const Room = new Schema({
    roomId: {
        type: String,
        unique: true,
        required: true
    },
    users: [
        {
            userName: { type: String },
            socketId: { type: String },
            devices: {
                audio: {type: Boolean, default: true},
                video: {type: Boolean, default: true},
            }
        }
    ],
    isPublic: {
        type: Boolean,
        default: false,
    },
    languageKey: {
        type: String,
    },
    name: {
        type: String,
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    wordLists: [{
        type: mongoose.Types.ObjectId,
        ref: 'WordsList'
    }],
    game: {
        type: mongoose.Types.ObjectId,
        ref: 'Game'
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

Room.methods.toDto = function toDto () {
    return {
        roomId: this.roomId,
        users: this.users,
        isPublic: this.isPublic,
        languageKey: this.languageKey,
        name: this.name,
        user: this.user,
        game: this.game,
        id: this.id,
    }
}

export default model('Room', Room);