import { Schema, model, Document } from 'mongoose';
import type { LanguageKeys } from '../const/LanguageKeys';

const Word = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    word: {
        type: String,
        required: true
    },
    infinitive: {
        type: String,
    },
    translation: {
        type: String,
    },
    languageKey: {
        type: String,
        required: true
    },
    isRoot: {
        type: Boolean,
        default: false,
    },
    transcription: {
        type: String,
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

export interface IWord {
    user?: string,
    word: string,
    translation?: string,
    languageKey: LanguageKeys,
    infinitive?: string,
    isRoot?: boolean,
    transcription?: string;
}

export type IWordDocument = IWord & Document;

export interface IWordDto {
    id: string,
    word: string,
    translation?: string,
    infinitive?: string,
}

Word.methods.toDto = function () {
    return {
        id: this._id,
        word: this.word,
        translation: this.translation,
        infinitive: this.infinitive,
        transcription: this.transcription,
    }
}

export default model('Word', Word);