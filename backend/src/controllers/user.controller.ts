import type { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { vars } from "../config/vars";
import { userService } from '../services/user.service';
import { DAY } from "../utils/convertors/convertedMilliseconds";
import { ApiError } from "../utils/errors/ApiError";

class UserController {
    async registration(req: Request, res: Response, next: NextFunction) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return next(ApiError.BadRequest('Validation error', errors.array()))
            }
            const { email, password } = req.body;
            const userData = await userService.registration(email, password);
            res.cookie('refreshToken', userData.refreshToken, { maxAge: DAY, httpOnly: true }); //secure=true https
            return res.json(userData);
        } catch (error) {
            console.log('UserController.registration error;', error);
            next(error);
        }
    }

    async resetPassword(req: Request, res: Response, next: NextFunction) {
        try {
            const { email } = req.body;
            await userService.resetPassword(email);
            return res.sendStatus(200);
        } catch (error) {
            console.log('UserController.resetPassword error;', error);
            next(error);
        }
    }

    async resetPasswordConfirm(req: Request, res: Response, next: NextFunction) {
        try {
            const { email, token, password } = req.body;
            await userService.resetPasswordConfirm(email, token, password);
            return res.sendStatus(200);
        } catch (error) {
            console.log('UserController.login resetPasswordConfirm;', error);
            next(error);
        }
    }

    async login(req: Request, res: Response, next: NextFunction) {
        try {
            const { email, password } = req.body;
            const userData = await userService.login(email, password);
            res.cookie('refreshToken', userData.refreshToken, { maxAge: DAY, httpOnly: true }); //secure=true https
            return res.json(userData);
        } catch (error) {
            console.log('UserController.login error;', error);
            next(error);
        }
    }

    async logout(req: Request, res: Response, next: NextFunction) {
        try {
            const { refreshToken } = req.cookies;
            const token = await userService.logout(refreshToken);
            res.clearCookie('refreshToken');
            return res.json(token);
        } catch (error) {
            console.log('UserController.logout error;', error)
            next(error);
        }
    }

    async activate(req: Request, res: Response, next: NextFunction) {
        try {
            const activationLink = req.params.link;
            await userService.activate(activationLink);
            return res.redirect(vars.clientUrl);
        } catch (error) {
            console.log('UserController.activate error;', error);
            next(error);
        }
    }

    async refresh(req: Request, res: Response, next: NextFunction) {
        try {
            const { refreshToken } = req.cookies;
            const userData = await userService.refresh(refreshToken)

            res.cookie('refreshToken', userData.refreshToken, { maxAge: DAY * 30, httpOnly: true }); //secure=true https
            return res.json(userData);
        } catch (error) {
            console.log('UserController.refresh error;', error);
            next(error);
        }
    }

    async getUsers(req: Request, res: Response, next: NextFunction) {
        try {
            res.json(['123', '321'])
        } catch (error) {
            console.log('UserController.getUsers error;', error);
            next(error);
        }
    }
}

export const userController = new UserController();