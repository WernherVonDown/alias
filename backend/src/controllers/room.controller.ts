import type { NextFunction, Request, Response } from "express";
import { roomsManager } from "../game/roomsManager";
import { roomService } from "../services/room.service";
import { wordsListService } from "../services/wordsList.service";
import { gameService } from "../services/game.service";

class RoomController {
    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const {
                languageKey,
                isPublic,
                name,
                wordLists,
                userId,
              } = req.body;

            const roomData = await roomService.create({
                languageKey,
                isPublic,
                name,
                userId,
                wordLists,
            });
            return res.json(roomData);
        } catch (error) {
            console.log('RoomController.create error;', error);
            next(error);
        }
    }

    async getRoom(req: Request, res: Response, next: NextFunction) {
        try {
            const roomId = req.params.roomId;
            const roomData = await roomService.getRoom(roomId);
            return res.json(roomData);
        } catch (error) {
            console.log('RoomController.getRoom error;', error);
            next(error);
        }
    }
}

export const roomController = new RoomController();
