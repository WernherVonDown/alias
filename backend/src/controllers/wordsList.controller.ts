import type { NextFunction, Request, Response } from "express";
import { wordsListService } from "../services/wordsList.service";
import type { IWordsList } from "../models/wordsList.model";
import ReversoContext from "../externalServices/reverso.service";
import { getTranslation } from "../utils/language/getTranslation";

class WordsListController {
    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const { words, name, languageKey, isPublic } = req.body;
            const user = res.locals.user;
            if (!words.length) {
                throw "No words";
            }
            if (!name) {
                throw "No name";
            }

            if (!languageKey) {
                throw "No language key";
            }

            const list = await wordsListService.create({ words, name, languageKey, isPublic, user: user.id } as IWordsList);
            // const wordsLists = await wordsListService.getAll({user: user.id});
            return res.json({ wordsListId: list._id });
        } catch (error) {
            console.log('WordsListController.create error;', error);
            next(error);
        }
    }

    async edit(req: Request, res: Response, next: NextFunction) {
        try {
            const { words, name, languageKey, isPublic } = req.body;
            const user = res.locals.user;
            const wordsListId = req.params.wordsListId;
            console.log("EDIT", wordsListId)
            if (!wordsListId) {
                throw "No wordId"
            }
            if (!words.length) {
                throw "No words";
            }
            if (!name) {
                throw "No name";
            }

            if (!languageKey) {
                throw "No language key";
            }

            await wordsListService.edit(wordsListId, { words, name, languageKey, isPublic, user: user.id } as IWordsList);

            return res.json({ wordsListId });
        } catch (error) {
            console.log('WordsListController.create error;', error);
            next(error);
        }
    }

    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const user = res.locals.user;
            const wordsLists = await wordsListService.getAll({ user: user.id });
            return res.json(wordsLists);
        } catch (error) {
            console.log('WordsListController.getAll error;', error);
            next(error);
        }
    }

    async getAllPublic(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("GET ALL")
            const wordsLists = await wordsListService.getAllPublic();
            return res.json(wordsLists);
        } catch (error) {
            console.log('WordsListController.getAllPublic error;', error);
            next(error);
        }
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        try {
            const user = res.locals.user;
            const wordsListId = req.params.wordsListId;
            const wordsList = await wordsListService.getById(wordsListId, user.id);
            console.log("SEARCH LIST", user.id, wordsListId)
            if (!wordsList) {
                throw "Words list not found";
            }
            return res.json(wordsList);
        } catch (error) {
            console.log('WordsListController.getById error;', error);
            next(error);
        }
    }

    async getTranslation(req: Request, res: Response, next: NextFunction) {
        try {
            const word = req.query.word as string;
            const translation = req.query.translation as string;
            const language = req.query.language as string;
            console.log("GET PARAMS", req.params, req.body)
            const result = await getTranslation(word || '', translation || '', language)
            return res.json(result);
        } catch (error) {
            console.log('RoomController.getTranslation error;', error);
            next(error);
        }
    }
}

export const wordsListController = new WordsListController();
