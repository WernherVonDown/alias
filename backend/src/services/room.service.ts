import { v4 } from "uuid";
import type { IDevicePermissions } from "../const/videoChat/types";
import roomModel from "../models/room.model";
import { ApiError } from "../utils/errors/ApiError";
import type { LanguageKeys } from "../const/LanguageKeys";
import { wordsListService } from "./wordsList.service";
import { gameService } from "./game.service";
import _ from 'lodash';

class RoomService {
    async create({
        languageKey,
        isPublic,
        name,
        userId,
        wordLists,
    }: {
        languageKey: LanguageKeys,
        isPublic?: boolean,
        name?: string,
        userId?: string,
        wordLists: string[]
    }) {
        const roomId = v4();
        const room = new roomModel({
            roomId,
            languageKey,
            isPublic,
            name,
            userId,
        });
        const words = await wordsListService.getWordsIds(wordLists);
        const gameId = await gameService.createGame({
            //@ts-ignore
            room: room._id,
            hostUser: userId,
            words: _.shuffle(words),
        })
//@ts-ignore
        room.game = gameId;
        await room.save()

        return { roomId };
    }

    async clearAllUsersInAllRooms() {
        await roomModel.updateMany({}, {
            $set: {
                users: [],
            }
        })
    }

    async setDevicePermissions({ socketId, roomId, devices }: { socketId: string, roomId: string, devices: IDevicePermissions }) {
        await roomModel.findOneAndUpdate({
            roomId,
            "users.socketId": socketId,
        }, {
            $set: { "users.$.devices": devices }
        })
    }

    async join({ userName, socketId, roomId, devices }: { userName: string, socketId: string, roomId: string, devices: IDevicePermissions }) {
        let room = await roomModel.findOne({ roomId });
        if (!room) return {}
        room.users?.push({
            userName,
            socketId,
            devices,
        })
        await room.save()
        //@ts-ignore
        const roomDTO = await room.toDto();
        return roomDTO;
    }

    async leave({ socketId, roomId }: { socketId: string, roomId: string }) {
        await roomModel.findOneAndUpdate(
            {
                roomId,
            },
            {
                $pull: {
                    users: {
                        socketId,
                    },
                },
            },
            {
                new: true,
                upsert: true,
            }
        );
    }

    async getRoom(roomId: string) {
        const room = await roomModel.findOne({ roomId });
        if (!room) {
            throw ApiError.BadRequest("Комнаты не существует");
        }
        //@ts-ignore
        const roomDTO = await room.toDto();
        return { room: roomDTO };
    }
}

export const roomService = new RoomService();