import gameModel, { IGameDocument, getGameUser } from "../models/game.model"
import randomColor from 'randomcolor';
import { SEC } from "../utils/convertors/convertedMilliseconds";
import { WordStatuses } from "../game/const/word/wordStatuses";
import { IWord } from "../models/word.model";
import { IWordDocument } from "../models/word.model";
import { emitToRedis } from "../boot/startRedis";
import { RedisEvents } from "../game/const/RedisEvents";
import _ from 'lodash';

export const ROUND_TIME_IN_SEC = 90;

class GameService {
    async clearGames() {
        await gameModel.updateMany({}, {
            $set: {
                user: {},
                currentUser: '',
                currentTeamId: '',
                gameStarted: false,
                roundStarted: false,
                teams: [],
                roundTeams: [],
                roundTeamsOrder: [],
                scores: {},
            }
        })
    }

    async getGame(gameId: string): Promise<IGameDocument> {
        const game = await gameModel.findOne({
            _id: gameId,
        })
        if (!game) throw "Игра не найдена";
        //@ts-ignore
        return game;
    }

    async createGame({
        room,
        words,
        hostUser,
    }: {
        room: string,
        words: string[],
        hostUser?: string,
    }) {
        const game = new gameModel({
            room,
            words,
            hostUser,
        });
        await game.save();
        return game._id;
    }

    async addUser(gameId: string, userId: string, userName: string) {
        const game = await this.getGame(gameId);
        const teamColor = this._addUserToTeam(game, userId);
        const user = getGameUser({
            id: userId,
            userName,
            teamColor,
        });
        game.users = { ...game.users, [userId]: user };
        if (!game.currentTeamId) {
            game.currentTeamId = teamColor;
        }
        if (!game.currentUser) {
            game.currentUser = userId;
        }
        await game.save();

        return {
            currentTeam: game.currentTeamId,
            currentUserId: game.currentUser,
            gameStarted: game.gameStarted,
            roundStarted: game.roundStarted,
            gameEnded: game.gameEnded,
            teamsScores: Object.entries(game.scores).reduce((a, [teamColor, score]) => {
                a.push(({ teamColor, score }))
                return a;
            }, [] as any),
            gameUsers: Object.values(game.users),
        }

        return { game, user, users: Object.values(game.users) };
    }

    _addUserToTeam(game: IGameDocument, userId: string) {
        if (game.teams.length) {
            const teams = [...game.toObject().teams]
            let gameIndex = teams.findIndex(t => t.users.length && t.users.length < 2);
            if (!~gameIndex) {
                gameIndex = teams.findIndex(t => t.users.length < 2);
            }
            if (~gameIndex) {

                const g = teams[gameIndex];
                const users =[...g.users, userId];
                teams[gameIndex].users = users
                game.teams = [...teams]
                return g.teamId;
            }
        }

        return this._addNewTeam(game, userId);
    }

    _getTeamId(): string {
        return randomColor();
    }

    async leave(gameId: string, userId: string) {
        const game = await this.getGame(gameId);
        const u = game.users[userId];
        if (!u) throw "Пользователь не найден";
        const users = {...game.toObject().users};

        delete users[userId];
        game.users = users
        const teams = [...game.toObject().teams]
        const teamIndex = teams.findIndex(t => t.users.includes(userId));
        if (~teamIndex) {
            const t = teams[teamIndex];
            //@ts-ignore
            teams[teamIndex] = {...t, users: t.users.filter(u => u !== userId)}
            game.teams = teams;
        }
        if (game.currentUser === userId) {
            emitToRedis(RedisEvents.roundEnded, { gameId })
            game.roundStarted = false;
            const {
                currentUserId,
                currentTeam,
            } = await this.endRound(gameId);
            game.currentUser = currentUserId;
            game.currentTeamId = currentTeam
            
        }
        await game.save()
        return {
            currentTeam: game.currentTeamId,
            currentUserId: game.currentUser,
            gameStarted: game.gameStarted,
            roundStarted: game.roundStarted,
            teamsScores: Object.entries(game.scores).reduce((a, [teamColor, score]) => {
                a.push(({ teamColor, score }))
                return a;
            }, [] as any),
            gameUsers: Object.values(game.users),
        }
    }

    _addNewTeam(game: IGameDocument, userId: string): string {
        const teamId = this._getTeamId();
        game.teams = [...game.teams, { teamId, users: [userId] }]
        game.roundTeamsOrder.push(teamId);
        game.scores = { ...game.scores, [teamId]: 0 };
        return teamId;
    }

    async wordStatus(gameId: string, data: {
        status: WordStatuses,
        word: IWordDocument,
    }) {
        const game = await this.getGame(gameId);
        const words = game.toJSON().words;
        //@ts-ignore
        game.words = words.filter(w => data.word._id !== w.toString());
        if (data.status === WordStatuses.GUESSED && typeof game.scores[game.currentTeamId] === 'number') {
            const scores = {...game.toJSON().scores};
            scores[game.currentTeamId] = scores[game.currentTeamId] + 1;
            game.scores = scores;
        }
        await game.save()

        return {
            teamsScores: Object.entries(game.scores).reduce((a, [teamColor, score]) => {
                a.push(({ teamColor, score }))
                return a;
            }, [] as any),
        }
    }

    async startGame(gameId: string) {
        const game = await this.getGame(gameId);
        game.gameStarted = true;
        await game.save();
        
    }

    async getWords(gameId: string) {
        const game = await this.getGame(gameId);
        await game.populate('words');
        return {
            words: game.toJSON().words.slice(0, ROUND_TIME_IN_SEC)
        }
    }

    async startRound(gameId: string) {
        const game = await this.getGame(gameId);
        game.roundStarted = true;
        await game.populate('words');
        await game.save();
        return {
            words: game.toJSON().words.slice(0, ROUND_TIME_IN_SEC)
        }
    }

    async endGame(gameId: string) {
        const game = await this.getGame(gameId);
        game.gameEnded = true;
        const scores = game.toJSON().scores
        //@ts-ignore
        const winnerScore = Math.max(...Object.values(scores));
        game.save();
        return {
            winnerTeam: _.findKey(scores, winnerScore),
        }
    }

    async endRound(gameId: string) {
        const game = await this.getGame(gameId);
        let roundTeams = game.toJSON().roundTeams;
        const roundTeamsOrder = game.toJSON().roundTeamsOrder;
        const teams = game.toJSON().teams;

        for(let i = 0; i < roundTeamsOrder.length; i++) {
            if (!roundTeams.length) {
                roundTeams = [...roundTeamsOrder];
            }
            const nextTeam = roundTeams.pop();
            //@ts-ignore
            const team = teams.find((t) => t.teamId === nextTeam);
            if (team?.users.length && nextTeam) {
                team.users = team.users.reverse();
                const currentUserId = team.users[0];
                game.currentUser = currentUserId;
                game.currentTeamId = nextTeam;
                game.teams = teams;
                game.roundTeams = roundTeams;
                game.roundTeamsOrder = roundTeamsOrder;
                await game.save()
                break;
            }
            
        }
        return {
            currentTeam: game.currentTeamId,
            currentUserId: game.currentUser,
        }
    }
}

export const gameService = new GameService()