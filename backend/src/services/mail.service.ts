import nodemailer, { Transporter } from "nodemailer";
import { vars } from "../config/vars";

class MailService {
    private transporter: Transporter;

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: "smtp",
            host: "mail.jino.ru",
            port: 465,
            // secure: false, 
            auth: vars.email.auth,
        })
    }

    async sendActivationMail(to: string, link: string) {
        this.transporter.sendMail({
            from: vars.email.auth.user,
            to,
            subject: `Активация аккаунта на ${vars.clientUrl}`,
            text: '',
            html: `
                <div>
                    <h1>Для активации аккаунта перейдите по ссылке</h1>
                    <a href="${link}">${link}</a>
                </div>
            `
        })
    }

    async sendResetPasswordMail(to: string, link: string) {
        this.transporter.sendMail({
            from: vars.email.auth.user,
            to,
            subject: `Сброс пароля для аккаунта на ${vars.clientUrl}`,
            text: '',
            html: `
                <div>
                    <h1>Для сброса пароля перейдите по ссылке</h1>
                    <a href="${link}">${link}</a>
                </div>
            `
        })
    }
}

export const mailService = new MailService();