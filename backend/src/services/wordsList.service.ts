import type { IWordDocument, IWordDto } from "../models/word.model";
import wordsListModel, { IWordsList, IWordsListFull } from "../models/wordsList.model";
import { toDtoIfHas } from "../utils/mongo/toDtoIfHas";
import { wordsService } from "./words.service";

class WordsListService {
    async create({ words, name, languageKey, isPublic, user }: IWordsList) {
        console.log("CREATE", { words, name, languageKey, isPublic, user })
        const wordIds = await Promise.all(
            words.map(w => wordsService.getWord({ word: w.word, translation: w.translation, languageKey, user }))
        );
        console.log("WORD IDS", wordIds)
        // const wl = await wordsListModel.findOne({ name, languageKey, isPublic, user });
        // if (wl) {
        //     throw "Words list with these params already exits"
        // }
        return wordsListModel.create({ words: wordIds, name, languageKey, isPublic, user });
    }

    async createPublic({ words, name, languageKey, user, infinitive }: IWordsList) {
        const wordIds = await Promise.all(
            words.map(w => wordsService.getRootWord({ word: w.word, translation: w.translation, languageKey, infinitive, user }))
        );
    }

    async edit(wordsListId: string, { words, name, languageKey, isPublic, user }: IWordsList) {
        console.log("EDIT", { words, name, languageKey, isPublic, user })
        const wordIds = await Promise.all(
            words.map(w => wordsService.getWord({ word: w.word, translation: w.translation, languageKey, user }))
        );
        console.log("WORD IDS", wordIds)
        const l = await this.getById(wordsListId, user, true);
        if (!l) throw "Words list not found";
        await l.updateOne({ words: wordIds, name, languageKey, isPublic, user });
    }

    async getAll({ user }: { user: string }) {
        const res = await wordsListModel.find({ user })//.populate('words');
        //@ts-ignore
        return res.map(r => r.toDto())
    }

    async getAllPublic() {
        const res = await wordsListModel.find({ isPublic: true })//.populate('words');
        //@ts-ignore
        return res.map(r => r.toDto())
    }

    async getById(wordsListId: string, user?: string, noDto?: boolean) {
        const res = await wordsListModel.findOne({ _id: wordsListId, user }).populate('words');
        console.log("GET BY ID RES", res, { _id: wordsListId, user, noDto })
        if (!res) return
        if (noDto) return res;
        return toDtoIfHas(res);
    }

    async getWordsIds(wordLists: string[]): Promise<string[]>  {
        const res = await Promise.all(wordLists.map(wId => wordsListModel.findOne({ _id: wId })))
//@ts-ignore
        const words = res.reduce((acc: IWordDto[], e: IWordsListFull) => {
            if (e?.words) {
                return [...acc, ...e.words]
            }
            return acc;
        }, []);
        //@ts-ignore
        return words;
    }

    async getWords(wordLists: string[]) {
        const res = await Promise.all(wordLists.map(wId => wordsListModel.findOne({ _id: wId }).populate('words')))

        const resDto = toDtoIfHas(res) as IWordsListFull[];
        const words = resDto.reduce((acc: IWordDto[], e: IWordsListFull) => {
            if (e?.words) {
                return [...acc, ...e.words]
            }
            return acc;
        }, []);
        return words;
    }
}

export const wordsListService = new WordsListService();