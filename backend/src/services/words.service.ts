import { LanguageKeys } from "../const/LanguageKeys";
import wordModel, { IWord } from "../models/word.model";

class WordsService {
    async getWord({ word, translation, languageKey, user, infinitive, isRoot }: IWord): Promise<string> {
        let wordRes = await wordModel.findOne({
            $or: [
                { word },
                // { infinitive: infinitive || word, }
            ],
            translation,
            languageKey,
            isRoot,
        });
        if (!wordRes) {
            console.log("CREATE", { word, translation, languageKey, user })
            wordRes = new wordModel({
                word,
                translation,
                languageKey,
                user,
                infinitive,
                isRoot,
            });
            await wordRes.save()
            // await wordModel.create({
            //     word,
            //     translation,
            //     languageKey,
            //     user,
            // }, {
            //     new: true,
            // })
        }
//@ts-ignore
        return wordRes._id;
    }

    async findRootWord ({word, translation, languageKey}: {word: string, translation: string, languageKey: string}) {
        const wordRes = await wordModel.findOne({
            $or: [
                { word },
                { infinitive:  word, },
                { translation }
            ],
            languageKey,
            isRoot: true,
        });

        return wordRes
    }

    async getRootWord({ word, translation, languageKey, infinitive }: IWord): Promise<string> {
        let wordRes = await wordModel.findOne({
            $or: [
                { word },
                { infinitive: infinitive || word, },
                { translation }
            ],
            languageKey,
            isRoot: true,
        });

        if (!wordRes) {
            wordRes = new wordModel({
                word,
                translation,
                languageKey,
                infinitive,
                isRoot: true,
            });
            await wordRes.save()
        }
        //@ts-ignore
        return wordRes._id;
    }
}

export const wordsService = new WordsService();