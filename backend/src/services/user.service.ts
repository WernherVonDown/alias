import UserModel from "../models/user.model";
import bcrypt from "bcrypt";
import { v4 } from "uuid";
import { mailService } from "./mail.service";
import { tokenService } from './token.service';
import { vars } from "../config/vars";
import path from "path";
import { ApiError } from "../utils/errors/ApiError";
import userModel from "../models/user.model";

class UserService {
    async registration(email: string, password: string) {
        const candidate = await UserModel.findOne({ email });
        if (candidate) {
            throw ApiError.BadRequest(`A user with email=${email} already exists`)
        }

        const hashedPassword = await bcrypt.hash(password, 3);
        const activationLink = v4();
        const resetPasswordLink = v4();
        const user = await UserModel.create({ email, password: hashedPassword, activationLink, resetPasswordLink });
        await mailService.sendActivationMail(
            email,
            vars.apiUrl + path.join('/user/activate/', activationLink)
        );
        //@ts-ignore
        const userDto = user.toDto();
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async resetPassword(email: string) {
        const candidate = await UserModel.findOne({ email });
        if (!candidate) {
            throw ApiError.BadRequest(`A user with email=${email} not exists`)
        }

        await mailService.sendResetPasswordMail(
            email,
            vars.clientUrl + `/remind-password-confirm?token=${candidate.resetPasswordLink}&email=${email}`
        );
    }

    async resetPasswordConfirm(email: string, resetPasswordLink: string, password: string) {
        const candidate = await UserModel.findOne({ email, resetPasswordLink });
        if (!candidate) {
            throw ApiError.BadRequest(`Error`)
        }

        const hashedPassword = await bcrypt.hash(password, 3);

        candidate.resetPasswordLink = v4();
        candidate.password = hashedPassword;
        await candidate.save();
    }

    async activate(activationLink: string) {
        const user = await UserModel.findOne({ activationLink });
        if (!user) {
            throw ApiError.BadRequest("Некорректная ссылка активации");
        }

        user.isActivated = true;
        await user.save();
    }

    async login(email: string, password: string) {
        const user = await UserModel.findOne({email})
        if (!user) {
            throw ApiError.BadRequest('User with this email not found')
        }

        const isPassEquals = await bcrypt.compare(password, user.password);

        if (!isPassEquals) {
            throw ApiError.BadRequest('Password is incorrect')
        }
//@ts-ignore
        const userDto = user.toDto();
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async logout(refreshToken: string) {
        const token = await tokenService.removeToken(refreshToken);
        return token;
    }

    async refresh(refreshToken: string) {
        console.log('refreshToken',refreshToken)
        if (!refreshToken) {
            throw ApiError.UnauthorizedError();
        }
        const userData = await tokenService.validateRefreshToken(refreshToken);
        const tokenFromDb = await tokenService.findToken(refreshToken);
        if (!userData || !tokenFromDb) {
            throw ApiError.UnauthorizedError();
        }

        const user = await userModel.findById(userData.id);
//@ts-ignore
        const userDto = user.toDto();
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }
}

export const userService = new UserService();