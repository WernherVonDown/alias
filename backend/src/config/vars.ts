import dotenv from 'dotenv';
import { parseNumber } from '../utils/parsers/parserNumber';

dotenv.config();

export const vars = Object.freeze({
    PORT: process.env.PORT || 5000,
    socketPort: parseNumber(process.env.SOCKET_PORT, 3008),
    apiUrl: process.env.API_URL || 'http://localhost:8000',
    clientUrl: process.env.CLIENT_URL || 'http://localhost:3000',
    mongo: {
        url: process.env.MONGO_URL || 'mongodb://mongo:27017/aliasgame',
    },
    jwt: {
        accessSecret: process.env.JWT_ACCESS_SECRET || 'access_secret',
        refreshSecret: process.env.JWT_REFRESH_SECRET || 'refresh_secret',
    },
    email: {
        service: process.env.EMAIL_SERVICE || 'GMAIL',
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD,
        },
    },
    redisUrl: process.env.REDIS_URL || 'redis://localhost:7000/1',
    tanalyticsToken: process.env.TANALYTICS_TOKEN || '',
})