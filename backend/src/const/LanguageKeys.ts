export enum LanguageKeys {
    english = 'english',
    french = 'french',
    german = 'german',
    chinese = 'chinese',
}