export interface IGetTranslationResponse {
    text: string,
    source: string,
    target: string,
    translations: string[],
    context: {
        examples:
        {
            source: string,
            target: string,
            source_phrases:
            {
                phrase: string,
                offset: number,
                length: number
            }[],
            target_phrases: {
                phrase: string,
                offset: number,
                length: number
            }[],
        }[],
        rude: boolean
    }, // or null
    detected_language: string,
    voice: string | null
}

export interface IReversoError {
    ok: boolean,
    message: string,
}

export interface IReverso {
    getTranslation: (
        text: string,
        sourceLang: string,
        targetLang: string,
        callback: (err: IReversoError, response: IGetTranslationResponse) => void
    ) => void,
}