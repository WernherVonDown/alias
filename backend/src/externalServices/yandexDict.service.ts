//@ts-ignore
import yandict from "yandict";
// import { vars } from "../config/vars";

yandict.key = 'dict.1.1.20230527T085856Z.59d4f61dea5ca0c5.b305e251112c68e614be6c0e3e1fc850efded000';
// yandict.reload()

export class YandexDict {
    constructor(
        private lang: string,
    ) {}
    async translate(text: string): Promise<any[]> {
        return new Promise(async res => {
            if (!yandict.loaded) {
                console.log("not loaded")
                await new Promise(loadRes => {
                    yandict.onload = loadRes
                })
                console.log("loaded")
            }
            // yandict.onload = async () => {
                // console.log("T", text, this.lang, yandict.loaded)
                const t = await yandict.lookup(text, this.lang).catch((e: any) => console.log("YA ERRROR", e, this.lang));
                // console.log("RES T", t)
                res(t?.def)
            // }
        })
        
    } 

    async translateTo(text: string, lang: string): Promise<any[]> {
        return new Promise(async res => {
            if (!yandict.loaded) {
                console.log("not loaded")
                await new Promise(loadRes => {
                    yandict.onload = loadRes
                })
                console.log("loaded")
            }
            // yandict.onload = async () => {
                // console.log("T", text, this.lang, yandict.loaded)
                const t = await yandict.lookup(text, lang).catch((e: any) => console.log("YA ERRROR", e, lang));
                // console.log("RES T", t)
                res(t?.def)
            // }
        })
        
    } 
}

export const translateInEnglish = new YandexDict('ru-en');
export const translateInFrench = new YandexDict('ru-fr');
export const translateInGerman = new YandexDict('ru-de');
export const translateInChinese = new YandexDict('ru-zh');
export const translateFromChinese = new YandexDict('zh-ru');
export const translateCommon = new YandexDict('ru-en')