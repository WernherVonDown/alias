//@ts-ignore
import Reverso from 'reverso-api'
import type { IReverso } from '../const/reverso';
const reverso: IReverso = new Reverso()
import { LanguageKeys } from "../const/LanguageKeys";

export default class ReversoContext {
    static async getTranslation(wordToTranslate: string, sourceLang: string, targetLang: string): Promise<string> {
        const res = await new Promise((resolve, rej) => {
            try {
                reverso.getTranslation(wordToTranslate, sourceLang, targetLang, (err, res) => {
                    console.log("getTranslation", err, res)
                    if (err) rej(err.message);
                    console.log("RES", res)
                    resolve(sourceLang === 'russian' ? res.translations[0] : Array.from(new Set(res.translations)).slice(0, 2).join(', '))
                })
            } catch (error) {
                console.log("REVERSO ERROR", error)
                rej(error)
            }
            
        })
        return res as string;
    }
} 