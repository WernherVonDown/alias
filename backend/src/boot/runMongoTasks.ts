import { gameService } from "../services/game.service";
import { roomService } from "../services/room.service"

export const runMongoTasks = async () => {
    await roomService.clearAllUsersInAllRooms();
    await gameService.clearGames();
}