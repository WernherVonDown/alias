import { io } from "../config/socket";
import { vars } from "../config/vars";
import { emitter } from "../utils/socket/emitter";

export const startSocket = () => {
    console.log('socket:running:start');
    io.listen(vars.socketPort);
    console.log(`socket:running:complete; started on port: ${vars.socketPort}`);
    emitter.init(io)
};