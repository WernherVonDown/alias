import NRP from 'node-redis-pubsub';
import { createClient } from 'redis';
import { vars } from '../config/vars';
import { RedisEvents } from '../game/const/RedisEvents';

const client = createClient({
    url: vars.redisUrl,
});
client.on('error', err => console.log('Redis Client Error', err));
const config = {
    url: vars.redisUrl,
};

export const nrp = NRP(config);

export const startRedis = async () => {
    await client.connect();
}

export const emitToRedis = (event: RedisEvents, data: any) => {
    nrp.emit(event, data);
}

export const subscribeToRedis = (event: RedisEvents, callback: (data: any) => void) => {
    nrp.on(event, callback);
}

