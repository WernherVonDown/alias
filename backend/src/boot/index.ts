
import { runMigrations } from '../migrations';
import { runMongoTasks } from './runMongoTasks';
import startMongo from './startMongo';
import { startRedis } from './startRedis';
import { startServer } from './startServer';
import { startSocket } from './startSocket';
export const runBootTasks = async () => {
    try {
        await startMongo();
        await startServer();
        startSocket();
        await runMongoTasks()
        await runMigrations()
        await startRedis()
        console.log('Boot succeed!');
    } catch (error) {
        console.log('run boot tasks error', error)
    }
}