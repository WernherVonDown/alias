import type { Server, Socket } from 'socket.io';
import { emitter } from '../utils/socket/emitter';
import { TEXT_CHAT_EVENTS } from './const/textChat/TEXT_CHAT_EVENTS';
import type { ITextChatMessage } from './const/textChat/types';

class TextChat {
    private roomId: string;
    private sockets: Map<string, Socket>;

    constructor(roomId: string) {
        this.roomId = roomId;
        this.sockets = new Map();
    }

    subscribe = (socket: Socket) => {
        socket.on(TEXT_CHAT_EVENTS.sendMessage, this.receiveMessage.bind(this, socket))
    }

    receiveMessage = (socket: Socket, data: ITextChatMessage) => {
        this.emitToToomButSocket(socket, TEXT_CHAT_EVENTS.sendMessage, data);
    }

    add(socket: Socket) {
        this.subscribe(socket);
    }

    emitToToomButSocket(socket: Socket, event: string, data: any) {
        emitter.sendToRoomButUser(socket, this.roomId, event, data);
        // socket.to(this.roomId).emit(event, data);
    }

    emitToRoom(event: string, data: any): void {
        emitter.sendToRoom(this.roomId, event, data)
        // this.io.to(this.roomId).emit(event, data);
    }
}

export default TextChat;