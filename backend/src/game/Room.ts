import type { Socket, Server } from 'socket.io';
import User from './User';
import { COMMON_EVENTS } from './const/events';
import { ROOM_EVENTS } from './const/room/ROOM_EVENTS';
import type { IRoomCreate } from './const/room/types';
import TextChat from './TextChat';
import Game from './Game';
import { USER_EVENTS } from './const/user/USER_EVENTS';
import { VIDEOCHAT_EVENTS } from './const/videoChat/VIDEOCHAT_EVENTS';
import { emitter } from '../utils/socket/emitter';

class Room {
    public roomId: string;
    protected users: Map<string, User>;
    private textChat: TextChat;
    private game: Game;

    constructor({ roomId, wordLists }: IRoomCreate) {
        this.roomId = roomId;
        this.users = new Map();
        this.textChat = new TextChat(roomId);
        this.game = new Game({ roomId, wordLists });
    }

    subscribe(socket: Socket): void {
        socket.on(COMMON_EVENTS.disconnect, this.leave.bind(this, socket));
        socket.on(VIDEOCHAT_EVENTS.changeDeviceEnabled, this.onChangeDeviceEnabled.bind(this, socket))
    }

    onChangeDeviceEnabled = (socket: Socket, data: {video: boolean, audio: boolean}) => {
        const user = this.users.get(socket.id)
        if (user) {
            user.tracksEnabled = data;
            this.emitToToomButSocket(socket, VIDEOCHAT_EVENTS.changeDeviceEnabled, {userId:user.id ,data});
        }
    }

    join = (socket: Socket, userName: string) => {
        console.log("JOIN MODAL", {userName})
        socket.join(this.roomId);
        this.subscribe(socket);
        const teamColor = this.game.addUserToTeam(socket.id); 
        console.log('join', userName, teamColor)
        const newUser = new User(userName, teamColor, socket.id, socket);
        this.users.set(socket.id, newUser);
        const user = { userName, roomId: this.roomId, id: socket.id, teamColor };
        // socket.emit(ROOM_EVENTS.joinRoom, user);
        this.emitToRoom(ROOM_EVENTS.userJoin, user);
        this.sendUsersList()
        this.textChat.add(socket);
        this.game.add(socket, newUser);
        this.game.sendTeamsScore()
        // socket.emit(ROOM_EVENTS.joinRoom, user)
        return this.getUsersDto();
    }

    sendUsersList = () => {
        const users = this.getUsersDto();
        this.emitToRoom(ROOM_EVENTS.usersData, users);
    }

    getUsersDto = () => {
        return Array.from(this.users, ([name, value]) => (value.toDTO()));
    }

    leave(socket: Socket) {
        const teamId = this.users.get(socket.id)?.teamColor || '' as string;
        const newUserTeam = this.game.leaveTeam(socket.id, teamId);
        if (newUserTeam) {
            const user = this.users.get(newUserTeam.userId)
            if (user) {
                console.log('newUserTeam', newUserTeam, user.teamColor);
                user.setTeamColor(newUserTeam.newTeamId);
                const userDto = user.toDTO();
                user.emit(USER_EVENTS.userChanged, userDto);
            }
        }

        this.game.delete(socket.id)

        this.users.delete(socket.id);
        this.sendUsersList();
    }

    emitToRoom(event: string, data: any): void {
        emitter.sendToRoom(this.roomId, event, data);
    }

    emitToToomButSocket(socket: Socket, event: string, data: any) {
        emitter.sendToRoomButUser(socket, this.roomId, event, data);
        // socket.to(this.roomId).emit(event, data);
    }
}

export default Room;