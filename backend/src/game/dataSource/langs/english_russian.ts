export default [
    {
        "foreignLang": "as [ æz ] — как",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "I [ aɪ ] — я",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "his [ hɪz ] — его",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "that [ ðæt ] — что / тот",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "he [ hiː ] — он",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "was [ wɒz ] — был",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "for [ fɔː r] — для",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "on [ ɒn ] — на",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "are [ ɑː r] — являются",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "with [ wɪð ] — с",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "they [ ðeɪ ] — они",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "be [ biː ] — быть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "at [ ət ] — на",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "one [ wʌn ] — один",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "have [ hæv ] — иметь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "this [ ðɪs ] — это",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "from [ frɒm ] — из",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "by [ baɪ ] — от",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hot [ hɒt ] — горячий / жарко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "word [ wɜːd ] — слово",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "but [ bʌt ] — но",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "what [ wɒt ] — что",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "some [ sʌm ] — некоторый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "is [ ɪz ] — является",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "it [ ɪt ] — это",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "you [ juː ] — ты",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "or [ ɔː r] — или",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "had [ hæd ] — имел",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "the [ ðiː ]",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "of [ əv ] — из / о",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "to [ tuː ] — в / к",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "and [ ænd ] — и",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "a [ ə ]",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "in [ ɪn ] — в",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "we [ wiː ] — мы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "can [ kæn ] — может",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "out [ aʊt ] — из / вне",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "other [ ˈʌð.ə r] — другой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "were [ wɜː r] — были",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "which [ wɪtʃ ] — который",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "do [ duː ] — делать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "their [ ðeə r] — их",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "time [ taɪm ] — время",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "if [ ɪf ] — если",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "will [wɪl] — будет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "how [ haʊ ] — как",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "said [ sed ] — говорить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "an [ æn ]",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "each [ iːtʃ ] — каждый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tell [ tel ] — говорить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "does [ dʌz ] — делает",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "set [ set ] — задавать / комплект",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "three [ θriː ] — три",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "want [ wɒnt ] — хотеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "air [ eə r] — воздух",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "well [wel] — хорошо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "also [ ˈɔːl.səʊ ] — также",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "play [ pleɪ ] — играть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "small [ smɔːl ] — маленький",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "end [ end ] — конец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "put [ pʊt ] — ставить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "home [ həʊm ] — дом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "read [ riːd ] — читать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hand [ hænd ] — рука",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "port [ pɔːt ] — порт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "large [ lɑːdʒ ] — большой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spell [ spel ] — читать по буквам / орфография",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "add [ æd ] — добавлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "even [ ˈiː.v ə n ] — даже",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "land [ lænd ] — земля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "here [ hɪə r] — здесь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "must [ mʌst ] — должен",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "big [ bɪɡ ] — большой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "high [ haɪ ] — высокий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "such [ sʌtʃ ] — такой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "follow [ ˈfɒl.əʊ ] — следовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "act [ ækt ] — действовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "why [ waɪ ] — почему",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ask [ ɑːsk ] — спрашивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "men [men] — люди",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "change [ tʃeɪndʒ ] — менять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "went [ went ] — отправился",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "light [ laɪt ] — свет / легкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kind [ kaɪnd ] — добрый / разновидность",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "off [ ɒf ] — от / выключенный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "need [ niːd ] — необходимость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "house [ haʊs ] — дом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "picture [ ˈpɪk.tʃə r] — картина / фото",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "try [ traɪ ] — пробовать / пытаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "us [ ʌs ] — нас",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "again [ əˈɡen ] — снова",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "animal [ ˈæn.ɪ.məl ] — животное",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "point [ pɔɪnt ] — точка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mother [ ˈmʌð.ə r] — мать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "world [ wɜːld ] — мир",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "near [ nɪə r] — близко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "build [ bɪld ] — строить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "self [ self ] — себя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "earth [ ɜːθ ] — земля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "father [ ˈfɑː.ðə r] — отец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "any [ ˈen.i ] — любой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "new [ njuː ] — новый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "work [ wɜːk ] — работа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "part [pɑːt] — часть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "take [ teɪk ] — брать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "get [get] — получать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "place [ pleɪs ] — место",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "made [ meɪd ] — сделать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "live [ lɪv ] — жить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "where [ weə r] — где",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "after [ ˈɑːf.tə r] — после",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "back [ bæk ] — назад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "little [ˈlɪt.əl] — немного",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "only [ ˈəʊn.li ] — только",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "round [ raʊnd ] — круглый / вокруг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "man [ mæn ] — мужчина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "year [ jɪə r] — год",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "came [ keɪm ] — пришел",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "show [ ʃəʊ ] — показывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "every [ ˈev.ri ] — каждый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "good [ ɡʊd ] — хороший",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "me [ miː ] — мне",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "give [ ɡɪv ] — давать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "our [ aʊə r] — наш",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "under [ ˈʌn.də r] — под",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "name [ neɪm ] — имя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "very [ ˈver.i ] — очень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "through [ θruː ] — через",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "just [ dʒʌst ] — только / просто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "form [ fɔːm ] — вид, форма / образовывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sentence [ ˈsen.təns ] — предложение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "great [ ɡreɪt ] — отличный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "think [ θɪŋk ] — думать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "say [ seɪ ] — говорить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "help [help] — помогать, помощь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "low [ ləʊ] — низкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "line [ laɪn ] — линия, черта",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "differ [ ˈdɪf.ə r] — отличаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "turn [ tɜːn ] — поворачивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cause [ kɔːz ] — причина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "much [ mʌtʃ ] — много",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mean [miːn] — значить / подлый / средний",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "before [ bɪˈfɔː r] — до",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "move [ muːv ] — двигать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "right [ raɪt ] — право / верный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "boy [ bɔɪ ] — мальчик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "old [ əʊld ] — старый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "too [ tuː ] — тоже",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "same [ seɪm ] — также",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "she [ ʃiː ] — она",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "all [ ɔːl ] — все",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "there [ ðeə r] — там",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "when [ wen ] — когда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "up [ ʌp ] — вверх / подниматься",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "use [ juːz ] — использовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "your [ jɔː r] — твой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "way [ weɪ ] — путь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "about [ əˈbaʊt ] — о",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "many [ ˈmen.i ] — много",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "then [ ðen ] — затем",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "them [ ðem ] — их",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "write [ raɪt ] — писать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "would [ wʊd ] — бы / было бы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "like [ laɪk ] — нравиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "so [ səʊ ] — так",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "these [ ðiːz ] — эти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "her [ hɜː r] — ее",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "long [ lɒŋ ] — долго / длинный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "make [ meɪk ] — делать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thing [ θɪŋ ] — вещь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "see [ siː ] — видеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "him [ hɪm ] — его",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "two [ tuː ] — два / двое",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "has [ hæz ] — имеет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "look [ lʊk ] — смотреть / взгляд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "more [ mɔː r] — больше",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "day [ deɪ ] — день",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "could [ kʊd ] — мог",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "go [ ɡəʊ ] — идти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "come [ kʌm ] — приходить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "did [ dɪd ] — сделал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "number [ ˈnʌm.bə r] — номер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sound [ saʊnd ] — звук",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "no [ nəʊ ] — нет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "most [ məʊst ] — самый / большинство",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "people [ ˈpiː.p ə l ] — люди",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "my [ maɪ ] — мой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "over [ ˈəʊ.və r] — над",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "know [ nəʊ ] — знать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "water [ ˈwɔː.tə r] — вода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "than [ ðæn ] — чем",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "call [ kɔːl ] — звонить / звонок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "first [ ˈfɜːst ] — первый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "who [ huː ] — кто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "may [ meɪ ] — может",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "down [ daʊn ] — вниз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "side [ saɪd ] — сторона",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "been [ biːn ] — был",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "now [ naʊ ] — сейчас",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "find [ faɪnd ] — найти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "head [ hed ] — голова",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stand [stænd] — стоять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "own [ əʊn ] — свой / владеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "page [ peɪdʒ ] — страница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "should [ ʃʊd ] — должен",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "country [ ˈkʌn.tri ] — страна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "found [ faʊnd ] — найденный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "answer [ ˈæn·sər ] — ответ / отвечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "school [ skuːl ] — школа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "grow [ ɡrəʊ ] — расти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "study [ ˈstʌd.i ] — изучать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "still [ stɪl ] — все еще",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "learn [ lɜːn ] — учиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "plant [ plɑːnt ] — растение / сажать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cover [ ˈkʌv.ə r] — накрывать / обложка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "food [ fuːd ] — еда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sun [ sʌn ] — солнце",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "four [ fɔː r] — четыре",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "between [ bɪˈtwiːn ] — между",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "state [ steɪt ] — состояние, государство, штат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "keep [ kiːp ] — держать, продолжать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "eye [ aɪ ] — глаз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "never [ ˈnev.ə r] — никогда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "last [ lɑːst ] — последний",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "let [ let ] — позволять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thought [ θɔːt ] — мысль / думал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "city [ ˈsɪt.i ] — город",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tree [ triː ] — дерево",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cross [ krɒs ] — пересекать / крест",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "farm [ fɑːm ] — ферма",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hard [ hɑːd ] — тяжелый / твердый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "start [ stɑːt ] — начинать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "might [ maɪt ] — может быть / мощь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "story [ ˈstɔː.ri ] — история",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "saw [ sɔː ] — увидел / пила",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "far [ fɑː r] — далеко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sea [ siː ] — море",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "draw [ drɔː ] — рисовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "left [ left ] — лево / оставил",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "late [ leɪt ] — поздно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "run [ rʌn ] — бежать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "don’t [ dəʊnt ] — не",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "while [ waɪl ] — в то время, как",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "press [ pres ] — нажимать / давить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "close [ kləʊz ] — близко / закрывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "night [ naɪt ] — ночь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "real [ rɪəl ] — реальный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "life [ laɪf ] — жизнь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "few [ fjuː ] — несколько",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "north [ nɔːθ ] — север",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "book [ bʊk ] — книга",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "carry [ ˈkær.i ] — нести",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "took [ tʊk ] — взял",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "science [ ˈsaɪ.əns ] — наука",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "eat [ iːt ] — есть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "room [ ruːm ] — комната",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "friend [ frend ] — друг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "began [ bɪˈɡæn ] — началось",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "idea [ aɪˈdɪə ] — идея",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fish [ fɪʃ ] — рыба",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mountain [ ˈmaʊn.tɪn ] — гора",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stop [ stɒp ] — остановка / останавливать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "once [ wʌns ] — однажды",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "base [ beɪs ] — базовый / основывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hear [ hɪə r] — слышать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "horse [ hɔːs ] — лошадь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cut [ kʌt ] — резать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sure [ ʃɔː r] — конечно / уверен",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "watch [ wɒtʃ ] — смотреть / часы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "colour [ ˈkʌl.ə r] — цвет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "face [ feɪs ] — лицо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wood [ wʊd ] — дерево",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "main [ meɪn ] — главный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "open [ ˈəʊ.p ə n ] — открыть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "seem [ siːm ] — казаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "together [ təˈɡeð.ə r] — вместе",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "next [ nekst ] — следующий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "white [ waɪt ] — белый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "children [ ˈtʃɪl.drən ] — дети",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "begin [ bɪˈɡɪn ] — начинать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "got [ ɡɒt ] — получил",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "walk [ wɔːk ] — гулять / прогулка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "example [ ɪɡˈzɑːm.p ə l ] — пример",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ease [ iːz ] — легкость / облегчать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "paper [ ˈpeɪ.pə r] — бумага",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "group [ ɡruːp ] — группа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "always [ ˈɔːl.weɪz ] — всегда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "music [ ˈmjuː.zɪk ] — музыка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "those [ ðəʊz ] — те",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "both [ bəʊθ ] — оба",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mark [ mɑːk ] — отметка / отмечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "often [ ˈɒf. ə n ] — часто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "letter [ ˈlet.ə r] — письмо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "until [ ənˈtɪl ] — до тех пор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mile [ maɪl ] — миля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "river [ ˈrɪv.ə r] — река",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "car [ kɑː r] — машина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "feet [ fiːt ] — ноги",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "care [ keə r] — заботиться / уход",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "second [ ˈsek. ə nd ] — секунда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "enough [ ɪˈnʌf ] — достаточно / довольно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "plain [ pleɪn ] — простой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "girl [ ɡɜːl ] — девочка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "usual [ ˈjuː.ʒu.əl ] — обычно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "young [ jʌŋ ] — молодой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ready [ ˈred.i ] — готовый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "above [ əˈbʌv ] — выше",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ever [ ˈev.ə r] — когда-либо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "red [ red ] — красный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "list [ lɪst ] — список",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "though [ ðəʊ ] — хоть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "feel [ fiːl ] — чувствовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "talk [ tɔːk ] — говорить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bird [ bɜːd ] — птица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "soon [ suːn ] — скоро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "body [ ˈbɒd.i ] — тело",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dog [ dɒɡ ] — собака",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "family [ ˈfæm. ə l.i ] — семья",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "direct [ daɪˈrekt ] — непосредственный / направлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pose [ pəʊz ] — поза / позировать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leave [ liːv ] — покидать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "song [ sɒŋ ] — песня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "measure [ ˈmeʒ.ə r] — мера / измерять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "door [ dɔː r] — дверь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "product [ ˈprɒd.ʌkt ] — продукт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "black [ blæk ] — черный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "short [ ʃɔːt ] — короткий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "numeral [ ˈnjuː.mə.rəl ] — числительное",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "class [ klɑːs ] — класс / сорт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wind [ wɪnd ] — ветер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "question [ ˈkwes.tʃən ] — вопрос",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "happen [ ˈhæp. ə n ] — случаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "complete [ kəmˈpliːt ] — полный / завершенный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ship [ʃɪp] — корабль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "area [ ˈeə.ri.ə ] — площадь / зона",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "half [ hɑːf ] — половина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rock [ rɒk ] — рок / камень / скала",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "order [ ˈɔː.də r] — заказ / порядок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fire [faɪər]— огонь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "south [ saʊθ ] — юг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "problem [ ˈprɒb.ləm ] — проблема",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "piece [ piːs ] — часть / кусок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "told [ təʊld ] — говорил",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "knew [ njuː ] — знал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pass [ pɑːs ] — проходить / передача",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "since [ sɪns ] — с / поскольку",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "top [ tɒp ] — верх / вершина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "whole [ həʊl ] — весь / целый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "king [ kɪŋ ] — король",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "street [ striːt ] — улица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "inch [ ɪntʃ ] — дюйм",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "multiply [ ˈmʌl.tɪ.plaɪ ] — умножать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nothing [ ˈnʌθ.ɪŋ ] — ничего",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "course [ kɔːs ] — курс / блюдо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stay [ steɪ ] — оставаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wheel [ wiːl ] — колесо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "full [ fʊl ] — полный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "force [ fɔːs ] — сила / заставлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "blue [ bluː ] — голубой / синий / грустный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "object [ ˈɒb.dʒɪkt ] — объект",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "decide [ dɪˈsaɪd ] — решать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "surface [ ˈsɜː.fɪs ] — поверхность",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "deep [ diːp ] — глубоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "moon [ muːn ] — луна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "island [ ˈaɪ.lənd ] — остров",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "foot [ fʊt ] — нога",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "system [ ˈsɪs.təm ] — система",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "busy [ ˈbɪz.i ] — занят",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "test [ test ] — тест / проверять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "record [ rɪˈkɔːd ] — записывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "boat [ bəʊt ] — лодка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "common [ ˈkɒm.ən ] — общий / обычный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gold [ ɡəʊld ] — золото",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "possible [ ˈpɒs.ə.b ə l ] — возможный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "plane [ pleɪn ] — самолет / плоскость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stead [ sted ] — место",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dry [ draɪ ] — сухой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wonder [ ˈwʌn.də r] — чудо / удивляться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "laugh [ lɑːf ] — смех / смеяться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thousand [ ˈθaʊ.z ə nd ] — тысяча",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ago [ əˈɡəʊ ] — тому назад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ran [ ræn ] — побежал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "check [ tʃek ] — проверять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "game [ ɡeɪm ] — игра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shape [ ʃeɪp ] — форма",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "equate [ ɪˈkweɪt ] — приравнивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "OK [ ˌəʊˈkeɪ ] — хорошо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "miss [ mɪs ] — скучать / пропускать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "brought [ brɔːt ] — привел",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heat [ hiːt ] — жара, высокая температура",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "snow [ snəʊ ] — снег",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tire [ taɪə r] — шина / утомлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bring [ brɪŋ ] — приносить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "yes [ jes ] — да",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "distant [ ˈdɪs.t ə nt ] — отдаленный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fill [ fɪl ] — наполнять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "east [ iːst ] — восток",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "paint [ peɪnt ] — краска / красить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "language [ ˈlæŋ.ɡwɪdʒ ] — язык",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "among [ əˈmʌŋ ] — среди",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "unit [ ˈjuː.nɪt ] — единица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "power [ paʊə r] — мощность / сила",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "town [ taʊn ] — город",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fine [ faɪn ] — отлично",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "certain [ ˈsɜː.t ə n ] — определенный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fly [ flaɪ ] — летать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fall [fɔːl] — падать / осень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lead [ liːd ] — вести / руководить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cry [ kraɪ ] — плакать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dark [ dɑːk ] — темный, темнота",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "machine [ məˈʃiːn ] — машина / механизм",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "note [ nəʊt ] — примечание / отмечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wait [ weɪt ] — ждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "plan [ plæn ] — план",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "figure [ ˈfɪɡ.ə r] — цифра / фигура",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "star [ stɑː r] — звезда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "box [ bɒks ] — коробка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "noun [ naʊn ] — существительное",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "field [ fiːld ] — поле",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rest [ rest ] — остальное / отдыхать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "correct [ kəˈrekt ] — поправлять / правильный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "able [ ˈeɪ.b ə l ] — в состоянии / способный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pound [ paʊnd ] — фунт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "done [ dʌn ] — сделанный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beauty [ ˈbjuː.ti ] — красота",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "drive [ draɪv ] — водить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stood [ stʊd ] — стоял",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "contain [ kənˈteɪn ] — содержать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "front [ frʌnt ] — перед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "teach [tiːtʃ] — обучать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "week [ wiːk ] — неделя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "final [ ˈfaɪ.n ə l ] — финал / последний",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gave [ ɡeɪv ] — дал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "green [ ɡriːn ] — зеленый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oh [ əʊ ] — ох",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quick [ kwɪk ] — быстро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "develop [ dɪˈvel.əp ] — развивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ocean [ ˈəʊ.ʃ ə n ] — океан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "warm [ wɔːm ] — тепло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "free [ friː ] — свободный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "minute [ ˈmɪn.ɪt ] — минута",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "strong [ strɒŋ ] — сильный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "special [ˈspeʃ.əl] — особый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mind [ maɪnd ] — разум / возражать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "behind [ bɪˈhaɪnd ] — позади",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "clear [ klɪə r] — ясный / понятно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tail [ teɪl ] — хвост",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "produce [ prəˈdʒuːs ] — производить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fact [ fækt ] — факт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "space [ speɪs ] — пространство / космос",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heard [ h ɜːd ] — слышал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "best [ best ] — лучший",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hour [ aʊə r] — час",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "better [ ˈbet.ə r] — лучше",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "true [ truː ] — правда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "during [ ˈdʒʊə.rɪŋ ] — в течение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hundred [ ˈhʌn.drəd ] — сотня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "five [ faɪv ] — пять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "remember [ rɪˈmem.bə r] — помнить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "step [ step ] — шаг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "early [ ˈɜː.li ] — ранний / рано",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hold [ həʊld ] — держать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "west [ west ] — запад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ground [ ɡraʊnd ] — земля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "interest [ ˈɪn.trəst ] — интерес",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "reach [ riːtʃ ] — достигать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fast [ fɑːst ] — быстро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "verb [ vɜːb ] — глагол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sing [ sɪŋ ] — петь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "listen [ ˈlɪs. ə n ] — слушать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "six [ sɪks ] — шесть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "table [ ˈteɪ.b ə l ] — стол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "travel [ ˈtræv. ə l ] — путешествовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "less [ les ] — меньше",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "morning [ ˈmɔː.nɪŋ ] — утро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ten [ ten ] — десять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "simple [ ˈsɪm.p ə l ] — просто / простой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "several [ ˈsev. ə r. ə l ] — несколько",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vowel [ vaʊəl ] — гласный (звук)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "towards [ twoʊrdz ] — к / по направлению к",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "war [ wɔː r] — война",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lay [ leɪ ] — лежать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "against [ əˈɡenst ] — против",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pattern [ ˈpæt. ə n ] — узор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "slow [ sləʊ ] — медленно / медленный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "center [ ˈsen.tə r] — центр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "love [ lʌv ] — любовь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "person [ ˈpɜː.s ə n ] — человек",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "money [ ˈmʌn.i ] — деньги",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "serve [ sɜːv ] — обслуживать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "appear [ əˈpɪə r] — появляться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "road [ rəʊd ] — дорога",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "map [ mæp ] — карта",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rain [ reɪn ] — дождь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rule [ ruːl ] — правило",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "govern [ ˈɡʌv. ə n ] — управлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pull [pʊl] — тянуть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cold [ kəʊld ] — холодный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "notice [ ˈnəʊ.tɪs ] — уведомление / замечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "voice [ vɔɪs ] — голос",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "energy [ ˈen.ə.dʒi ] — энергия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hunt [ hʌnt ] — охотититься",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "probable [ ˈprɒb.ə.b ə l ] — вероятный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bed [ bed ] — кровать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "brother [ ˈbrʌð.ə r] — брат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "egg [ eɡ ] — яйцо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ride [ raɪd ] — ездить / поездка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cell [ sel ] — клетка / мобильный ( амер. )",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "believe [ bɪˈliːv ] — верить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "perhaps [ pəˈhæps ] — возможно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pick [ pɪk ] — выбирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sudden [ ˈsʌd. ə n ] — внезапный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "count [ kaʊnt ] — считать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "square [ skweə r] — квадрат / площадь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "reason [ ˈriː.z ə n ] — причина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "length [ leŋθ ] — длина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "represent [ ˌrep.rɪˈzent ] — представлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "art [ ɑːt ] — искусство",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "subject [ ˈsʌb.dʒekt ] — предмет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "region [ ˈriː.dʒ ə n ] — регион",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "size [ saɪz ] — размер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vary [ ˈveə.ri ] — меняться / варьировать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "settle [ ˈset. ə l ] — селиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "speak [ spiːk ] — говорить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "weight [ weɪt ] — вес",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "general [ ˈdʒen. ə r. ə l ] — главный / общий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ice [ aɪs ] — лед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "matter [ ˈmæt.ə r] — иметь значение / предмет / материя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "circle [ ˈsɜː.k ə l ] — круг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pair [ peə r] — пара",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "include [ ɪnˈkluːd ] — включать в себя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "divide [ dɪˈvaɪd ] — разделять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "syllable [ ˈsɪl.ə.b ə l ] — слог",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "felt [ felt ] — чувствовал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "grand [ ɡrænd ] — большой / великий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ball [ bɔːl ] — мяч",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "yet [ jet ] — еще",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wave [ weɪv ] — волна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "drop [ drɒp ] — капля / бросать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heart [ hɑːt ] — сердце",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "AM [ ˌ eɪ ˈem ] — до полудня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "present [ ˈprez. ə nt ] — подарок / настоящий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heavy [ ˈhev.i ] — тяжелый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dance [ dɑːns ] — танец / танцевать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "engine [ ˈen.dʒɪn ] — двигатель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "position [ pəˈzɪʃ. ə n ] — позиция",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "arm [ ɑːm ] — рука",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wide [ waɪd ] — широкий / повсюду",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sail [ seɪl ] — паруса / плавать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "material [ məˈtɪə.ri.əl ] — материал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fraction [ ˈfræk.ʃ ə n ] — доля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "forest [ ˈfɒr.ɪst ] — лес",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sit [ sɪt ] — сидеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "race [ reɪs ] — гонка / раса",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "window [ ˈwɪn.dəʊ ] — окно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "store ( амер. ) [ stɔː r] — магазин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "summer [ ˈsʌm.ə r] — лето",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "train [ treɪn ] — поезд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sleep [ sliːp ] — спать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "prove [ pruːv ] — доказывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "alone [ əˈləʊn ] — один / одинокий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leg [ leɡ ] — нога",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "exercise [ ˈek.sə.saɪz ] — упражнение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wall [ wɔːl ] — стена",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "catch [ kætʃ ] — поймать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mount [ maʊnt ] — взбираться / монтировать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wish [ wɪʃ ] — желать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sky [ skaɪ ] — небо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "board [ bɔːd ] — доска",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "joy [ dʒɔɪ ] — радость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "winter [ ˈwɪn.tə r] — зима",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "somebody [ ˈsʌm.bə.di ] — кто-то",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "written [ ˈrɪt. ə n ] — написанный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wild [ waɪld ] — дикий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "instrument [ ˈɪn.strə.mənt ] — инструмент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kept [ kept ] — сохранил",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "glass [ ɡlɑːs ] — стекло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "grass [ ɡrɑːs ] — трава",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cow [ kaʊ ] — корова",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "job [ dʒɒb ] — работа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "edge [ edʒ ] — край",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sign [ saɪn ] — подписывать / знак",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "visit [ ˈvɪz.ɪt ] — посещать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "past [ pɑːst ] — прошлое",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "soft [ sɒft ] — мягкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fun [ fʌn ] — веселый / веселье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bright [ braɪt ] — яркий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gas [ ɡæs ] — газ / бензин ( амер. )",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "weather [ ˈweð.ə r] — погода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "month [ mʌnθ ] — месяц",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "million [ ˈmɪl.jən ] — миллион",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bear [ beə r] — медведь / выносить (терпеть)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "finish [ ˈfɪn.ɪʃ ] — закончить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "happy [ ˈhæp.i ] — счастливый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hope [ həʊp ] — надежда / надеяться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "flower [ flaʊə r] — цветок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "clothes [ kləʊðz ] — одежда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "strange [ streɪndʒ ] — странный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gone [ ɡɒn ] — ушедший",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trade [ treɪd ] — торговля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "melody [ ˈmel.ə.di ] — мелодия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trip [ trɪp ] — поездка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "office [ ˈɒf.ɪs ] — офис",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "receive [ rɪˈsiːv ] — получать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "row [ rəʊ ] — ряд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mouth [ maʊθ ] — рот",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "exact [ ɪɡˈzækt ] — точный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "symbol [ ˈsɪm.b ə l ] — символ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die [ daɪ ] — умирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "least [ liːst ] — наименее / меньше всего",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trouble [ ˈtrʌb. ə l ] — проблема",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shout [ ʃaʊt ] — кричать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "except [ ɪkˈsept ] — кроме / исключая",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "writer [ ˈraɪ.tə r] — писатель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "seed [siːd] — семя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tone [ təʊn ] — тон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "join [ dʒɔɪn ] — присоединяться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "suggest [ səˈdʒest ] — предлагать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "clean [ kliːn ] — чисто / чистый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "break [ breɪk ] — перерыв / ломать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lady [ ˈleɪ.di ] — леди",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "yard [ jɑːd ] — двор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rise [ raɪz ] — рост / подниматься",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bad [ bæd ] — плохой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "blow [ bləʊ ] — удар / дуть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oil [ ɔɪl ] — масло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "blood [ blʌd ] — кровь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "touch [ tʌtʃ ] — трогать / прикосновение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "goal [ ɡəʊl ] — цель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cent [ sent ] — цент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mix [ mɪks ] — смесь / смешивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "team [ tiːm ] — команда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wire [ waɪə r] — проволока",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cost [ kɒst ] — стоимость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lost [ lɒst ] — потерял / потерянный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "brown [ braʊn ] — коричневый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wear [ weə r] — носить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "garden [ ˈɡɑː.d ə n ] — сад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "equal [ ˈiː.kwəl ] — равный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sent [ sent ] — отправил / отправленный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "choose [ tʃuːz ] — выбирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "feel [ fiːl ] — чувствовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fit [ fɪt ] — соответствовать / подходить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "flow [ fləʊ ] — поток / течь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fair [ feə r] — ярмарка / честный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bank [ bæŋk ] — банк",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "collect [ kəˈlekt ] — собирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "save [ seɪv ] — сохранять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "control [ kənˈtrəʊl ] — контроль / контролировать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "decimal [ ˈdes.ɪ.məl ] — десятичный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ear [ ɪə r] — ухо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "else [ els ] — еще",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quite [ kwaɪt ] — довольно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "broke [ brəʊk ] — сломал / без денег",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "case [ keɪs ] — случай",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "middle [ ˈmɪd. ə l ] — средний",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kill [ kɪl ] — убивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "son [ sʌn ] — сын",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lake [ leɪk ] — озеро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "moment [ ˈməʊ.mənt ] — момент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "scale [ skeɪl ] — шкала",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "loud [ laʊd ] — громко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spring [ sprɪŋ ] — весна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "observe [ əbˈzɜːv ] — наблюдать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "child [ tʃaɪld ] — ребенок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "straight [ streɪt ] — прямо / прямой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "consonant [ ˈkɒn.sə.nənt ] — согласный (звук)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nation [ ˈneɪ.ʃ ə n ] — нация",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dictionary [ ˈdɪk.ʃ ə n. ə r.i ] — словарь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "milk [ mɪlk ] — молоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "speed [ spiːd ] — скорость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "method [ ˈmeθ.əd ] — метод",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "organ [ ˈɔː.ɡən ] — орган",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pay [ peɪ ] — платить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "age [ eɪdʒ ] — возраст",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "section [ ˈsek.ʃ ə n ] — раздел",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dress [ dres ] — платье / одеваться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cloud [ klaʊd ] — облако",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "surprise [ səˈpraɪz ] — сюрприз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quiet [ ˈkwaɪ.ət ] — тихо / тихий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stone [ stəʊn ] — камень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tiny [ ˈtaɪ.ni ] — крошечный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "climb [ klaɪm ] — подъем / взбираться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cool [ kuːl ] — крутой / прохладно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "design [ dɪˈzaɪn ] — дизайн",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "poor [ pɔː r] — бедный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lot [ lɒt ] — много",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "explain [ ɪkˈspleɪn ] — объяснять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bottom [ ˈbɒt. ə m ] — низ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "key [ kiː ] — ключ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "iron [ aɪən ] — железо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "single [ ˈsɪŋ.ɡ ə l ] — один",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stick [ stɪk ] — придерживаться / приклеивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "flat [ flæt ] — квартира ( брит. ) / плоский",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "twenty [ ˈtwen.ti ] — двадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "skin [ skɪn ] — кожа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "smile [ smaɪl ] — улыбка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "crease [ kriːs ] — складка / мять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hole [ həʊl ] — дыра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jump [ dʒʌmp ] — прыгать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "baby [ ˈbeɪ.bi ] — ребенок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "eight [ eɪt ] — восемь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "village [ ˈvɪl.ɪdʒ ] — деревня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "meet [ miːt ] — встречать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "root [ ruːt ] — корень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "buy [ baɪ ] — покупать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "raise [ reɪz ] — поднимать / повышать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "solve [ sɒlv ] — решать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "metal [ ˈmet. ə l ] — металл",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "whether [ ˈweð.ə r] — будь то / ли",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "push [ pʊʃ ] — давить / нажимать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "seven [ ˈsev. ə n ] — семь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "paragraph [ ˈpær.ə.ɡrɑːf ] — параграф",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "third [ θɜːd ] — третий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shall [ ʃæl ] — должен",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "held [ held ] — держал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hair [ heə r] — волосы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "describe [ dɪˈskraɪb ] — описывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cook [ kʊk ] — готовить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "floor [ flɔː r] — пол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "either [ ˈaɪ.ðə r] — или",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "result [ rɪˈzʌlt ] — результат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "burn [ bɜːn ] — гореть / ожог",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hill [ hɪl ] — холм",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "safe [ seɪf ] — безопасный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cat [ kæt ] — кот / кошка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "century [ ˈsen.tʃ ə r.i ] — век / столетие",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "consider [ kənˈsɪd.ə r] — рассматривать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "type [ taɪp ] — тип / печатать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "law [ lɔː ] — закон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bit [ bɪt ] — немного / часть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "coast [ kəʊst ] — берег",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "copy [ ˈkɒp.i ] — копировать / копия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "phrase [ freɪz ] — фраза / формулировать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "silent [ ˈsaɪlənt ] — тихий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tall [ tɔːl ] — высокий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sand [ sænd ] — песок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "soil [ sɔɪl ] — почва",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "roll [ rəʊl ] — вертеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "temperature [ ˈtem.prə.tʃə r] — температура",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "finger [ ˈfɪŋ.ɡə r] — палец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "industry [ ˈɪn.də.stri ] — индустрия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "value [ ˈvæl.juː ] — значение / стоимость / ценность",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fight [ faɪt ] — драка / бороться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lie [ laɪ ] — ложь / лгать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beat [ biːt ] — бить / ритм",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "excite [ ɪkˈsaɪt ] — возбуждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "natural [ˈnætʃ.ər.əl / ] — натуральный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "view [ vjuː ] — вид",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sense [ sens ] — смысл / чувство",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "capital [ ˈkæp.ɪ.t ə l ] — столица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "won’t [ wəʊnt ] — не будет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chair [ tʃeə r] — стул",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "danger [ ˈdeɪn.dʒə r] — опасность",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fruit [ fruːt ] — фрукт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rich [ rɪtʃ ] — богатый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thick [ θɪk ] — толстый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "soldier [ ˈsəʊl.dʒə r] — солдат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "process [ ˈprəʊ.ses ] — процесс / подвергать обработке",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "operate [ ˈɒp. ə r.eɪt ] — управлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "practice [ ˈpræk.tɪs ] — практиковать / практика",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "separate [ ˈsep ə reɪt ] — разделять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "difficult [ ˈdɪf.ɪ.k ə lt ] — сложный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "doctor [ ˈdɒk.tə r] — доктор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "please [ pliːz ] — пожалуйста / угождать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "protect [ prəˈtekt ] — защищать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "noon [ nuːn ] — полдень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "crop [ krɒp ] — обрезать / урожай",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "modern [ ˈmɒd. ə n ] — современный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "element [ ˈel.ɪ.mənt ] — элемент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hit [ hɪt ] — удар / бить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "student [ ˈstjuː.d ə nt ] — студент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "corner [ ˈkɔː.nə r] — угол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "party [ ˈpɑː.ti ] — вечеринка / партия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "supply [ səˈplaɪ ] — поставка / снабжать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "whose [ huːz ] — чьи",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "locate [ ləʊˈkeɪt ] — размещать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ring [ rɪŋ ] — кольцо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "character [ ˈkær.ək.tə r] — персонаж / характер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "insect [ ˈɪn.sekt ] — насекомое",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "caught [ kɔːt ] — поймал / пойманный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "period [ ˈpɪə.ri.əd ] — период / точка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "indicate [ ˈɪn.dɪ.keɪt ] — указывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "radio [ ˈreɪ.di.əʊ ] — радио",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spoke [ spəʊk ] — говорил",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "atom [ ˈæt.əm ] — атом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "human [ ˈhjuː.mən ] — человек",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "history [ ˈhɪs.t ə r.i ] — история",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "effect [ ɪˈfekt ] — эффект / следствие",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "electric [ iˈlek.trɪk ] — электрический",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "expect [ ɪkˈspekt ] — ожидать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bone [ bəʊn ] — кость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rail [ reɪl ] — рельс / железнодорожный путь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "imagine [ ɪˈmædʒ.ɪn ] — представлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "provide [ prəˈvaɪd ] — предоставлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "agree [ əˈɡriː ] — соглашаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thus [ ðʌs ] — таким образом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gentle [ ˈdʒen.t ə l ] — нежный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "woman [ ˈwʊm.ən ] — женщина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "captain [ ˈkæp.tɪn ] — капитан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "guess [ ɡes ] — предположение / угадывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "necessary [ ˈnes.ə.ser.i ] — необходимый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sharp [ ʃɑːp ] — острый / умный / стильный (о человеке)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wing [ wɪŋ ] — крыло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "create [ kriˈeɪt ] — создавать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "neighbour [ ˈneɪ.bə r] — сосед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wash [ wɒʃ ] — мыть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bat [ bæt ] — летучая мышь / бита",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rather [ ˈrɑː.ðə r] — скорее",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "crowd [ kraʊd ] — толпа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "corn [ kɔːn ] — кукуруза",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "compare [ kəmˈpeə r] — сравнивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "poem [ ˈpəʊ.ɪm ] — стих",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "string [ strɪŋ ] — струна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bell [ bel ] — колокол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "depend [ dɪˈpend ] — зависеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "meat [ miːt ] — мясо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rub [ rʌb ] — тереть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tube [ tʃuːb ] — трубка / метро (в Лондоне)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "famous [ ˈfeɪ.məs ] — известный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dollar [ ˈdɒl.ə r] — доллар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stream [ striːm ] — поток / течь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fear [ fɪə r] — страх / бояться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sight [ saɪt ] — взгляд / вид / зрение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thin [ θɪn ] — тонкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "triangle [ ˈtraɪ.æŋ.ɡ ə l ] — треугольник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "planet [ ˈplæn.ɪt ] — планета",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hurry [ ˈhʌr.i ] — торопиться / спешка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chief [ tʃiːf ] — шеф",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "colony [ ˈkɒl.ə.ni ] — колония",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "clock [ klɒk ] — часы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mine [ maɪn ] — мой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tie [ taɪ ] — галстук / завязывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "enter [ ˈen.tə r] — вход / входить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "major [ ˈmeɪ.dʒə r] — майор / основной",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fresh [ freʃ ] — свежий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "search [ sɜːtʃ ] — поиск / искать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "send [ send ] — посылать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "yellow [ ˈjel.əʊ ] — желтый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gun [ ɡʌn ] — пистолет / ружье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "allow [ əˈlaʊ ] — позволять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "print [ prɪnt ] — печать / печатать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dead [ ded ] — мертвый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spot [ spɒt ] — место / пятно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "desert [ ˈdez.ət ] — пустыня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "suit [ suːt ] — костюм / подходить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "current [ ˈkʌr. ə nt ] — текущий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lift [ lɪft ] — поднимать / лифт ( брит. )",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rose [ rəʊz ] — роза",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "arrive [ əˈraɪv ] — прибывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "master [ ˈmɑː.stə r] — мастер / главный / справляться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "track [ træk ] — трек / след",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "parent [ ˈpeə.r ə nt ] — родитель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shore [ ʃɔː r] — побережье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "division [ dɪˈvɪʒ. ə n ] — деление",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sheet [ ʃiːt ] — лист / простынь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "substance [ ˈsʌb.st ə ns ] — вещество",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "favour [ ˈfeɪ.və r] — одолжение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "connect [ kəˈnekt ] — соединять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "post [ pəʊst ] — почта / после",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spend [ spend ] — тратить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chord [ kɔːd ] — аккорд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fat [ fæt ] — жир / жирный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "glad [ ɡlæd ] — рад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "original [ əˈrɪdʒ. ə n. ə l ] — оригинальный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "share [ ʃeə r ] — делиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "station [ ˈsteɪ.ʃ ə n ] — станция",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dad [ dæd ] — папа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bread [ bred ] — хлеб",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "charge [ tʃɑːdʒ ] — заряд / заряжать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "proper [ ˈprɒp.ə r] — правильный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bar [ bɑː r] — бар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "offer [ ˈɒf.ə r] — предлагать / предложение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "segment [ ˈseɡ.mənt ] — часть / сегмент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "slave [ sleɪv ] — раб",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "duck [ dʌk ] — утка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "instant [ ˈɪn.stənt ] — мгновенный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "market [ ˈmɑː.kɪt ] — рынок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "degree [ dɪˈɡriː ] — температура",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "populate [ ˈpɒp.jə.leɪt ] — населять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "education [ ˌedʒ.uˈkeɪ.ʃ ə n ] — образование",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dear [ dɪə r] — дорогая",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "enemy [ ˈen.ə.mi ] — враг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "reply [ rɪˈplaɪ ] — ответ / отвечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "drink [ drɪŋk ] — напиток / пить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "occur [ əˈkɜː r] — происходить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "support [ səˈpɔːt ] — поддержка / поддерживать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "speech [ spiːtʃ ] — речь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nature [ ˈneɪ.tʃə r] — природа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "range [ reɪndʒ ] — диапазон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "steam [ stiːm ] — пар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "motion [ ˈməʊ.ʃ ə n ] — движение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "path [ pɑːθ ] — путь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "liquid [ ˈlɪk.wɪd ] — жидкость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "violence [ ˈvaɪə.l ə ns ] — жестокость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "meant [ ment ] — значил",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quotient [ ˈkwəʊ.ʃ ə nt ] — частное",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "teeth [ tiːθ ] — зубы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shell [ ʃel ] — ракушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "neck [ nek ] — шея",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oxygen [ ˈɒk.sɪ.dʒən ] — кислород",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sugar [ ˈʃʊɡ.ə r] — сахар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "death [ deθ ] — смерть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pretty [ ˈprɪt.i ] — красивый / довольно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "skill [ skɪl ] — умение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "women [ ˈwɪm.ɪn ] — женщины",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "season [ ˈsiː.z ə n ] — сезон / время года",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "solution [ səˈluː.ʃ ə n ] — решение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "magnet [ ˈmæɡ.nət ] — магнит",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "silver [ ˈsɪl.və r] — серебро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "thanks [ θæŋks ] — спасибо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lunch [ lʌntʃ ] — ужин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "match [ mætʃ ] — совпадать / матч",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "treat [ triːt ] — угощение / обращаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "especially [ ɪˈspeʃ. ə l.i ] — особенно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fail [ feɪl ] — провал / терпеть провал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "afraid [ əˈfreɪd ] — испуганный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "huge [ hjuːdʒ ] — огромный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sister [ ˈsɪs.tə r] — сестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "steel [ stiːl ] — сталь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "discuss [ dɪˈskʌs ] — обсуждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "forward [ ˈfɔː.wəd ] — вперед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "similar [ ˈsɪm.ɪ.lə r] — похожий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "guide [ ɡaɪd ] — руководство / направлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "experience [ ɪkˈspɪə.ri.əns ] — опыт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "score [ skɔː r] — очки / счет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "apple [ ˈæp. ə l ] — яблоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "evidence [ ˈev.ɪ.d ə ns ] — доказательства",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "message [ ˈmes.ɪdʒ ] — сообщение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "movie [ ˈmuː.vi ] — фильм ( амер. )",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "coat [ kəʊt ] — пальто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mass [ mæs ] — масса",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "card [ kɑːd ] — карта",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "band [ bænd ] — группа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rope [ rəʊp ] — веревка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "slip [ slɪp ] — скользить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "win [ wɪn ] — побеждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dream [ driːm ] — мечта / мечтать / сон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "evening [ ˈiːv.nɪŋ ] — вечер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "condition [ kənˈdɪʃ. ə n ] — состояние",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "feed [ fiːd ] — кормить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tool [ tuːl ] — инструмент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "total [ ˈtəʊ.t ə l ] — сумма",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "basic [ ˈbeɪ.sɪk ] — базовый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "smell [ smel ] — запах",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "valley [ ˈvæl.i ] — долина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "not [ nɒt ] — не",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "double [ˈdʌb.əl] — двойной / вдвойне / двойник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "seat [ siːt ] — место",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "continue [ kənˈtɪn.juː ] — продолжать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "news [ njuːz ] — новости",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "police [ pəˈliːs ] — полиция",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hat [ hæt ] — шляпа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sell [ sel ] — продавать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "success [ səkˈses ] — успех",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "company [ ˈkʌm.pə.ni ] — компания",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "security [ sɪˈkjʊə.rə.ti ] — безопасность / охрана",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "event [ ɪˈvent ] — событие",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "particular [ pəˈtɪk.jə.lə r] — особенный / конкретный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "deal [ diːl ] — сделка / часть / разбираться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "swim [ swɪm ] — плавать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "term [ tɜːm ] — срок / семестр / термин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "opposite [ ˈɒp.ə.zɪt ] — напротив / противоположный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wife [ waɪf ] — жена",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shoes [ ʃuːz ] — обувь / туфли",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shoulders [ ˈʃəʊl.də r z ] — плечи",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spread [ spred ] — распространение / распространять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "arrange [ əˈreɪndʒ ] — организовывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "camp [ kæmp ] — лагерь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "invent [ ɪnˈvent ] — изобретать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cotton [ ˈkɒt. ə n ] — хлопок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "born [ bɔːn ] — родиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "determine [ dɪˈtɜː.mɪn ] — определять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quarter [ ˈkwɔː.tə r] — четверть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nine [ naɪn ] — девять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "remove [ rɪˈmuːv ] — удалять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "noise [ nɔɪz ] — шум",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "level [ ˈlev. ə l ] — уровень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chance [ tʃɑːns ] — шанс",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gather [ ˈɡæð.ə r]— собирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shop [ ʃɒp ] — магазин ( брит. )",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stretch [ stretʃ ] — растягивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "throw [ θrəʊ ] — бросать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "shine [ ʃaɪn ] — сиять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "property [ ˈprɒp.ə.ti ] — собственность",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "issue [ ˈɪʃ.uː ] — проблема",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "legal [ ˈliː.ɡ ə l ] — законный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "select [ sɪˈlekt ] — выбирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wrong [ rɒŋ ] — неправильный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gray [ ɡreɪ ] — серый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "repeat [ rɪˈpiːt ] — повторять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "require [ rɪˈkwaɪə r] — требоваться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "broad [ brɔːd ] — широкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "prepare [ prɪˈpeə r] — подготавливать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "salt [ sɒlt ] — соль / соленый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nose [ nəʊz ] — нос",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "plural [ ˈplʊə.r ə l ] — множественное число",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "anger [ ˈæŋ.ɡə r] — гнев",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "claim [ kleɪm ] — запрос / требовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "price [ praɪs ] — цена",
        "motherLang": "",
        "oneWord": true
    }
]