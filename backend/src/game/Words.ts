import type { IWord } from './const/game/types';
import french_russian from './dataSource/langs/french_russian_old';
import english_russian from './dataSource/langs/english_russian';
import german_russian from './dataSource/langs/german_russian';
import { wordsListService } from '../services/wordsList.service';
import type { IWordDto } from '../models/word.model';

// import WordsList from './models/WordsList';

interface ILangs {
    [key: string]: IWord[]
}
const LANGS: ILangs = {
    french_russian,
    english_russian,
    german_russian
}

class Words {
    private wordsSaved: IWordDto[];
    private words: IWordDto[];
    wordLists: string[] = [];
    private removedWords: IWordDto[] = [];

    constructor(wordLists: string[]) {
        this.wordsSaved = [];
        this.words = [];
        this.wordLists = wordLists;
        
        this.prepareData();
    }

    shuffle(array: IWordDto[]): IWordDto[] {
        let currentIndex = array.length, randomIndex;

        // While there remain elements to shuffle...
        while (currentIndex != 0) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            // And swap it with the current element.
            [array[currentIndex], array[randomIndex]] = [
                array[randomIndex], array[currentIndex]];
        }

        return array;
    }

    removeWord = async (wordId: string) => {
        this.wordsSaved.filter(w => {
            if (w.id.toString() === wordId) {
                this.removedWords.push(w)
                return false;
            }
            return true;
        })
    }

    prepareData = async () => {
        this.wordsSaved = await wordsListService.getWords(this.wordLists);
        console.log("WORDS TT", this.wordsSaved, this.wordLists)
        // console.log('prepareData', this.custom)
        // if (!this.custom) {
        //     const langKey = `${this.foreignLang}_${this.motherLang}`;
        //     this.wordsSaved = LANGS[langKey];
        // } else {
        //     // const wordslist = await WordsList.findOne({_id: this.custom}).populate('words');
        //     // this.wordsSaved = wordslist.words;
        // }
    }

    getAllWords(): IWordDto[] {
        return [...this.shuffle(this.wordsSaved)]
    }

    getWord(): IWordDto {
        if (!this.words.length) {
            this.words = [...this.shuffle(this.wordsSaved)];
        }

        return this.words.shift() as IWordDto;
    }
}

export default Words;