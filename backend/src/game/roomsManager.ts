import type { Server, Socket } from 'socket.io';
import { COMMON_EVENTS } from './const/events';
import Room from './Room';
import type { IRoomCreate } from './const/room/types';
import { ROOM_EVENTS } from './const/room/ROOM_EVENTS';

class RoomsManager {
    private rooms: Map<string, Room>;
    private users: Map<string, string>

    constructor() {
        this.rooms = new Map();
        this.users = new Map();
    }

    onDisconnect = (socket: Socket) => {
        const roomId = this.users.get(socket.id);
        if (roomId) {
            this.rooms.get(roomId)?.leave(socket);
            this.users.delete(socket.id)
        }
    }

    createRoom = ({roomId, wordLists}: IRoomCreate) => {
        console.log('createRoom', {roomId, wordLists})
        if (!this.rooms.get(roomId)) {
            this.rooms.set(roomId, new Room({roomId, wordLists}));
        }
    }

    joinRoom(socket: Socket, {roomId, userName}: {roomId: string, userName: string}) {
        // console.log('joinRoom', {roomId, userName}, this.rooms.get(roomId))
        // console.log("JOIN ROOM", this.rooms.get(roomId))
        this.users.set(socket.id, roomId);
        return this.rooms.get(roomId)?.join(socket, userName);
    }
}

export const roomsManager = new RoomsManager();