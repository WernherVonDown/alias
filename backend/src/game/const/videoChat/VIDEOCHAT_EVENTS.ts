export enum VIDEOCHAT_EVENTS {
    startCall = 'videoChat:startCall',
    sendToServer = 'videoChat:sendToServer',
    changeDeviceEnabled = 'videoChat:changeDeviceEnabled',
}