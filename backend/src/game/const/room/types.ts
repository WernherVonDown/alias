export interface IRoomCreate {
    roomId: string;
    wordLists: string[];
}