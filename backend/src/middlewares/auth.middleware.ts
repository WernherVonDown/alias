import type { NextFunction, Request, Response } from "express";
import { ApiError } from "../utils/errors/ApiError";
import { tokenService } from '../services/token.service';
import type { IUserDto } from '../models/user.model';

export interface IGetUserAuthInfoRequest extends Request {
    user: IUserDto;
  }

export const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
    try {
        const authtorizationHeader = req.headers.authtorization as string;
        if (!authtorizationHeader) {
            return next(ApiError.UnauthorizedError());
        }

        const acсessToken = authtorizationHeader.split(' ')[1];

        if(!acсessToken) {
            return next(ApiError.UnauthorizedError());
        }

        const userData = tokenService.validateAccessToken(acсessToken);

        if (!userData) {
            return next(ApiError.UnauthorizedError());
        }
        res.locals.user = userData;
        next();
    } catch (error) {
        return next(ApiError.UnauthorizedError());
    }
}