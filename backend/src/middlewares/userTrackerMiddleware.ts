import { NextFunction, Request, Response } from "express"
import Tanalytics from "tanalytics-sdk"
import { vars } from "../config/vars";
import { generateRandomString } from "../utils/generateRandomString";

const checkIsBot = ({ req }: any) => /bot|googlebot|crawler|spider|robot/i.test(req.headers['user-agent'] || '');

const getTrackId = ({ req, res, ipAddress }: { req: Request, res: Response, ipAddress?: string }) => {
    let trackId = req.cookies.trackId;
    let trackNew = false
    if (!trackId) {
        const oneDatInSeconds = 24 * 60 * 60;
        const expiresDate = new Date();
        expiresDate.setSeconds(expiresDate.getSeconds() + oneDatInSeconds);
        const hiddenIp = ipAddress ? btoa(ipAddress) : "";
        trackId = hiddenIp || generateRandomString(30);
        res.setHeader('Set-Cookie', `trackId=${trackId}; Path=/; HttpOnly Max-Age=${oneDatInSeconds}; Expires=${expiresDate.toUTCString()}`);
        trackNew = true;
    }
    return { trackId, trackNew };
}

export const onTrackLogin = async ({ req, res, tanalytics, ipAddress, isBot, trackNew }: IOnTrackOptions) => {
    const { email } = req.body;
    if (isBot) {
        await tanalytics.sendMessage({ message: `Логин бот ${req.headers['user-agent'] || ''}` });
    } else if (trackNew) {
        await tanalytics.sendMessage({ message: `Логин ${email || ipAddress}\n\n${req.headers['user-agent'] || ''}` }).then((e: any) => console.log("EEEE", e));;
    }
}

export const onTrackRegister = async ({ req, res, ipAddress, tanalytics, isBot, trackNew }: IOnTrackOptions) => {
    const { email } = req.body;
    if (isBot) {
        await tanalytics.sendMessage({ message: `Регистрация бот ${req.headers['user-agent'] || ''}` })
    } else if (trackNew) {
        tanalytics.sendMessage({ message: `Регистрация ${email || ipAddress}\n\n${req.headers['user-agent'] || ''}` });
    }
}

interface IOnTrackOptions { 
    req: Request, 
    res: Response, 
    ipAddress?: string, 
    tanalytics: Tanalytics, 
    isBot: boolean, 
    trackNew: boolean 
}

interface IOptions {
    onTack?: (data: IOnTrackOptions) => any,
    eventType: string;
    onError?: (data: { req: Request, res: Response, error: any}) => any; 
};

export const userTrackerMiddleware = ({ eventType, onTack, onError }: IOptions): (req: Request, res: Response, next: NextFunction) => Promise<void> => {
    return async function (req: Request, res: Response, next: NextFunction) {
        try {
            const { email } = req.body;
            const ipAddress = req.headers['x-forwarded-for'] as string || req.connection.remoteAddress;
            const tanalytics = new Tanalytics(vars.tanalyticsToken);
            const { trackId, trackNew } = getTrackId({ req, res, ipAddress })
            const isBot = checkIsBot({ req });
            tanalytics.send(eventType, { userId: trackId });
            if (onTack) {
                onTack({req, res, ipAddress, tanalytics, trackNew, isBot})
            }
            tanalytics.sendMessage({ message: `${eventType} ${email || ipAddress}\n\n${req.headers['user-agent'] || ''}` });
            next();
        } catch (error) {
            if (onError) {
                onError({req, res, error});
            }
            console.log("userTrackerMiddleware error", error);
            next();
        }
    }
}
