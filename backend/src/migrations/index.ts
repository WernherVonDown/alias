import MigrationModel from "../models/Migration.model";
import { migrationList, migrationRegistry } from "./migrationRegistry";

export const runMigrations = async () => {
    try {
        const passedMigrations = await MigrationModel.find();
        const passedMigrationsTypes = passedMigrations.map(e => e.type);
        const remainingMigrationTypes = migrationList.filter(e => !passedMigrationsTypes.includes(e));

        if (remainingMigrationTypes.length) {
            for (const migrationType of remainingMigrationTypes) {
                await migrationRegistry[migrationType]();

                const migration = new MigrationModel({ type: migrationType });
                await migration.save();
            }
        }
        console.log("migrations done")
    } catch (error) {
        console.log('runMigrations', error)
    }

}