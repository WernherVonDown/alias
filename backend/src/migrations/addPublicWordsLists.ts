import { LanguageKeys } from "../const/LanguageKeys";
import { YandexDict, translateFromChinese, translateInChinese, translateInEnglish, translateInFrench, translateInGerman } from "../externalServices/yandexDict.service";
import wordsListModel, { IWordsList } from "../models/wordsList.model";
import wordModel, { IWord } from "../models/word.model";
import fs from 'fs'


const lists: { [key in LanguageKeys]: string } = {
    [LanguageKeys.english]: __dirname + '/data/english.json',
    [LanguageKeys.german]: __dirname + '/data/german.json',
    [LanguageKeys.french]: __dirname + '/data/french.json',
    [LanguageKeys.chinese]: __dirname + '/data/chinese.json',
}

const getWord = async ({ word, transcription, translation, languageKey, infinitive }: IWord) => {
    try {
        console.log("get words", { word, transcription, translation, languageKey, infinitive })
        let wordRes = await wordModel.findOne({
            word,
            transcription,
            translation,
            languageKey,
            infinitive,
            isRoot: true,
        });

        if (!wordRes) {
            wordRes = new wordModel({
                word,
                transcription,
                translation,
                languageKey,
                infinitive,
                isRoot: true,
            });

            await wordRes.save();
        }



        console.log("RES WORD", wordRes)

        return wordRes._id;
    } catch (error) {
        console.log("ERROR WORD", error, word)
    }
}
const lang = LanguageKeys.chinese;

// const createDoc = async () => {
//     const res = await wordsListModel.find({
//         isPublic: true,
//         languageKey: lang,
//     }).populate('words')
//     const res2 = res.map(l => ({isPublic: l.isPublic, languageKey: l.languageKey, name: l.name, 
//         words: l.words.map((w: IWord) => ({ word: w.word,
//             translation: w.translation,
//             transcription: w.transcription,
//             languageKey: w.languageKey,
//             infinitive: w.infinitive,
//             isRoot: w.isRoot}))
//     }))
//     console.log("RES", JSON.stringify(res2))
//     fs.appendFileSync(__dirname + `/${lang}.json`, JSON.stringify(res2))
// }

export const addPublicWordsLists = (language: LanguageKeys) => {
    return async () => {
        try {
            const dataPath = lists[language];
            const isExists = fs.existsSync(dataPath)
            console.log("COMPUTE", language, dataPath, isExists)
            if (isExists) {
                const data = fs.readFileSync(dataPath, 'utf-8');
                const parsedData = JSON.parse(data);
                console.log('READ DATA', parsedData)
                parsedData.map(async (list: IWordsList) => {
                    const resList = await wordsListModel.findOne({
                        languageKey: list.languageKey,
                        name: list.name,
                        isPublic: true,
                    });
                    console.log("RESULT LIST", resList)
                    if (!resList || resList.words.length !== list.words.length) {
                        const words = await Promise.all(list.words.map(getWord))
                        console.log("WORDS", words)
                        const r = await wordsListModel.findOneAndUpdate({
                            languageKey: list.languageKey,
                            name: list.name,
                            isPublic: true,
                        }, {
                            languageKey: list.languageKey,
                            name: list.name,
                            isPublic: true,
                            words,
                        },{
                            upsert: true,
                            new: true,
                        })
                        console.log("LIST CREATED", r)
                    }
                })
            }
        } catch (error) {
            console.log(`migrationError addPublicWordsLists ${language}`, error);
        }
    }
}