import { LanguageKeys } from "../const/LanguageKeys";
import { addPublicWordsLists } from "./addPublicWordsLists";

const publickWordsLists = {
    addPublicWordsListsEn: 'addPublicWordsListsEn',
    addPublicWordsListsDe: 'addPublicWordsListsDe',
    addPublicWordsListsFr: 'addPublicWordsListsFr',
    addPublicWordsListsZh: 'addPublicWordsListsZh',
}

const migrationKeys = {
    ...publickWordsLists,
}

export const migrationRegistry = {
    [migrationKeys.addPublicWordsListsEn]: addPublicWordsLists(LanguageKeys.english),
    [migrationKeys.addPublicWordsListsDe]: addPublicWordsLists(LanguageKeys.german),
    [migrationKeys.addPublicWordsListsFr]: addPublicWordsLists(LanguageKeys.french),
    [migrationKeys.addPublicWordsListsZh]: addPublicWordsLists(LanguageKeys.chinese),
}

export const migrationList = Object.values(migrationKeys);