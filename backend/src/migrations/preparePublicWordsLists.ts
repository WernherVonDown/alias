import { LanguageKeys } from "../const/LanguageKeys";
// import { YandexDict, translateFromChinese, translateInChinese, translateInEnglish, translateInFrench, translateInGerman } from "../externalServices/yandexDict.service";
// // import { a1 } from "./data/a1";
// // import { hsk1 } from "./data/hsk-1";
// //@ts-ignore
// import TextToIPA from 'text-to-ipa';
// import wordsListModel from "../models/wordsList.model";
// import wordModel, { IWord } from "../models/word.model";
// import fs from 'fs'
// import { addPublicWordsLists } from "./addPublicWordsLists";
// import { a1 } from "./data/a1";
// import { hsk1 } from "./data/hsk-1";
// import { a2 } from "./data/a2";
// import { hsk2 } from "./data/hsk-2";
// import { b1 } from "./data/b1";
// import { hsk3 } from "./data/hsk-3";
// import { b2 } from "./data/b2";
// import { hsk4 } from "./data/hsk-4";
// import { c1 } from "./data/c1";
// import { hsk5 } from "./data/hsk-5";
// import c2 from "./data/c2";
// import { hsk6 } from "./data/hsk-6";
// TextToIPA.loadDict();



// interface IList {
//     words: string[] | string[][];
//     name: string,
// }

// const words = [
//     "август",
//     "автобус",
//     // "автомобиль",
//     // "брать в аренду",
//     // "вода",
//     // "стекло"
//     // "бритва",
//     // "бросать",
//     // "яблоко",
//     // "красивый",
//     // "вещь",
//     // "женщина",
//     // 'девочка',
// ]

// const wordsZh = [
//     [
//         "爸爸",
//         "bà ba"
//     ],
//     [
//         "杯子",
//         "bēi zi"
//     ],
//     [
//         "北京",
//         "běi jīng"
//     ],
//     [
//         "本",
//         "běn"
//     ],
//     [
//         "不客气",
//         "bú kè qi"
//     ],
//     // [
//     //     "不",
//     //     "bù"
//     // ],
//     // [
//     //     "菜",
//     //     "cài"
//     // ],
//     // [
//     //     "茶",
//     //     "chá"
//     // ],
// ]

// const ListsEn: IList[] = [
//     {
//         words: c2,
//         name: 'English C1',
//     },
// ]

// const ListsDe: IList[] = [
//     {
//         words: c2,
//         name: 'German C2',
//     },
// ]

// const ListsFr: IList[] = [
//     {
//         words: c2,
//         name: 'French C2',
//     },
// ]

// const ListsZh: IList[] = [
//     {
//         words: hsk6,
//         name: 'Chinese HSK 6',
//     },
// ]

// const lists: { [key in LanguageKeys]: IList[] } = {
//     [LanguageKeys.english]: ListsEn,
//     [LanguageKeys.german]: ListsDe,
//     [LanguageKeys.french]: ListsFr,
//     [LanguageKeys.chinese]: ListsZh,
// }


const sylregExp = /^(a|e|i|o|u)/i

export const translateResult: { [key in LanguageKeys]: (trans: { text: string, pos: string, gen: 'f' | 'm' | 'n' }) => string } = {
    [LanguageKeys.english]: (trans) => {
        let resTransText = trans.text;
        if (trans.pos === 'verb') {
            resTransText = `to ${trans.text}`;
        } else if (trans.pos === 'noun') {
            resTransText = `${trans.text.match(sylregExp) ? 'an' : 'a'} ${trans.text}`;
        }
        return resTransText;
    },
    [LanguageKeys.german]: (trans) => {
        let resTransText = trans.text;
        if (trans.pos === 'noun') {
            let article = 'der';
            if (trans.gen === 'n') {
                article = 'das'
            } else if (trans.gen === 'f') {
                article = 'die'
            }

            resTransText = `${article} ${trans.text}`;
        }
        return resTransText;
    },
    [LanguageKeys.french]: (trans) => {
        let resTransText = trans.text;
        if (trans.pos === 'noun') {
            let article = 'un';
            if (trans.gen === 'f') {
                article = 'une'
            }

            resTransText = `${article} ${trans.text}`;
        }
        return resTransText;
    },
    [LanguageKeys.chinese]: (trans) => {
        // console.log("TRANS", trans)
        return trans.text;
    },
}

// const translators: { [key in LanguageKeys]: YandexDict } = {
//     [LanguageKeys.english]: translateInEnglish,
//     [LanguageKeys.german]: translateInGerman,
//     [LanguageKeys.french]: translateInFrench,
//     [LanguageKeys.chinese]: translateFromChinese,
// }

// const IN_RUS_TRANS: { [key in LanguageKeys]: boolean } = {
//     [LanguageKeys.chinese]: true,
//     [LanguageKeys.english]: false,
//     [LanguageKeys.german]: false,
//     [LanguageKeys.french]: false,
// }

// const getWord = async (language: LanguageKeys, word: string, transcript?: string) => {
//     try {
//         // console.log("RUN", word, transcript)
//         let wordRes = await wordModel.findOne({
//             $or: [
//                 { word },
//                 { translation: word }
//             ],
//             languageKey: language,
//             isRoot: true,
//         });
//         if (wordRes) {
//             return wordRes._id
//         }
//         const res = await translators[language].translate(word);

//         if (!res.length) {
//             console.log("skip", language, word, transcript)
//             return
//         }
//         const trans = res[0]?.tr[0];
//         // console.log("TRANS", trans)
//         const resTransText = translateResult[language](trans)
//         if (LanguageKeys.english === language) {
//             try {
//                 transcript = TextToIPA.lookup(trans.text).text;
//             } catch (error) {
//                 console.log("TRANS ERROR", error)
//             }

//         }
//         // resTransText = res[0].tr[0].pos === 'verb' ? `to ${res[0].tr[0].text}` : res[0].tr[0].text
//         // console.log("RESULT", word, resTransText, transcript);
//         const inRus = IN_RUS_TRANS[language];
//         if (!inRus) {
//             wordRes = new wordModel({
//                 word: res[0].tr[0].text,
//                 translation: word,
//                 transcription: transcript,
//                 languageKey: language,
//                 infinitive: resTransText,
//                 isRoot: true,
//             });
//         } else {
//             wordRes = new wordModel({
//                 word: word,
//                 translation: resTransText,
//                 transcription: transcript,
//                 languageKey: language,
//                 infinitive: word,
//                 isRoot: true,
//             });
//         }


//         await wordRes.save();
//         // console.log("RES WORD", wordRes)

//         return wordRes._id;
//     } catch (error) {
//         console.log("ERROR WORD", error, word)
//     }
// }
// const lang = LanguageKeys.chinese;
// export const startLol = () => {
//     // return;
//     // // return addPublicWordsLists(lang)();
//     // return createDoc()
//     // return
//     const list = lists[lang];
//     list.forEach(async ({ name, words }) => {
//         const resList = await wordsListModel.findOne({
//             languageKey: lang,
//             name,
//             isPublic: true,
//         });
//         console.log("IS LIST EXIST", resList)
//         if (!resList || resList.words.length !== words.length) {
//             //@ts-ignore
//             if (lang === LanguageKeys.chinese) {
//                 words = await Promise.all(words.map(([w, t]) => getWord(lang, w, t))) as string[]
//             } else {
//                 //@ts-ignore
//                 words = await Promise.all(words.map(w => getWord(lang, w))) as string[]
//             }
//             words = words.filter((w: string) => w);
//             console.log('WORDS IDS', words);
            

//             const r = await wordsListModel.findOneAndUpdate({
//                 languageKey: lang,
//                 name,
//                 isPublic: true,
//             }, {
//                 languageKey: lang,
//                 name,
//                 isPublic: true,
//                 words,
//             },{
//                 upsert: true,
//                 new: true,
//             })
//             console.log("RESULT LIST", r)
//         } else {
//             console.log("LIST NORM", resList)
//         }
//     })

// }

// const createDoc = async () => {
//     const res = await wordsListModel.find({
//         isPublic: true,
//         languageKey: lang,
//     }).populate('words')
//     const res2 = res.map(l => ({isPublic: l.isPublic, languageKey: l.languageKey, name: l.name, 
//         words: l.words.map((w: IWord) => ({ word: w.word,
//             translation: w.translation,
//             transcription: w.transcription,
//             languageKey: w.languageKey,
//             infinitive: w.infinitive,
//             isRoot: w.isRoot}))
//     }))
//     console.log("RES", JSON.stringify(res2))
//     fs.appendFileSync(__dirname + `/data/${lang}.json`, JSON.stringify(res2))
// }

// // export const addPublicWordsLists = (language: LanguageKeys) => {
// //     return async () => {
// //         try {
// //             console.log("COMPUTE", language)
// //         } catch (error) {
// //             console.log(`migrationError addPublicWordsLists ${language}`, error);
// //         }
// //     }
// // }